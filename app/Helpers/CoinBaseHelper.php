<?php

namespace App\Helpers;

use Config;

class CoinBaseHelper
{
    //GET FUNCTION COINBASE
    public static function  getMethod($url)
    {
        $coinBaseKey = getenv("COINBASE_KEY");
        $coinBaseSecret = getenv("COINBASE_SECRET");
        $coinBaseVersion = getenv("COINBASE_VERSION");
        $baseURL = getenv("COINBASE_BASEURL");
        // $passphrase = getenv("COINBASE_PASSPHRASE_LOCAL");
        $method = "GET";

        //GET TIMESTAMP FROM COINBASE
        $chPost = curl_init();
        curl_setopt($chPost, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($chPost, CURLOPT_URL, "https://api.coinbase.com/v2/time");
        $resultPost = curl_exec($chPost);
        curl_close($chPost);
        $jsonPost = json_decode($resultPost, true);
        $timestamp = $jsonPost["data"]["epoch"];

        //GENERATE SIGNATURE KEY
        //MESSAGE UNTUK ENKRIPSI
        $message = $timestamp . $method . $url;
        //SIGNATURE KEY -> ENKRIPSI HASH HMAC
        $signature = hash_hmac("sha256", $message, $coinBaseSecret);

        $request_headers = array(
            "CB-ACCESS-KEY:" . $coinBaseKey,
            "CB-ACCESS-SIGN:" . $signature,
            "CB-ACCESS-TIMESTAMP:" . $timestamp,
            "CB-VERSION:" . $coinBaseVersion,
            // "CB-ACCESS-PASSPHRASE:".$passphrase,
            // "USER-AGENT:*"
        );

        //GET DATA FROM URL
        $chPost = curl_init();
        //SET OPTION CURL
        curl_setopt($chPost, CURLOPT_URL, $baseURL . $url);
        curl_setopt($chPost, CURLOPT_HTTPHEADER, $request_headers);
        curl_setopt($chPost, CURLINFO_HEADER_OUT, true);
        curl_setopt($chPost, CURLOPT_RETURNTRANSFER, true);
        $resultPost = curl_exec($chPost);
        curl_close($chPost);
        //DECODE JSON DATA
        $jsonPost = json_decode($resultPost, true);
        error_log($resultPost);
        return $jsonPost;
    }

    //POST FUNCTION COINBASE
    public static function postMethod($url, $body = "")
    {
        $coinBaseKey = getenv("COINBASE_KEY");
        $coinBaseSecret = getenv("COINBASE_SECRET");
        $coinBaseVersion = getenv("COINBASE_VERSION");
        $baseURL = getenv("COINBASE_BASEURL");
        $method = "POST";


        //GET TIMESTAMP FROM COINBASE
        $chPost = curl_init();
        curl_setopt($chPost, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($chPost, CURLOPT_URL, "https://api.coinbase.com/v2/time");
        $resultPost = curl_exec($chPost);
        curl_close($chPost);
        $jsonPost = json_decode($resultPost, true);
        $timestamp = $jsonPost["data"]["epoch"];

        //GENERATE SIGNATURE KEY
        //MESSAGE UNTUK ENKRIPSI
        $message = $timestamp . $method . $url . $body;
        //SIGNATURE KEY -> ENKRIPSI HASH HMAC
        $signature = hash_hmac("sha256", $message, $coinBaseSecret);

        $request_headers = array(
            "Content-Type: application/json",
            "CB-ACCESS-KEY:" . $coinBaseKey,
            "CB-ACCESS-SIGN:" . $signature,
            "CB-ACCESS-TIMESTAMP:" . $timestamp,
            "CB-VERSION:" . $coinBaseVersion
        );

        //GET DATA FROM URL
        $chPost = curl_init();
        curl_setopt($chPost, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($chPost, CURLOPT_POSTFIELDS, $body);
        //SET OPTION CURL
        curl_setopt($chPost, CURLOPT_URL, $baseURL . $url);
        curl_setopt($chPost, CURLOPT_HTTPHEADER, $request_headers);
        curl_setopt($chPost, CURLINFO_HEADER_OUT, true);
        curl_setopt($chPost, CURLOPT_RETURNTRANSFER, true);
        $resultPost = curl_exec($chPost);
        curl_close($chPost);
        //DECODE JSON DATA
        $jsonPost = json_decode($resultPost, true);
        error_log($resultPost);
        return $jsonPost;
    }

    //GET HARGA CRYPTO
    public static function getPriceTokoCrypto($type)
    {
        $url = "https://www.tokocrypto.com/v1/market/depth?symbol=" . $type;
        $data = CommonHelper::getAPIExternal($url);

        return $data;
    }
}
