<?php

namespace App\Http\Controllers\WebServices;

use App\Helpers\CommonHelper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\MasterCoin;
use App\Models\MasterMining;
use App\Models\MasterStaking;
use App\Models\PriceCoin;
use App\Models\SettingFee;
use App\Models\SettingGeneral;
use App\Models\SharingProfit;
use App\Models\TrxDeposit;
use App\Models\TrxHistory;
use App\Models\TrxMining;
use App\Models\TrxStaking;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use stdClass;
use Validator;

class UserWebServiceController extends Controller
{
    //
    public function register(Request $request)
    {
        $email = $this->test_input($request->email);
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return $this->createErrorMessage("Email not valid", 400);
        }

        $checkExist = User::where("email", $request->email)->first();
        if ($checkExist) {
            if ($checkExist->status == 1) {
                return $this->createErrorMessage("Email Already Exists", 400);
            } else {
                $checkExist->delete();
            }
        }

        if (isset($request->referal_code)) {
            $check = User::where("referal_code", $request->referal_code)->first();
            if (!isset($check)) {
                return $this->createErrorMessage("Referral Code not exist", 400);
            }
        }

        $newUser = new User();
        $newUser->email = $request->email;
        $newUser->password =  Hash::make($request->password);
        $newUser->name = $request->name;
        $newUser->referal_parent = $request->referal_code;
        $kode = "REF" . $this->generateRandomString(4) . date("YmdHis");
        $newUser->referal_code = $kode;
        $verify_code = Hash::make($this->generateRandomString(16));
        $newUser->verify_code = $verify_code;

        $newUser->save();
        DB::commit();

        $view = 'verifyemailmails';
        $to_name = $request->name;
        $to_email = $request->email;
        $from = env('MAIL_USERNAME', 'noreplysupport@gmail.com');
        $emailname = env('MAIL_NAME', 'VINIX');
        $link = URL('/verify_email') . "?email=" . $request->email . "&kode=" . $verify_code;
        $data = array(
            "kode" => $verify_code,
            "name" => $request->name,
            "email" => $request->email,
            "link" => $link
        );
        CommonHelper::sendMail($view, $data, $to_name, $to_email, "Verification Sign Up Vinix", $from, $emailname);

        return $this->createSuccessMessage("Sign Up Success, please verify your email");
    }
    public function topupadmin(Request $request)
    {
        if (!Auth::check()) {
            return $this->createErrorMessage("Please Login First", 400);
        }
        $user = Auth::user();
        if ($user->role != 2) {
            return $this->createErrorMessage("User Not Allowed", 400);
        }

        $id_user = $request->id_user;
        $nominal = $request->nominal;
        $id_coin = $request->id_coin;

        $data = new TrxHistory();
        $data->id_user = $id_user;
        $data->id_coin = $id_coin;
        $data->id_ref = -1;
        $data->trx_type = "DEPOSIT";
        $data->status = 1;
        $data->total_fee = 0;
        $data->amount = $nominal;
        $data->save();

        return $this->createSuccessMessage($data, 200, "Success Top Up Admin");
    }

    public function verifyemail(Request $request)
    {
        $email = $request->email;
        $kode = $request->kode;

        $data = User::where("email", $email)
            ->where("verify_code", $kode)
            ->first();
        if (!isset($data)) {
            return $this->createErrorMessage("Verification Code Expired", 400);
        }
        $data->status = 1;
        $data->verify_code = "";
        $data->save();

        return $this->createSuccessMessage("Verification Email Success");
    }



    public function login(Request $request)
    {
        $email = $request->email;
        $password = $request->password;

        if (!isset($email) || !isset($password)) {
            return $this->createErrorMessage("Parameter tidak lengkap", 400);
        }

        $data = [
            "email" => $email,
            "password" => $password
        ];
        Auth::attempt($data, true);

        if (Auth::check()) {
            $user = Auth::user();
            if ($user->status == 0) {
                Auth::logout();
                return $this->createErrorMessage("Please verify your email", 400);
            }
            if ($user->referal_code == null) {
                $dt = User::where("id", $user->id)->first();
                $kode = "REF" . $this->generateRandomString(4) . date("YmdHis");
                $dt->referal_code = $kode;
                $dt->save();
            }
            return $this->createSuccessMessage($user, 200, "Login Success");
        } else {
            return $this->createErrorMessage("Email or Password wrong", 400);
        }
    }

    public function getSaldo()
    {
        if (!Auth::check()) {
            return $this->createErrorMessage("Please Login First", 400);
        }
        $user = Auth::user();

        //GET SEMUA KOIN YANG ADA
        $coin = MasterCoin::get();
        foreach ($coin as $value => $c) {
            $data[$value] = $c;
            $data[$value]["saldo"] = User::getSaldo($c->code, $user->id);
        }

        return $this->createSuccessMessage($data);
    }

    public function getInitialData()
    {
        if (!Auth::check()) {
            return $this->createErrorMessage("Please Login First", 400);
        }

        $user = Auth::user();
        //GET SEMUA KOIN YANG ADA
        $data["user"] = $user;
        $coin = MasterCoin::get();
        $total_asset = 0;
        $id_usd = MasterCoin::getIdCoin("USDV");
        foreach ($coin as $value => $c) {
            $saldo = User::getSaldo($c->code, $user->id);
            $data["wallet"][$value] = $c;
            $data["wallet"][$value]["saldo"] = $saldo;

            $saldo_usd = PriceCoin::convertCoin($c->id, $id_usd, $saldo);
            $total_asset = $total_asset + $saldo_usd;
        }
        $data["total_asset"] = $total_asset;
        //GET SETTING FEE
        $setting = SettingFee::get();
        foreach ($setting as $s) {
            $temp = str_replace(" ", "_", $s->fee_name);
            $data["setting"][$temp] = $s;
        }

        //GET SETTING GENERAL
        $settingGeneral = SettingGeneral::get();
        foreach ($settingGeneral as $value => $c) {
            $data["setting_general"][$c->setting_name] = $c;
        }
        $sharing_profit = SharingProfit::get();
        $data["sharing_profit"] = $sharing_profit;
        $data["price_vnx"] = PriceCoin::getPrice("VNX");

        // return $this->createSuccessMessage($total_asset);

        return $this->createSuccessMessage($data);
    }

    public function logout()
    {
        if (!Auth::check()) {
            return $this->createSuccessMessage("User already Logout");
        }

        Auth::logout();
        return $this->createSuccessMessage("Logout Success");
    }

    public function forgotPassword(Request $request)
    {
        $email = $request->email;
        $data = User::where("email", $email)->first();
        if (!isset($data)) {
            return $this->createErrorMessage("Email not exist", 400);
        }
        $forget_password_code = $this->generateRandomString(8);
        // echo $forgot_password_code;
        $data->forget_password_code = $forget_password_code;
        $data->save();


        $view = 'forgotpasswordmails';
        $to_name = $data->name;
        $to_email = $data->email;
        $from = env('MAIL_USERNAME', 'noreplysupport@vinix.ca');
        $emailname = env('MAIL_NAME', 'VINIX');
        $link = URL('/reset-password') . "?email=" . $data->email . "&kode=" . $forget_password_code;
        $data = array(
            "kode" => $forget_password_code,
            "name" => $data->name,
            "email" => $data->email,
            "link" => $link
        );
        CommonHelper::sendMail($view, $data, $to_name, $to_email, "Reset Password Vinix", $from, $emailname);

        return $this->createSuccessMessage(null, 200, "Verification Code had sent to your email");
        // return $this->createSuccessMessage($data);
    }

    public function getDashboard()
    {
        if (!Auth::check()) {
            return $this->createErrorMessage("Please Login First", 400);
        }

        $user = Auth::user();

        $id_usdv = MasterCoin::getIdCoin("USDV");
        $id_vnx = MasterCoin::getIdCoin("VNX");
        $id_eth = MasterCoin::getIdCoin("ETH");

        $wallet = new stdClass();

        $wallet->staking = new stdClass();
        $wallet->mining = new stdClass();

        // $wallet->usdv->saldo = User::getSaldo("USDV", $user->id);
        // $wallet->vnx->saldo = User::getSaldo("VNX", $user->id);

        $date_now = date("Y-m-d H:i:s");
        $dataStaking = TrxStaking::where("trx_staking.id_user", $user->id)
            ->where("trx_staking.status", 0)
            ->join("master_staking", "master_staking.id", "trx_staking.id_product_staking")
            ->get();


        $dataMining = TrxMining::leftJoin("master_mining", "trx_mining.id_product_mining", "=", "master_mining.id")
            ->where("trx_mining.status", 0)
            ->where("trx_mining.id_user", $user->id)
            ->select("trx_mining.*", "master_mining.id_coin_reward as master_id_coin_reward", "master_mining.product_name", "master_mining.product_type", "master_mining.tenor as master_tenor", "master_mining.price as master_price")
            ->get();

        $total_staking = 0;
        $total_can_harvest_staking = 0;
        $total_mining = 0;
        $total_can_harvest_mining = 0;

        foreach ($dataStaking as $dt) {
            $amount_in_dollar = PriceCoin::convertCoin($dt->id_coin_stake, $id_usdv, $dt->amount_stake);
            $total_staking += $amount_in_dollar;

            //HITUNG PROFIT YANG DAPAT DIHARVEST
            $selisih = CommonHelper::getSelisihMinutes($dt->harvest_at, $date_now);
            $profit = $dt->profit;
            $profit = floatval(($profit / 100) / 30 / 24 / 60) * $selisih;

            $hasil_farming = $profit * $dt->amount_stake;

            //POTONG SHARING PROFIT
            $sp = SharingProfit::getSharingProfit($dt->id_coin_reward, $dt->amount_stake);
            $sharing_profit = $hasil_farming * ($sp->sharing_profit / 100);
            $hasil_farming = $hasil_farming - $sharing_profit;
            $hasil_farming = PriceCoin::convertCoin($dt->id_coin_reward, $id_usdv, $hasil_farming);
            $total_can_harvest_staking = $total_can_harvest_staking + $hasil_farming;
        }

        foreach ($dataMining as $dt) {
            if ($dt->id_product_mining != null) {
                $dt->price = $dt->master_price;
                $dt->id_coin_reward = $dt->master_id_coin_reward;
                $dt->tenor = $dt->master_tenor;
            } else {
                $dt->product_name = "Custom";
                $dt->product_type = "Custom Mining";
            }
            if (MasterCoin::getKodeCoin($dt->id_coin_reward) != "AETHER") {
                $dt->selisih = CommonHelper::getSelisihDay(CommonHelper::convertDateFormat($dt->last_harvest), $date_now);

                $selisih_date_harvest = CommonHelper::getSelisihMinutes($dt->last_harvest, $date_now);

                $fee = SettingFee::getValue("MINING_FEE");
                $dt->reward_coin = MasterCoin::getKodeCoin($dt->id_coin_reward);
                //GET VALUE MHS TO USD
                // $mhs_to_usd = SettingGeneral::where("setting_name", "MHS_" . $dt->reward_coin . "_PER_DAY")->first();
                // $can_harvest = (($mhs_to_usd->value / 24 / 60) * $dt->mining_speed) * $selisih_date_harvest;
                $can_harvest = $dt->can_harvest;

                $total_mining += $dt->price;

                $can_harvest = $can_harvest - $fee->value;
                if ($can_harvest < 0) {
                    $can_harvest = 0;
                }
                $total_can_harvest_mining += $can_harvest;
            }
        }

        // $wallet->usdv = TrxStaking::getTotalCanHarvestStaking($id_usdv, $user->id);
        // $wallet->vnx = TrxStaking::getTotalCanHarvestStaking($id_vnx, $user->id);

        $wallet->profit_usdv = MasterStaking::where("id_coin_reward", $id_usdv)
            ->orderBy("profit", "desc")
            ->first()->profit * 12;
        $wallet->profit_vnx = MasterStaking::where("id_coin_reward", $id_vnx)
            ->orderBy("profit", "desc")
            ->first()->profit * 12;
        $wallet->profit_eth = MasterStaking::where("id_coin_reward", $id_eth)
            ->orderBy("profit", "desc")
            ->first()->profit * 12;


        $data = new stdClass();
        $data->wallet = $wallet;
        $data->total_staking = $total_staking;
        $data->total_can_harvest_staking = $total_can_harvest_staking;
        $data->total_mining = $total_mining;
        $data->total_can_harvest_mining = $total_can_harvest_mining;
        // $data = $wallet_usdv;
        // $data = $wallet_vnx;
        return $this->createSuccessMessage($data);
    }

    public function getDashboardAdmin()
    {
        $totalRequestWithdraw = TrxDeposit::where("trx_type", "WITHDRAW")
            ->where("status", 0)
            ->where("verified", 1)
            ->count("id");

        $dataHarvest = TrxHistory::where("trx_type", "STAKING_HARVEST")
            ->orWhere("trx_type", "MINING_REWARD")
            ->get();

        $dataActiveStaking = TrxStaking::where("trx_staking.status", 0)
            ->join("master_staking", "trx_staking.id_product_staking", "master_staking.id")
            ->join("master_coin", "master_staking.id_coin_reward", "master_coin.id")
            ->get();
        $dataActiveMining = TrxMining::leftJoin("master_mining", "trx_mining.id_product_mining", "=", "master_mining.id")
            ->where("trx_mining.status", 0)
            ->select("trx_mining.*", "master_mining.id_coin_reward as master_id_coin_reward", "master_mining.product_name", "master_mining.product_type", "master_mining.tenor as master_tenor", "master_mining.price as master_price")
            ->get();



        $dataAsset = TrxHistory::where("trx_type", "DEPOSIT")->get();
        $id_dollar = MasterCoin::getIdCoin("USDV");

        $feeOriginal = SettingFee::where("fee_name", "HARVEST_FEE")->first()->value;
        $date_now = date("Y-m-d H:i:s");

        $totalAsset = 0;
        $totalHarvest = 0;
        $totalSharingProfitStaking = 0;
        $totalSharingProfitMining = 0;


        //INITIAL DATA STAKING
        $temp = MasterStaking::join("master_coin", "master_staking.id_coin_reward", "master_coin.id")->select("master_coin.code")->distinct()->get();

        foreach ($temp as $t) {
            $staking[$t->code]["code"] = $t->code;
            $staking[$t->code]["amount_in_dollar"] = 0;
            $staking[$t->code]["amount"] = 0;
            $staking[$t->code]["total_staking_in_dollar"] = 0;
            $staking[$t->code]["total_staking"] = 0;

            $harvest["staking"][$t->code]["code"] = $t->code;
            $harvest["staking"][$t->code]["amount_in_dollar"] = 0;
            $harvest["staking"][$t->code]["amount"] = 0;
        }

        //INITIAL DATA MINING
        $temp = MasterMining::join("master_coin", "master_mining.id_coin_reward", "master_coin.id")->select("master_coin.code")->distinct()->get();
        foreach ($temp as $t) {
            $mining[$t->code]["code"] = $t->code;
            $mining[$t->code]["amount_in_dollar"] = 0;
            $mining[$t->code]["amount"] = 0;
            $mining[$t->code]["total_speed"] = 0;

            $harvest["mining"][$t->code]["code"] = $t->code;
            $harvest["mining"][$t->code]["amount_in_dollar"] = 0;
            $harvest["mining"][$t->code]["amount"] = 0;
        }

        //INITIAL DATA AETHER
        $t->code = "AETHER";
        $mining[$t->code]["code"] = $t->code;
        $mining[$t->code]["amount_in_dollar"] = 0;
        $mining[$t->code]["amount"] = 0;
        $mining[$t->code]["total_speed"] = 0;

        $harvest["mining"][$t->code]["code"] = $t->code;
        $harvest["mining"][$t->code]["amount_in_dollar"] = 0;
        $harvest["mining"][$t->code]["amount"] = 0;

        //HITUNG DATA ASSET
        foreach ($dataAsset as $dt) {
            $amount_in_dollar = PriceCoin::convertCoin($dt->id_coin, $id_dollar, $dt->amount);
            $totalAsset += $amount_in_dollar;
        }

        //HITUNG DATA HARVEST
        foreach ($dataHarvest as $dt) {
            $amount_in_dollar = PriceCoin::convertCoin($dt->id_coin, $id_dollar, $dt->amount);
            $totalHarvest += $amount_in_dollar;
            if ($dt->trx_type == "MINING_REWARD") {
                $data = TrxMining::where("trx_mining.id", $dt->id_ref)
                    ->first();
                if (!isset($data["id_product_mining"])) {
                    $data["code"] = MasterCoin::getKodeCoin($data["id_coin_reward"]);
                } else {
                    $kode = MasterMining::where("id", $data["id_product_mining"])->select("id_coin_reward")->first();
                    $data["code"] = MasterCoin::getKodeCoin($kode->id_coin_reward);
                }
                $harvest["mining"][$data["code"]]["amount_in_dollar"] += $amount_in_dollar;
                $harvest["mining"][$data["code"]]["amount"] += $dt->amount;
            } else if ($dt->trx_type == "STAKING_HARVEST") {
                $data = TrxStaking::where("trx_staking.id", $dt->id_ref)
                    ->join("master_staking", "trx_staking.id_product_staking", "master_staking.id")
                    ->join("master_coin", "master_staking.id_coin_reward", "master_coin.id")
                    ->first();

                $harvest["staking"][$data["code"]]["amount_in_dollar"] += $amount_in_dollar;
                $harvest["staking"][$data["code"]]["amount"] += $dt->amount;
            }
        }
        $totalAsset += $totalHarvest;

        //HITUNG TOTAL STAKING YANG HARUS DIBAGIKAN
        foreach ($dataActiveStaking as $dt) {

            //HITUNG FEE HARVEST
            $fee = PriceCoin::convertCoin($id_dollar, $dt->id_coin_reward, $feeOriginal);

            //HITUNG PROFIT YANG DAPAT DIHARVEST
            $selisih = CommonHelper::getSelisihMinutes($dt->harvest_at, $date_now);
            $profit = $dt->profit;
            $profit = floatval(($profit / 100) / 30 / 24 / 60) * $selisih;
            $hasil_farming = $profit * $dt->amount_stake;
            $hasil_farming = $hasil_farming - $fee;
            $hasil_farming = PriceCoin::convertCoin($dt->id_coin_reward, $id_dollar, $hasil_farming);
            $totalAsset += $hasil_farming;
            //POTONG SHARING PROFIT
            $sp = SharingProfit::getSharingProfit($dt->id_coin_reward, $dt->amount_stake);
            $sharing_profit = $hasil_farming * ($sp->sharing_profit / 100);
            if ($dt->referal_status == 0) {
                $hasil_farming = $hasil_farming - $sharing_profit;
            }
            $totalSharingProfitStaking += $hasil_farming;

            $staking[$dt->code]["amount_in_dollar"] += $hasil_farming;
            $staking[$dt->code]["amount"] += PriceCoin::convertCoin($id_dollar, $dt->id_coin_reward, $hasil_farming);
            $staking[$dt->code]["total_staking_in_dollar"] += PriceCoin::convertCoin($dt->id_coin_stake, $id_dollar, $dt->amount_stake);
            $staking[$dt->code]["total_staking"] += $dt->amount_stake;
        }

        //HITUNG TOTAL MINING YANG HARUS DIBAGIKAN
        foreach ($dataActiveMining as $dt) {
            //HITUNG PROGRESS MINING

            if ($dt->id_product_mining != null) {
                $dt->price = $dt->master_price;
                $dt->id_coin_reward = $dt->master_id_coin_reward;
                $dt->tenor = $dt->master_tenor;
            } else {
                $dt->product_name = "Custom";
                $dt->product_type = "Custom Mining";
            }
            $dt->selisih = CommonHelper::getSelisihDay(CommonHelper::convertDateFormat($dt->last_harvest), $date_now);

            $selisih_date_harvest = CommonHelper::getSelisihMinutes($dt->last_harvest, $date_now);


            $fee = SettingFee::getValue("MINING_FEE");
            $dt->reward_coin = MasterCoin::getKodeCoin($dt->id_coin_reward);
            //GET VALUE MHS TO USD
            // $mhs_to_usd = SettingGeneral::where("setting_name", "MHS_" . $dt->reward_coin . "_PER_DAY")->first();
            // $can_harvest = (($mhs_to_usd->value / 24 / 60) * $dt->mining_speed) * $selisih_date_harvest;

            $can_harvest = $dt->can_harvest;
            $totalAsset += $can_harvest;

            // $can_harvest = $can_harvest - $fee->value;
            if ($can_harvest < 0) {
                $can_harvest = 0;
            }
            $totalSharingProfitMining += $can_harvest;

            $mining[$dt->reward_coin]["amount_in_dollar"] += $can_harvest;
            $mining[$dt->reward_coin]["amount"] += PriceCoin::convertCoin($id_dollar, $dt->id_coin_reward, $can_harvest);
            $mining[$dt->reward_coin]["total_speed"] += $dt->mining_speed;
        }

        $data = new stdClass();
        $data->pending_withdraw = $totalRequestWithdraw;
        $data->total_harvest_in_dollar = $totalHarvest;
        $data->total_asset_in_dollar = $totalAsset;
        $data->total_sharing_profit_staking = $totalSharingProfitStaking;
        $data->total_sharing_profit_mining = $totalSharingProfitMining;
        $data->harvest = $harvest;
        $data->staking = $staking;
        $data->mining = $mining;

        return $this->createSuccessMessage($data);
    }

    public function getDataHistoryDashboard()
    {
        $dataHistoryStaking = TrxHistory::where("trx_history.trx_type", "like", "%STAKING%")
            ->join("users", "users.id", "trx_history.id_user")
            ->join("trx_staking", "trx_history.id_ref", "trx_staking.id")
            ->join("master_staking", "trx_staking.id_product_staking", "master_staking.id")
            ->join("master_coin", "trx_history.id_coin", "master_coin.id")
            ->select("trx_history.*", "users.name", "master_staking.product_name", "master_coin.code")
            ->orderBy("trx_history.created_at", "desc")
            ->get();
        $dataHistoryMining = TrxHistory::where("trx_type", "like", "%MINING%")
            ->join("users", "users.id", "trx_history.id_user")
            ->join("trx_mining", "trx_history.id_ref", "trx_mining.id")
            ->leftJoin("master_mining", "trx_mining.id_product_mining", "master_mining.id")
            ->join("master_coin", "trx_history.id_coin", "master_coin.id")
            ->select("trx_history.*", "users.name", "master_mining.product_name", "master_coin.code")
            ->orderBy("created_at", "desc")
            ->get();

        $data["history_staking"] = $dataHistoryStaking;
        $data["history_mining"] = $dataHistoryMining;
        return $this->createSuccessMessage($data);
    }

    public function changePassword(Request $request)
    {
        $new_password = $request->new_password;
        $email = $request->email;
        $verification_code = $request->code;

        $data = User::where("email", $email)->first();
        if (!isset($data)) {
            return $this->createErrorMessage("Email not exist", 400);
        }
        if ($verification_code != $data->forget_password_code) {
            return $this->createErrorMessage("Your Verification Code Expired, please generate code again.", 400);
        }

        if ($new_password == "") {
            return $this->createErrorMessage("New Password must filled", 400);
        }

        $data->password =  Hash::make($new_password);
        $data->forget_password_code = null;
        $data->save();

        DB::commit();

        return $this->createSuccessMessage("Success change password");
    }

    public function getAllUser()
    {
        if (!Auth::check()) {
            return $this->createErrorMessage("Please Login First", 400);
        }

        $user = Auth::user();
        if ($user->role != 2) {
            return $this->createErrorMessage("User Not Allowed", 400);
        }

        $data = User::select("id", "name", "email", "referal_code", "referal_parent", "status")->get();

        return $this->createSuccessMessage($data);
    }

    public function getDetailUser(Request $request)
    {
        if (!Auth::check()) {
            return $this->createErrorMessage("Please Login First", 400);
        }

        $user = Auth::user();
        if ($user->role != 2) {
            return $this->createErrorMessage("User Not Allowed", 400);
        }

        $dataUser = User::where("id", $request->id)->first();
        $dataMining = TrxMining::where("trx_mining.id_user", $request->id)
            ->leftJoin("master_mining", "master_mining.id", "trx_mining.id_product_mining")
            ->orderBy("trx_mining.created_at", "desc")
            ->select("trx_mining.*", "master_mining.id_coin_reward as master_id_coin_reward", "master_mining.product_name", "master_mining.product_type", "master_mining.tenor as master_tenor", "master_mining.price as master_price")
            ->get();

        $dataStaking = TrxStaking::where("trx_staking.id_user", $request->id)
            ->join("master_staking", "master_staking.id", "trx_staking.id_product_staking")
            ->orderBy("trx_staking.created_at", "desc")
            ->select("trx_staking.*", "master_staking.product_name")
            ->get();

        $dataHarvestStaking = TrxHistory::where("trx_history.id_user", $request->id)
            ->join("trx_staking", "trx_staking.id", "trx_history.id_ref")
            ->join("master_staking", "master_staking.id", "trx_staking.id_product_staking")
            ->where("trx_history.trx_type", "STAKING_HARVEST")
            ->select("trx_history.*", "master_staking.*", "trx_staking.*", "trx_history.created_at as date_transaction")
            ->get();

        $dataHarvestMining = TrxHistory::where("trx_history.id_user", $request->id)
            ->join("trx_mining", "trx_mining.id", "trx_history.id_ref")
            ->leftJoin("master_mining", "master_mining.id", "trx_mining.id_product_mining")
            ->join("master_coin", "master_mining.id_coin_reward", "master_coin.id")
            ->where("trx_history.trx_type", "MINING_REWARD")
            ->select("trx_history.*", "master_mining.*", "trx_mining.*", "trx_history.created_at as date_transaction", "master_coin.code")
            ->get();

        $date_now = date("Y-m-d H:i:s");
        foreach ($dataMining as $dt) {
            //HITUNG PROGRESS MINING
            $created_at = CommonHelper::convertDateFormat($dt->created_at);
            $total_hari = CommonHelper::getSelisihDay($created_at, $dt->end_at);
            $progress = CommonHelper::getSelisihDay($created_at, $date_now);
            $progressBar = ($progress / $total_hari) * 100;
            if ($progressBar > 100) {
                $progressBar = 100;
            }
            $dt->progress_percentage = round($progressBar, 2);

            $dt->created_at_format = CommonHelper::convertDateFormatFrontend($dt->created_at);
            $dt->end_at_format = CommonHelper::convertDateFormatFrontend($dt->end_at);
            $dt->last_harvest_format = CommonHelper::convertDateFormatFrontend($dt->last_harvest);
            $dt->selisih = CommonHelper::getSelisihDay(CommonHelper::convertDateFormat($dt->last_harvest), date("Y-m-d"));

            $selisih_date_harvest = CommonHelper::getSelisihMinutes($dt->last_harvest, $date_now);


            $fee = SettingFee::getValue("MINING_FEE");
            //CONVERT PROFIT USD TO KOIN REWARD
            $id_usd = MasterCoin::getIdCoin("USDV");
            $id_eth = MasterCoin::getIdCoin("ETH");
            if ($dt->id_product_mining != null) {
                $dt->price = $dt->master_price;
                $dt->id_coin_reward = $dt->master_id_coin_reward;
                $dt->tenor = $dt->master_tenor;
            } else {
                $dt->product_name = "Custom";
                $dt->product_type = "Custom Mining";
            }
            $dt->reward_coin = MasterCoin::getKodeCoin($dt->id_coin_reward);

            //GET VALUE MHS TO USD
            // $mhs_to_usd = SettingGeneral::where("setting_name", "MHS_" . $dt->reward_coin . "_PER_DAY")->first();
            // $can_harvest = (($mhs_to_usd->value / 24 / 60) * $dt->mining_speed) * $selisih_date_harvest;
            // echo $selisih_date_harvest . "\n";
            $can_harvest = $dt->can_harvest;
            // if ($dt->reward_coin != "AETHER") {
            //     $can_harvest = PriceCoin::convertCoin($id_usd, $dt->id_coin_reward, $can_harvest);
            // }
            $fee = PriceCoin::convertCoin($id_usd, $dt->id_coin_reward, $fee->value);
            $can_harvest = $can_harvest - $fee;
            if ($can_harvest < 0) {
                $can_harvest = 0;
            }
            $dt->can_harvest = $can_harvest;
            $dt->fee = $fee;
        }

        $id_coin_usd = MasterCoin::getIdCoin("USDV");
        $feeOriginal = SettingFee::where("fee_name", "HARVEST_FEE")->first()->value;
        foreach ($dataStaking as $dt) {
            $data_master_staking = MasterStaking::getStakingData($dt->id_product_staking);

            //HITUNG FEE HARVEST
            $fee = PriceCoin::convertCoin($id_coin_usd, $data_master_staking->id_coin_reward, $feeOriginal);
            $dt->fee = $fee;

            //HITUNG PROFIT YANG DAPAT DIHARVEST
            $selisih = CommonHelper::getSelisihMinutes($dt->harvest_at, $date_now);
            $profit = $data_master_staking->profit;
            $profit = floatval(($profit / 100) / 30 / 24 / 60) * $selisih;

            $hasil_farming = $profit * $dt->amount_stake;

            //POTONG SHARING PROFIT
            $sp = SharingProfit::getSharingProfit($data_master_staking->id_coin_reward, $dt->amount_stake);
            $sharing_profit = $hasil_farming * ($sp->sharing_profit / 100);
            $hasil_farming = $hasil_farming - $sharing_profit;
            $dt->can_harvest = $hasil_farming;

            //HITUNG PROGRESS STAKING
            $created_at = CommonHelper::convertDateFormat($dt->created_at);
            $total_hari = CommonHelper::getSelisihDay($created_at, $dt->end_at);
            $progress = CommonHelper::getSelisihDay($created_at, $date_now);
            $progressBar = ($progress / $total_hari) * 100;
            if ($progressBar > 100) {
                $progressBar = 100;
            }
            $dt->progress_percentage = round($progressBar, 2);
            $selisih = CommonHelper::getSelisihDay($dt->harvest_at, $date_now);
            $dt->selisih_harvest = $selisih;

            $dt->created_at_format = CommonHelper::convertDateFormatFrontend($dt->created_at);
            $dt->end_at_format = CommonHelper::convertDateFormatFrontend($dt->end_at);
            $dt->harvest_at_format = CommonHelper::convertDateFormatFrontend($dt->harvest_at);
            //GET COIN REWARD
            $dt->coin_reward = MasterCoin::getKodeCoin($data_master_staking->id_coin_reward);
            $dt->url_coin = MasterCoin::getURLCoin($data_master_staking->id_coin_reward);
            $dt->product = $data_master_staking;
            $dt->product["APR"] = $dt->product->profit * $dt->product->tenor;
        }

        $coin = MasterCoin::get();
        foreach ($coin as $value => $c) {
            $data[$value] = $c;
            $data[$value]["saldo"] = User::getSaldo($c->code, $dataUser->id);
        }
        $dataUser->balance = $data;


        $data = new stdClass();
        $data->user = $dataUser;
        $data->mining = $dataMining;
        $data->staking = $dataStaking;
        $data->history_harvest_staking = $dataHarvestStaking;
        $data->history_harvest_mining = $dataHarvestMining;
        return $this->createSuccessMessage($data);
    }

    function generateRandomString($length = 10)
    {
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }


    function test_input($data)
    {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }
}
