<?php

namespace App\Http\Controllers\WebServices;

use App\Helpers\CommonHelper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\LogTrxMining;
use App\Models\MasterCoin;
use App\Models\MasterMining;
use App\Models\PriceCoin;
use App\Models\SettingFee;
use App\Models\SettingGeneral;
use App\Models\TrxHistory;
use App\Models\TrxMining;
use App\Models\TrxReferal;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use stdClass;

class MiningWebServiceController extends Controller
{
    //
    public function add_master(Request $request)
    {
        $data = new MasterMining();
        $data->product_name = $request->product_name;
        $data->product_type = $request->product_type;
        $data->id_coin_reward = $request->id_coin_reward;
        $data->tenor = $request->tenor;
        $data->price = $request->price;
        $data->mining_speed = $request->mining_speed;
        $data->save();
        DB::commit();

        return $this->createSuccessMessage("Master Mining telah ditambahkan");
    }

    public function get_product(Request $request)
    {
        if (isset($request->id)) {
            $data = MasterMining::where("id", $request->id)->first();
            return $this->createSuccessMessage($data);
        }

        if ($request->type == "ETH") {
            $idcoin = MasterCoin::getIdCoin($request->type);
            $data = MasterMining::where("id_coin_reward", $idcoin)->where("status", 1)->get();
            $id_usdv = MasterCoin::getIdCoin("USDV");
            $mhs_eth_to_usd = SettingGeneral::where("setting_name", "MHS_ETH_PER_DAY")->first();
            foreach ($data as $dt) {
                $dt->earn_per_day = number_format(($mhs_eth_to_usd->value * $dt->mining_speed), 2, ".", ",");
                //CONVERT PROFIT USD TO KOIN REWARD
                // $income_mining = $mhs_eth_to_usd->value * $dt->mining_speed;
                // $dt->earn_per_day = PriceCoin::convertCoin($id_usdv, $dt->id_coin_reward, $income_mining);
            }
            return $this->createSuccessMessage($data);
        } else if ($request->type == "RVN") {
            $idcoin = MasterCoin::getIdCoin($request->type);
            $data = MasterMining::where("id_coin_reward", $idcoin)->where("status", 1)->get();
            $id_usdv = MasterCoin::getIdCoin("USDV");
            $mhs_rvn_to_usd = SettingGeneral::where("setting_name", "MHS_RVN_PER_DAY")->first();
            foreach ($data as $dt) {
                $dt->earn_per_day = number_format(($mhs_rvn_to_usd->value * $dt->mining_speed), 2, ".", ",");
                //CONVERT PROFIT USD TO KOIN REWARD
                // $income_mining = $mhs_eth_to_usd->value * $dt->mining_speed;
                // $dt->earn_per_day = PriceCoin::convertCoin($id_usdv, $dt->id_coin_reward, $income_mining);
            }
            return $this->createSuccessMessage($data);
        } else if (!isset($request->type)) {
            $data = MasterMining::join("master_coin", "master_mining.id_coin_reward", "master_coin.id")
                ->select("master_mining.*", "master_coin.code")
                ->get();
            return $this->createSuccessMessage($data);
        }
    }

    public function start_mining(Request $request)
    {
        //CEK USER LOGIN
        if (!Auth::check()) {
            return $this->createErrorMessage("Please Login first", 400);
        }
        $user = Auth::user();

        //CEK PRODUK APAKAH ADA
        $data_master_mining = MasterMining::getData($request->id_product_mining);
        if (!isset($data_master_mining)) {
            return $this->createErrorMessage("Product Mining not found", 400);
        }

        $kode_coin = MasterCoin::getKodeCoin($request->id_coin_currency);
        $saldo = User::getSaldo($kode_coin, $user->id);
        $price = $data_master_mining->price;
        if ($kode_coin != "USDV") {
            $id_usdv = MasterCoin::getIdCoin("USDV");
            $price = PriceCoin::convertCoin($id_usdv, $request->id_coin_currency, $price);
        }

        if ($saldo < $price) {
            return $this->createErrorMessage("Insufficient Balance", 400);
        }

        //CEK REFERAL AKTIF ATAU TIDAK
        $min_value_referal = SettingGeneral::where("setting_name", "MIN_VALUE_REFERAL_USD")->first()->value;
        $id_usd = MasterCoin::getIdCoin("USDV");
        $min_value_referal = PriceCoin::convertCoin($id_usd, $request->id_coin_currency, $min_value_referal);

        //HITUNG DATE END MINING
        $date = date("Y-m-d H:i:s");
        $time = strtotime($date);
        $end_date = date("Y-m-d H:i:s", strtotime("+" . $data_master_mining->tenor . " month", $time));

        $trx_mining = new TrxMining();
        $trx_mining->id_user = $user->id;
        $trx_mining->id_product_mining = $request->id_product_mining;
        $trx_mining->status = 0;
        $trx_mining->end_at = $end_date;
        $trx_mining->mining_speed = $data_master_mining->mining_speed;
        $trx_mining->last_harvest = $date;
        if ($price >= $min_value_referal) {
            $trx_mining->referal_status = 1;
        } else {
            $trx_mining->referal_status = 0;
        }
        $trx_mining->save();

        //CREATE TRX REFERAL MINING UNTUK PARENT
        if ($price >= $min_value_referal && isset($user->referal_parent)) {
            //GET MAX LEVEL REFERAL DARI DATABASE
            $level_max_referal = SettingGeneral::where("setting_name", "MAX_LEVEL_REFERAL")->first()->value;
            $data_user = $user;
            for ($i = 0; $i < $level_max_referal; $i++) {
                if (isset($data_user->referal_parent)) {
                    //GET REFERAL KODE PARENT
                    $data_parent = User::getDataFromReferal($data_user->referal_parent);
                    //INSERT DATA REFERAL
                    TrxReferal::insertData($data_parent->id, $trx_mining->id, "REFERAL_MINING", $date, $i + 1);
                    $data_user = $data_parent;
                } else {
                    $i = $level_max_referal;
                }
            }
        }

        //CREATE HISTORY TRANSAKSI (PENGURANGAN SALDO)
        $history_from = new TrxHistory();
        $history_from->id_user = $user->id;
        $history_from->id_coin = $request->id_coin_currency;
        $history_from->id_ref = $trx_mining->id;
        $history_from->trx_type = "MINING_DEPOSIT";
        $history_from->status = 1;
        $history_from->total_fee = 0;
        $history_from->amount = $price * -1;
        $history_from->save();

        DB::commit();

        return $this->createSuccessMessage($trx_mining, "Mining started");
    }

    public function start_mining_custom(Request $request)
    {
        if (!Auth::check()) {
            return $this->createErrorMessage("Please Login First", 400);
        }

        $user = Auth::user();
        $mining_speed = $request->mining_speed;
        $type_coin_reward = $request->type_coin_reward;
        $tenor = $request->tenor;

        $id_coin_reward = MasterCoin::getIdCoin($type_coin_reward);

        //HITUNG PRICE BERDASARKAN SPEED
        if ($type_coin_reward != "AETHER") {
            $price_mhs = SettingGeneral::where("setting_name", "PRICE_MHS_USD")->first();
            $price_usd = $mining_speed * $price_mhs->value;
        } else {
            $price_mhs = SettingGeneral::where("setting_name", "PRICE_AETHER_USD")->first();
            $price_usd = $mining_speed * $price_mhs->value;
        }

        $kode_coin = MasterCoin::getKodeCoin($request->id_coin_currency);
        $saldo = User::getSaldo($kode_coin, $user->id);

        //CEK SALDO
        if ($kode_coin != "USDV") {
            //CONVERT KE CURRENCY PILIHAN WALLET
            $id_usdv = MasterCoin::getIdCoin("USDV");
            $price = PriceCoin::convertCoin($id_usdv, $request->id_coin_currency, $price_usd);
        } else {
            $price = $price_usd;
        }

        if ($saldo < $price) {
            return $this->createErrorMessage("Insufficient Balance, price : " . $price, 400);
        }

        //CEK REFERAL AKTIF ATAU TIDAK
        $min_value_referal = SettingGeneral::where("setting_name", "MIN_VALUE_REFERAL_USD")->first()->value;
        $id_usd = MasterCoin::getIdCoin("USDV");
        $min_value_referal = PriceCoin::convertCoin($id_usd, $request->id_coin_currency, $min_value_referal);

        //HITUNG DATE END MINING
        $date = date("Y-m-d H:i:s");
        $time = strtotime($date);
        $end_date = date("Y-m-d H:i:s", strtotime("+" . $tenor . " month", $time));

        $trx_mining = new TrxMining();
        $trx_mining->id_user = $user->id;
        $trx_mining->id_product_mining = null;
        $trx_mining->price = $price_usd;
        $trx_mining->id_coin_reward = $id_coin_reward;
        $trx_mining->tenor = $tenor;
        $trx_mining->status = 0;
        $trx_mining->end_at = $end_date;
        $trx_mining->mining_speed = $mining_speed;
        $trx_mining->last_harvest = $date;
        if ($price >= $min_value_referal) {
            $trx_mining->referal_status = 1;
        } else {
            $trx_mining->referal_status = 0;
        }
        $trx_mining->save();

        //CREATE TRX REFERAL MINING UNTUK PARENT
        if ($price >= $min_value_referal && isset($user->referal_parent)) {
            //GET MAX LEVEL REFERAL DARI DATABASE
            $level_max_referal = SettingGeneral::where("setting_name", "MAX_LEVEL_REFERAL")->first()->value;
            $data_user = $user;
            for ($i = 0; $i < $level_max_referal; $i++) {
                if (isset($data_user->referal_parent)) {
                    //GET REFERAL KODE PARENT
                    $data_parent = User::getDataFromReferal($data_user->referal_parent);
                    //INSERT DATA REFERAL
                    TrxReferal::insertData($data_parent->id, $trx_mining->id, "REFERAL_MINING", $date, $i + 1);
                    $data_user = $data_parent;
                } else {
                    $i = $level_max_referal;
                }
            }
        }

        //CREATE HISTORY TRANSAKSI (PENGURANGAN SALDO)
        $history_from = new TrxHistory();
        $history_from->id_user = $user->id;
        $history_from->id_coin = $request->id_coin_currency;
        $history_from->id_ref = $trx_mining->id;
        $history_from->trx_type = "MINING_DEPOSIT";
        $history_from->status = 1;
        $history_from->total_fee = 0;
        $history_from->amount = $price * -1;
        $history_from->save();

        DB::commit();

        return $this->createSuccessMessage($trx_mining, "Mining started");
    }

    public function start_mining_custom_admin(Request $request)
    {
        if (!Auth::check()) {
            return $this->createErrorMessage("Please Login First", 400);
        }

        $user = Auth::user();
        if ($user->role != 2) {
            return $this->createErrorMessage("User Not Allowed", 400);
        }

        $user = User::where("id", $request->id_user)->first();
        $mining_speed = $request->mining_speed;
        $type_coin_reward = $request->type_coin_reward;
        $tenor = $request->tenor;

        //HITUNG PRICE BERDASARKAN SPEED
        if ($type_coin_reward != "AETHER") {
            $price_mhs = SettingGeneral::where("setting_name", "PRICE_MHS_USD")->first();
            $price_usd = $mining_speed * $price_mhs->value;
        } else {
            $price_mhs = SettingGeneral::where("setting_name", "PRICE_AETHER_USD")->first();
            $price_usd = $mining_speed * $price_mhs->value;
        }
        $id_coin_reward = MasterCoin::getIdCoin($type_coin_reward);


        $kode_coin = MasterCoin::getKodeCoin($request->id_coin_currency);
        $saldo = User::getSaldo($kode_coin, $user->id);

        //CEK SALDO
        if ($kode_coin != "USDV") {
            //CONVERT KE CURRENCY PILIHAN WALLET
            $id_usdv = MasterCoin::getIdCoin("USDV");
            $price = PriceCoin::convertCoin($id_usdv, $request->id_coin_currency, $price_usd);
        } else {
            $price = $price_usd;
        }

        if ($saldo < $price) {
            return $this->createErrorMessage("Insufficient Balance, price : " . $price, 400);
        }

        //CEK REFERAL AKTIF ATAU TIDAK
        $min_value_referal = SettingGeneral::where("setting_name", "MIN_VALUE_REFERAL_USD")->first()->value;
        $id_usd = MasterCoin::getIdCoin("USDV");
        $min_value_referal = PriceCoin::convertCoin($id_usd, $request->id_coin_currency, $min_value_referal);

        //HITUNG DATE END MINING
        $date = date("Y-m-d H:i:s");
        $time = strtotime($date);
        $end_date = date("Y-m-d H:i:s", strtotime("+" . $tenor . " month", $time));

        $trx_mining = new TrxMining();
        $trx_mining->id_user = $user->id;
        $trx_mining->id_product_mining = null;
        $trx_mining->price = $price_usd;
        $trx_mining->id_coin_reward = $id_coin_reward;
        $trx_mining->tenor = $tenor;
        $trx_mining->status = 0;
        $trx_mining->end_at = $end_date;
        $trx_mining->mining_speed = $mining_speed;
        $trx_mining->last_harvest = $date;
        if ($price >= $min_value_referal) {
            $trx_mining->referal_status = 1;
        } else {
            $trx_mining->referal_status = 0;
        }
        $trx_mining->save();

        //CREATE TRX REFERAL MINING UNTUK PARENT
        if ($price >= $min_value_referal && isset($user->referal_parent)) {
            //GET MAX LEVEL REFERAL DARI DATABASE
            $level_max_referal = SettingGeneral::where("setting_name", "MAX_LEVEL_REFERAL")->first()->value;
            $data_user = $user;
            for ($i = 0; $i < $level_max_referal; $i++) {
                if (isset($data_user->referal_parent)) {
                    //GET REFERAL KODE PARENT
                    $data_parent = User::getDataFromReferal($data_user->referal_parent);
                    //INSERT DATA REFERAL
                    TrxReferal::insertData($data_parent->id, $trx_mining->id, "REFERAL_MINING", $date, $i + 1);
                    $data_user = $data_parent;
                } else {
                    $i = $level_max_referal;
                }
            }
        }

        //CREATE HISTORY TRANSAKSI (PENGURANGAN SALDO)
        $history_from = new TrxHistory();
        $history_from->id_user = $user->id;
        $history_from->id_coin = $request->id_coin_currency;
        $history_from->id_ref = $trx_mining->id;
        $history_from->trx_type = "MINING_DEPOSIT";
        $history_from->status = 1;
        $history_from->total_fee = 0;
        $history_from->amount = $price * -1;
        $history_from->save();

        DB::commit();

        return $this->createSuccessMessage($trx_mining, "Mining started");
    }

    public function list_mining()
    {
        //CEK USER LOGIN
        if (!Auth::check()) {
            return $this->createErrorMessage("Please Login first", 400);
        }
        $user = Auth::user();
        $data = TrxMining::leftJoin("master_mining", "trx_mining.id_product_mining", "=", "master_mining.id")
            ->where("trx_mining.id_user", $user->id)
            ->where("trx_mining.status", 0)
            ->select("trx_mining.*", "master_mining.id_coin_reward as master_id_coin_reward", "master_mining.product_name", "master_mining.product_type", "master_mining.tenor as master_tenor", "master_mining.price as master_price")
            ->get();

        $date_now = date("Y-m-d H:i:s");
        foreach ($data as $dt) {
            //HITUNG PROGRESS MINING
            $created_at = CommonHelper::convertDateFormat($dt->created_at);
            $total_hari = CommonHelper::getSelisihDay($created_at, $dt->end_at);
            $progress = CommonHelper::getSelisihDay($created_at, $date_now);
            $progressBar = ($progress / $total_hari) * 100;
            if ($progressBar > 100) {
                $progressBar = 100;
            }
            $dt->progress_percentage = round($progressBar, 2);

            $dt->created_at_format = CommonHelper::convertDateFormatFrontend($dt->created_at);
            $dt->end_at_format = CommonHelper::convertDateFormatFrontend($dt->end_at);
            $dt->last_harvest_format = CommonHelper::convertDateFormatFrontend($dt->last_harvest);
            $dt->selisih = CommonHelper::getSelisihDay(CommonHelper::convertDateFormat($dt->last_harvest), date("Y-m-d"));

            // $selisih_date_harvest = CommonHelper::getSelisihMinutes($dt->last_harvest, $date_now);

            //CONVERT PROFIT USD TO KOIN REWARD
            $id_usd = MasterCoin::getIdCoin("USDV");
            $id_eth = MasterCoin::getIdCoin("ETH");
            if ($dt->id_product_mining != null) {
                $dt->price = $dt->master_price;
                $dt->id_coin_reward = $dt->master_id_coin_reward;
                $dt->tenor = $dt->master_tenor;
            } else {
                $dt->product_name = "Custom";
                $dt->product_type = "Custom Mining";
            }
            $dt->reward_coin = MasterCoin::getKodeCoin($dt->id_coin_reward);

            //NO FEE UNTUK AETHER
            if ($dt->reward_coin != "AETHER") {
                $fee = SettingFee::getValue("MINING_FEE");
            } else {
                $fee = new stdClass();
                $fee->value = 0;
            }

            //GET VALUE MHS TO USD
            // $mhs_to_usd = SettingGeneral::where("setting_name", "MHS_" . $dt->reward_coin . "_PER_DAY")->first();
            // $can_harvest = (($mhs_to_usd->value / 24 / 60) * $dt->mining_speed) * $selisih_date_harvest;
            // echo $selisih_date_harvest . "\n";

            // if ($dt->reward_coin != "AETHER") {
            //     $can_harvest = PriceCoin::convertCoin($id_usd, $dt->id_coin_reward, $can_harvest);
            // }

            $fee = PriceCoin::convertCoin($id_usd, $dt->id_coin_reward, $fee->value);
            // $dt->can_harvest = $can_harvest;
            $dt->fee = $fee;
            //MIN WITHDRAW 0.2 ETH
            //UNTUK AETHER NO MIN WITHDRAW
            if ($dt->reward_coin != "AETHER") {
                $dt->min_withdraw = PriceCoin::convertCoin($id_eth, $dt->id_coin_reward, 0.002);
            } else {
                $dt->min_withdraw = 0;
            }
        }

        return $this->createSuccessMessage($data);
    }

    public function harvest(Request $request)
    {
        //CEK USER LOGIN
        if (!Auth::check()) {
            return $this->createErrorMessage("Please Login first", 400);
        }
        $user = Auth::user();

        $data_trx_mining = TrxMining::where("id", $request->id_trx_mining)
            ->where("status", 0)
            ->where("id_user", $user->id)
            ->first();
        if (!isset($data_trx_mining)) {
            return $this->createErrorMessage("Transaction Not found", 400);
        }

        $date_now = date("Y-m-d H:i:s");
        if ($data_trx_mining->id_product_mining != null) {
            $data_product = MasterMining::getData($data_trx_mining->id_product_mining);
        } else {
            $data_product = new stdClass();
            $data_product->id_coin_reward = $data_trx_mining->id_coin_reward;
        }
        $selisih_date_harvest = CommonHelper::getSelisihMinutes($data_trx_mining->last_harvest, $date_now);
        if ($selisih_date_harvest <= 0) {
            return $this->createErrorMessage("Mining belum bisa di harvest", 400);
        }

        $kode_coin = MasterCoin::getKodeCoin($data_product->id_coin_reward);

        //GET VALUE MHS TO USD
        // $mhs_to_usd = SettingGeneral::where("setting_name", "MHS_" . $kode_coin . "_PER_DAY")->first();
        // $income_mining = (($mhs_to_usd->value / 24 / 60) * $data_trx_mining->mining_speed) * $selisih_date_harvest;
        $income_mining = $data_trx_mining->can_harvest;

        $id_usd = MasterCoin::getIdCoin("USDV");
        //NO FEE UNTUK AETHER
        if ($kode_coin != "AETHER") {
            //GET FEE HARVEST MINING
            $fee = SettingFee::getValue("MINING_FEE");
            $fee = PriceCoin::convertCoin($id_usd, $data_product->id_coin_reward, $fee->value);
            $income_mining = $income_mining - $fee;
            if ($income_mining <= 0) {
                return $this->createErrorMessage("Insufficient Mining Fee", 400);
            }
        } else {
            $fee = 0;
        }

        //CONVERT PROFIT USD TO KOIN REWARD
        // $income_mining = PriceCoin::convertCoin($id_usd, $data_product->id_coin_reward, $income_mining);

        //CREATE HISTORY TRANSAKSI (PENAMBAHAN SALDO)
        $history_from = new TrxHistory();
        $history_from->id_user = $data_trx_mining->id_user;
        $history_from->id_coin = $data_product->id_coin_reward;
        $history_from->id_ref = $data_trx_mining->id;
        $history_from->trx_type = "MINING_REWARD";
        $history_from->status = 1;
        $history_from->total_fee = $fee;
        $history_from->amount = $income_mining;
        $history_from->save();

        $data_trx_mining->last_harvest = $date_now;
        $data_trx_mining->can_harvest = 0;
        $data_trx_mining->save();

        DB::commit();
        return $this->createSuccessMessage($history_from, 200, "Harvest Mining Success");
    }

    public function detail(Request $request)
    {
        $id = $request->id;
        $data = TrxMining::join("users", "trx_mining.id_user", "users.id")
            ->where("trx_mining.id", $id)
            ->select("users.name as cust_name", "trx_mining.*")
            ->first();
        $data->history = TrxHistory::where("trx_history.id_ref", $id)
            ->join("master_coin", "master_coin.id", "trx_history.id_coin")
            ->where("trx_history.trx_type", "like", "MINING%")
            ->select("trx_history.*", "master_coin.code")
            ->get();
        $data->product = MasterMining::where("id", $data->id_product_mining)->first();
        if (!isset($data->product)) {
            $data->product = new stdClass();
            $data->product->price = $data->price;
            $data->product->product_name = "Custom Mining";
            $data->product->product_type = "Custom";
            $data->product->tenor = $data->tenor;
        }
        return $this->createSuccessMessage($data);
    }

    public function edit(Request $request)
    {
        $data = MasterMining::where("id", $request->id)->first();
        $data->product_name = $request->product_name;
        $data->product_type = $request->product_type;
        $data->id_coin_reward = $request->id_coin_reward;
        $data->tenor = $request->tenor;
        $data->price = $request->price;
        $data->mining_speed = $request->mining_speed;
        $data->status = $request->status;
        $data->save();

        return $this->createSuccessMessage($data);
    }

    public function logmining(Request $request)
    {
        $id = $request->id;
        $data = LogTrxMining::where("id_trx_mining", $id)->get();
        $total = 0;
        foreach ($data as $dt) {
            $total = $total + $dt->earning;
            $dt->total = $total;
        }
        return $this->createSuccessMessage($data);
    }


    public function edit_mhs(Request $request)
    {
        $data = TrxMining::where("id", $request->id)->first();
        $data->mining_speed = $request->speed;
        $data->save();

        return $this->createSuccessMessage($data);
    }
}
