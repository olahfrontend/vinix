<?php

namespace App\Http\Controllers\WebServices;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\MasterCoin;
use App\Models\PriceCoin;
use App\Models\SettingFee;
use App\Models\TrxDeposit;
use App\Models\TrxExchange;
use App\Models\TrxHistory;
use App\Models\TrxMining;
use App\Models\TrxStaking;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class TrxExchangeWebServiceController extends Controller
{
    //
    public function exchange(Request $request)
    {
        if (!Auth::check()) {
            return $this->createErrorMessage("Please Login first", 400);
        }

        $user = Auth::user();

        //AMBIL PARAMETER
        $from_coin = $request->from_coin;
        $to_coin = $request->to_coin;
        $from_amount = $request->amount;

        //GET ID KOIN
        $id_from_coin = MasterCoin::getIdCoin($from_coin);
        $id_to_coin = MasterCoin::getIdCoin($to_coin);

        //CEK KOIN ADA GAK
        if (!isset($id_from_coin) || !isset($id_to_coin)) {
            return $this->createErrorMessage("Koin tidak ada", 400);
        }

        //CEK UNTUK PERTUKARAN VINIX HANYA BOLEH MENGGUNAKAN USD DAN SEBALIKNYA
        // if ($to_coin == "VNX") {
        //     if ($from_coin != "USD") {
        //         return $this->createErrorMessage("Exchange Vinix hanya bisa digunakan pada currency USD", 400);
        //     }
        // }
        // if ($from_coin == "VNX") {
        //     if ($to_coin != "USD") {
        //         return $this->createErrorMessage("Exchange Vinix hanya bisa digunakan pada currency USD", 400);
        //     }
        // }

        //GET HARGA
        $price_coin_from = PriceCoin::getPrice($from_coin)->price;
        $price_coin_to = PriceCoin::getPrice($to_coin)->price;

        // print_r($price_coin_from);

        //GET SALDO
        $saldo = User::getSaldo($from_coin, $user->id);

        //CEK SALDO CUKUP TIDAK
        if ($saldo < $from_amount) {
            return $this->createErrorMessage("Saldo tidak cukup", 400);
        }

        //HITUNG RATE
        $rate = $price_coin_from / $price_coin_to;

        //GET EXCHANGE FEE
        $id_coin_usd = MasterCoin::getIdCoin("USDV");
        $fee = SettingFee::getValue("EXCHANGE FEE");
        $fee = PriceCoin::convertCoin($id_coin_usd, $id_to_coin, $fee->value);
        if ($fee > ($from_amount * $rate)) {
            return $this->createErrorMessage("Insufficient exchange fee", 400);
        }

        // //CREATE TRANSAKSI EXCHANGE
        $trans = new TrxExchange();
        $trans->id_user = $user->id;
        $trans->from_coin = $id_from_coin;
        $trans->to_coin = $id_to_coin;
        $trans->status = 1;
        $trans->fee = $fee;
        $trans->rate = $rate;
        $trans->from_amount = $from_amount;
        $from_amount = $from_amount;
        $trans->to_amount = ($from_amount * $rate) - $fee;
        $trans->save();

        //CREATE HISTORY EXCHANGE DARI (COIN ASAL)
        $history_from = new TrxHistory();
        $history_from->id_user = $user->id;
        $history_from->id_coin = $id_from_coin;
        $history_from->id_ref = $trans->id;
        $history_from->trx_type = "EXCHANGE";
        $history_from->status = 1;
        $history_from->total_fee = 0;
        $history_from->amount = $from_amount * -1;
        $history_from->save();

        //CREATE HISTORY EXCHANGE KE (COIN TUJUAN)
        $history_to = new TrxHistory();
        $history_to->id_user = $user->id;
        $history_to->id_coin = $id_to_coin;
        $history_to->id_ref = $trans->id;
        $history_to->trx_type = "EXCHANGE";
        $history_to->status = 1;
        $history_to->total_fee = $fee;
        $history_to->amount = ($from_amount * $rate) - $fee;
        $history_to->save();

        return $this->createSuccessMessage($trans, 200, "Exchange Success");
    }

    public function checkRateExchange(Request $request)
    {
        $from_coin = $request->from_coin;
        $to_coin = $request->to_coin;

        $price_from = PriceCoin::getPrice($from_coin);
        $price_to = PriceCoin::getPrice($to_coin);
        if (!isset($price_from) || !isset($price_to)) {
            return $this->createErrorMessage("Coin not exist", 400);
        }

        $rate = $price_from->price / $price_to->price;
        $data["rate"] = $rate;
        $data["currency"] = $to_coin;
        return $this->createSuccessMessage($data);
    }

    public function getListWithdraw()
    {
        if (!Auth::check()) {
            return $this->createErrorMessage("Please Login first", 400);
        }

        $user = Auth::user();
        if ($user->role == 1) {
            $data = TrxHistory::join("master_coin", "master_coin.id", "trx_history.id_coin")
                ->select("master_coin.code", "trx_history.*")
                ->where("trx_history.id_user", $user->id)
                ->orderBy("trx_history.created_at", "desc")
                ->get();

            return $this->createSuccessMessage($data);
        } else if ($user->role == 2) {
            $data = TrxDeposit::join("users", "users.id", "trx_deposit.id_user")
                ->join("master_coin", "master_coin.id", "trx_deposit.id_coin")
                ->where("trx_deposit.trx_type", "WITHDRAW")
                ->where("verified", 1)
                ->select("users.name", "users.email", "master_coin.code", "trx_deposit.*")
                ->get();
            return $this->createSuccessMessage($data);
        }
    }

    public function confirmWithdraw(Request $request)
    {

        if (!Auth::check()) {
            return $this->createErrorMessage("Please Login first", 400);
        }

        $user = Auth::user();
        if ($user->role != 2) {
            return $this->createErrorMessage("User Not Allowed", 400);
        }
        $id = $request->id;
        $status = $request->status;

        $data1 = TrxDeposit::where("id", $id)->first();
        if (!isset($data1)) {
            return $this->createErrorMessage("Transaction not found", 400);
        }
        $data1->status = $status;
        $data1->txn_hash = $request->txn_hash;
        $data1->save();

        if ($status == -1) {
            $data = new TrxHistory();
            $data["id_user"] = $data1->id_user;
            $data["id_coin"] = $data1->id_coin;
            $data["id_ref"] = $data1->id;
            $data["trx_type"] = "REFUND_WITHDRAW";
            $data["status"] = 1;
            $data["total_fee"] = 0;
            $data["amount"] = $data1->amount + $data1->total_fee;
            $data->save();
        }

        return $this->createSuccessMessage($data1, 200);
    }

    public function detail_history(Request $request)
    {
        $ref = $request->ref;
        $id = $request->id;

        $type = $ref;
        if (str_contains($ref, "MINING")) {
            $type = "MINING";
        } else if (str_contains($ref, "STAKING")) {
            $type = "STAKING";
        } else if (str_contains($ref, "EXCHANGE")) {
            $type = "EXCHANGE";
        }

        if ($type == "MINING") {
            $data = TrxMining::where("id", $id)
                ->first();
            $data->detail = TrxHistory::where("trx_history.id_ref", $id)
                ->join("master_coin", "master_coin.id", "trx_history.id_coin")
                ->where("trx_history.trx_type", "like", "%MINING%")
                ->select("trx_history.*", "master_coin.code")
                ->get();
        } else if ($type == "STAKING") {
            $data = TrxStaking::where("id", $id)
                ->first();
            $data->detail = TrxHistory::where("trx_history.id_ref", $id)
                ->join("master_coin", "master_coin.id", "trx_history.id_coin")
                ->where("trx_history.trx_type", "like", "%STAKING%")
                ->select("trx_history.*", "master_coin.code")
                ->get();
        } else if ($type == "EXCHANGE") {
            $data = TrxExchange::where("id", $id)
                ->first();
            $data->from_coin = MasterCoin::getKodeCoin($data->from_coin);
            $data->to_coin = MasterCoin::getKodeCoin($data->to_coin);
            $data->detail = TrxHistory::where("trx_history.id_ref", $id)
                ->join("master_coin", "master_coin.id", "trx_history.id_coin")
                ->where("trx_history.trx_type", "like", "%EXCHANGE%")
                ->select("trx_history.*", "master_coin.code")
                ->get();
        } else if ($type == "DEPOSIT" || $type = "WITHDRAW") {
            $data = TrxDeposit::where("id", $id)
                ->first();
            $data->id_coin = MasterCoin::getKodeCoin($data->id_coin);
            $data->detail = TrxHistory::where("trx_history.id_ref", $id)
                ->join("master_coin", "master_coin.id", "trx_history.id_coin")
                ->where("trx_history.trx_type", "WITHDRAW")
                ->orWhere("trx_history.trx_type", "DEPOSIT")
                ->where("trx_history.id_ref", $id)
                ->orWhere("trx_history.trx_type", "REFUND_WITHDRAW")
                ->where("trx_history.id_ref", $id)
                ->select("trx_history.*", "master_coin.code")
                ->get();
        }

        $data->type_ref = $type;

        return $this->createSuccessMessage($data);
    }
}
