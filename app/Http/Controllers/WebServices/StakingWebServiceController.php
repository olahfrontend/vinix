<?php

namespace App\Http\Controllers\WebServices;

use App\Helpers\CommonHelper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\MasterCoin;
use App\Models\MasterStaking;
use App\Models\PriceCoin;
use App\Models\SettingFee;
use App\Models\SettingGeneral;
use App\Models\SharingProfit;
use App\Models\TrxHistory;
use App\Models\TrxReferal;
use App\Models\TrxStaking;
use App\Models\User;
use DateTime;
use Illuminate\Support\Facades\Auth;
use stdClass;

class StakingWebServiceController extends Controller
{
    //
    public function add_staking(Request $request)
    {
        $data = new MasterStaking();
        $data->product_name = $request->product_name;
        $data->id_coin_stake = $request->id_coin_stake;
        $data->id_coin_reward = $request->id_coin_reward;
        $data->tenor = $request->tenor;
        $data->comitment_fee = $request->comitment_fee;
        $data->profit = $request->profit;
        $data->save();

        return $this->createSuccessMessage($data, 200, "Add Master Staking berhasil");
    }

    public function get_product_staking(Request $request)
    {
        if (isset($request->id)) {
            $data = MasterStaking::where("id", $request->id)->first();
            return $this->createSuccessMessage($data);
        } else {
            $data = MasterStaking::where("status", 1)->get();
        }
        foreach ($data as $dt) {
            $dt["APR"] = $dt->profit * 12;
            $dt["coin_stake"] = MasterCoin::getKodeCoin($dt->id_coin_stake);
            $dt["coin_reward"] = MasterCoin::getKodeCoin($dt->id_coin_reward);
            $dt["url_coin"] = MasterCoin::getURLCoin($dt->id_coin_reward);
        }
        return $this->createSuccessMessage($data);
    }

    public function farm_staking(Request $request)
    {
        if (!Auth::check()) {
            return $this->createErrorMessage("Please Login first", 400);
        }

        $user = Auth::user();

        $id_product_staking = $request->id_product;
        $amount = $request->amount;

        //CEK PRODUK
        $data_product = MasterStaking::getStakingData($id_product_staking);
        if (!isset($data_product)) {
            return $this->createErrorMessage("Product not found", 400);
        }

        //CEK SALDO
        $kode_coin = MasterCoin::getKodeCoin($data_product->id_coin_stake);
        $saldo = User::getSaldo($kode_coin, $user->id);
        if ($saldo < $amount) {
            return $this->createErrorMessage("Saldo tidak cukup", 400);
        }

        //CEK REFERAL AKTIF ATAU TIDAK
        $min_value_referal = SettingGeneral::where("setting_name", "MIN_VALUE_REFERAL_USD")->first()->value;
        $id_usd = MasterCoin::getIdCoin("USDV");
        $min_value_referal = PriceCoin::convertCoin($id_usd, $data_product->id_coin_stake, $min_value_referal);

        //HITUNG DATE END STAKING
        $date = date("Y-m-d H:i:s");
        $time = strtotime($date);
        $end_date = date("Y/m/d", strtotime("+" . $data_product->tenor . " month", $time));

        //CREATE TRX STAKING
        $data = new TrxStaking();
        $data->id_user = $user->id;
        $data->id_product_staking = $id_product_staking;
        $data->amount_stake = $amount;
        $data->harvest_at = $date;
        $data->end_at = $end_date;
        if ($amount >= $min_value_referal) {
            $data->referal_status = 1;
        } else {
            $data->referal_status = 0;
        }
        $data->save();

        //CREATE TRX REFERAL STAKING UNTUK PARENT
        if ($amount >= $min_value_referal && isset($user->referal_parent)) {
            //GET MAX LEVEL REFERAL DARI DATABASE
            $level_max_referal = SettingGeneral::where("setting_name", "MAX_LEVEL_REFERAL")->first()->value;
            $data_user = $user;
            for ($i = 0; $i < $level_max_referal; $i++) {
                if (isset($data_user->referal_parent)) {
                    //GET REFERAL KODE PARENT
                    $data_parent = User::getDataFromReferal($data_user->referal_parent);
                    //INSERT DATA REFERAL
                    TrxReferal::insertData($data_parent->id, $data->id, "REFERAL_STAKING", $date, $i + 1);
                    $data_user = $data_parent;
                } else {
                    $i = $level_max_referal;
                }
            }
        }

        // //CREATE HISTORY TRANSAKSI (PENGURANGAN SALDO)
        $history_from = new TrxHistory();
        $history_from->id_user = $user->id;
        $history_from->id_coin = $data_product->id_coin_stake;
        $history_from->id_ref = $data->id;
        $history_from->trx_type = "STAKING_DEPOSIT";
        $history_from->status = 1;
        $history_from->total_fee = 0;
        $history_from->amount = $amount * -1;
        $history_from->save();

        return $this->createSuccessMessage($data, 200, "Berhasil Staking");
    }

    public function harvest(Request $request)
    {
        if (!Auth::check()) {
            return $this->createErrorMessage("Please Login first", 400);
        }
        $user = Auth::user();

        $id_trx_staking = $request->id_trx_staking;

        //CEK APAKAH STAKING ADA
        $data_trx_staking = TrxStaking::getDetailStaking($user->id, $id_trx_staking);
        if (!isset($data_trx_staking)) {
            return $this->createErrorMessage("Staking not exist", 400);
        }

        if ($data_trx_staking->status == 1) {
            return $this->createErrorMessage("Staking already finished", 400);
        }
        $date_now = date("Y-m-d h:i:s");
        // $date_last_harvest = CommonHelper::convertDateFormat($data_trx_staking->harvest_at);
        $date_end = CommonHelper::convertDateFormat($data_trx_staking->end_at);

        // $selisih_date_harvest = CommonHelper::getSelisihDay($date_last_harvest, $date_now);
        $selisih_minute_harvest = CommonHelper::getSelisihMinutes($data_trx_staking->harvest_at, $date_now);
        $selisih_date_end = CommonHelper::getSelisihDay($date_end, $date_now);

        // echo $selisih_minute_harvest;

        if ($selisih_date_end < 0) {
            //STAKING BELUM SELESAI
            if ($selisih_minute_harvest <= 0) {
                return $this->createErrorMessage("Staking belum dapat diharvest", 400);
            } else {
                //DAPAT DIHARVEST 
                $data_master_staking = MasterStaking::getStakingData($data_trx_staking->id_product_staking);

                //GET FEE
                $id_coin_usd = MasterCoin::getIdCoin("USDV");
                $fee = SettingFee::where("fee_name", "HARVEST_FEE")->first()->value;
                $fee = PriceCoin::convertCoin($id_coin_usd, $data_master_staking->id_coin_reward, $fee);

                //HITUNG HASIL
                $profit = $data_master_staking->profit;
                $profit = floatval(($profit / 100) / 30 / 24 / 60) * $selisih_minute_harvest;

                $hasil_farming = $data_trx_staking->amount_stake * $profit;

                //POTONG SHARING PROFIT
                $sp = SharingProfit::getSharingProfit($data_master_staking->id_coin_reward, $data_trx_staking->amount_stake);
                $sharing_profit = $hasil_farming * ($sp->sharing_profit / 100);
                $hasil_farming = $hasil_farming - $sharing_profit;

                //POTONG FEE
                $hasil_farming = $hasil_farming - $fee;

                if ($hasil_farming <= 0) {
                    //SALDO HASIL TIDAK CUKUP UNTUK BAYAR FEE
                    return $this->createErrorMessage("Insufficient fund", 400);
                }

                //CREATE TRANSAKSI HISTORY
                $history = new TrxHistory();
                $history->id_user = $user->id;
                $history->id_coin = $data_master_staking->id_coin_reward;
                $history->id_ref = $data_trx_staking->id;
                $history->trx_type = "STAKING_HARVEST";
                $history->status = 1;
                $history->total_fee = $fee + $sharing_profit;
                $history->amount = $hasil_farming;
                $history->save();

                //UPDATE LAST HARVEST TRANSAKSI STAKING
                $data_trx = TrxStaking::where("id", $request->id_trx_staking)->first();
                $data_trx->harvest_at = $date_now;
                $data_trx->save();

                return $this->createSuccessMessage($history, 200, "Success Harvest");
            }
        } else {
            //SUDAH LEWAT DATE END -> STAKING SELESAI
            $data_master_staking = MasterStaking::getStakingData($data_trx_staking->id_product_staking);

            //GET FEE
            $id_coin_usd = MasterCoin::getIdCoin("USDV");
            $fee = SettingFee::where("fee_name", "HARVEST_FEE")->first()->value;
            $fee = PriceCoin::convertCoin($id_coin_usd, $data_master_staking->id_coin_reward, $fee);

            //HITUNG HASIL
            $profit = $data_master_staking->profit;
            $profit = floatval(($profit / 100) / 30 / 24 / 60) * $selisih_minute_harvest;

            $hasil_farming = $data_trx_staking->amount_stake * $profit;

            //POTONG SHARING PROFIT
            $sp = SharingProfit::getSharingProfit($data_master_staking->id_coin_reward, $data_trx_staking->amount_stake);
            $sharing_profit = $hasil_farming * ($sp->sharing_profit / 100);
            $hasil_farming = $hasil_farming - $sharing_profit;

            // POTONG FEE HARVEST
            $hasil_farming = $hasil_farming - $fee;

            if ($hasil_farming <= 0) {
                //SALDO HASIL TIDAK CUKUP UNTUK BAYAR FEE
                return $this->createErrorMessage("Insufficient fund", 400);
            }

            //CREATE TRANSAKSI HISTORY HARVEST
            $history = new TrxHistory();
            $history->id_user = $user->id;
            $history->id_coin = $data_master_staking->id_coin_reward;
            $history->id_ref = $data_trx_staking->id;
            $history->trx_type = "STAKING_HARVEST";
            $history->status = 1;
            $history->total_fee = $fee + $sharing_profit;
            $history->amount = $hasil_farming;
            $history->save();

            //CREATE TRANSAKSI HISTORY WITHDRAW STAKING
            $history2 = new TrxHistory();
            $history2->id_user = $user->id;
            $history2->id_coin = $data_master_staking->id_coin_reward;
            $history2->id_ref = $data_trx_staking->id;
            $history2->trx_type = "STAKING_WITHDRAW";
            $history2->status = 1;
            $history2->total_fee = 0;
            $history2->amount = $data_trx_staking->amount_stake;
            $history2->save();

            //UPDATE TRANSAKSI STAKING STATUS = 1 -> SELESAI
            $data_trx = TrxStaking::where("id", $request->id_trx_staking)->first();
            $data_trx->harvest_at = date("Y/m/d");
            $data_trx->status = 1;
            $data_trx->save();

            return $this->createSuccessMessage($history, 200, "Success Harvest");
        }
    }

    public function myfarm()
    {
        $user = CommonHelper::checkLogin();
        if (!isset($user)) {
            return $this->createErrorMessage("Please Login first", 400);
        }

        //GET ACTIVE LIST STAKING
        $data = TrxStaking::where("id_user", $user->id)
            ->where("status", 0)
            ->get();

        //GET HARVEST FEE
        $id_coin_usd = MasterCoin::getIdCoin("USDV");
        $feeOriginal = SettingFee::where("fee_name", "HARVEST_FEE")->first()->value;
        $date_now = date("Y-m-d H:i:s");
        foreach ($data as $dt) {
            $data_master_staking = MasterStaking::getStakingData($dt->id_product_staking);

            //HITUNG FEE HARVEST
            $fee = PriceCoin::convertCoin($id_coin_usd, $data_master_staking->id_coin_reward, $feeOriginal);
            $dt->fee = $fee;

            //HITUNG PROFIT YANG DAPAT DIHARVEST
            $selisih = CommonHelper::getSelisihMinutes($dt->harvest_at, $date_now);
            $profit = $data_master_staking->profit;
            $profit = floatval(($profit / 100) / 30 / 24 / 60) * $selisih;

            $hasil_farming = $profit * $dt->amount_stake;

            //POTONG SHARING PROFIT
            $sp = SharingProfit::getSharingProfit($data_master_staking->id_coin_reward, $dt->amount_stake);
            $sharing_profit = $hasil_farming * ($sp->sharing_profit / 100);
            $hasil_farming = $hasil_farming - $sharing_profit;
            $dt->can_harvest = $hasil_farming;

            //HITUNG PROGRESS STAKING
            $created_at = CommonHelper::convertDateFormat($dt->created_at);
            $total_hari = CommonHelper::getSelisihDay($created_at, $dt->end_at);
            $progress = CommonHelper::getSelisihDay($created_at, $date_now);
            $progressBar = ($progress / $total_hari) * 100;
            if ($progressBar > 100) {
                $progressBar = 100;
            }
            $dt->progress_percentage = round($progressBar, 2);
            $selisih = CommonHelper::getSelisihDay($dt->harvest_at, $date_now);
            $dt->selisih_harvest = $selisih;

            $dt->created_at_format = CommonHelper::convertDateFormatFrontend($dt->created_at);
            $dt->end_at_format = CommonHelper::convertDateFormatFrontend($dt->end_at);
            $dt->harvest_at_format = CommonHelper::convertDateFormatFrontend($dt->harvest_at);
            //GET COIN REWARD
            $dt->coin_reward = MasterCoin::getKodeCoin($data_master_staking->id_coin_reward);
            $dt->url_coin = MasterCoin::getURLCoin($data_master_staking->id_coin_reward);
            $dt->product = $data_master_staking;
            $dt->product["APR"] = $dt->product->profit * $dt->product->tenor;
        }

        return $this->createSuccessMessage($data);
    }

    public function withdraw(Request $request)
    {
        $user = CommonHelper::checkLogin();
        if (!isset($user)) {
            return $this->createErrorMessage("Please Login First", 400);
        }

        $id_trx_staking = $request->id_trx_staking;
        $data_trx = TrxStaking::where("id", $id_trx_staking)
            ->where("status", 0)
            ->first();

        if (!isset($data_trx)) {
            return $this->createErrorMessage("Staking not exist", 400);
        }

        //CEK STAKING SUDAH SELESAI APA BELOM
        $date_now = date("Y-m-d");
        $selisih_date_end = CommonHelper::getSelisihDay($data_trx->end_at, $date_now);
        if ($selisih_date_end > 0) {
            return $this->createErrorMessage("Staking already finished", 400);
        }

        $data_master_staking = MasterStaking::getStakingData($data_trx->id_product_staking);

        //HARVEST HASIL STAKING
        $date_now = date("Y-m-d");
        $date_last_harvest = CommonHelper::convertDateFormat($data_trx->harvest_at);
        $date_end = CommonHelper::convertDateFormat($data_trx->end_at);

        $selisih_date_harvest = CommonHelper::getSelisihDay($date_last_harvest, $date_now);
        $selisih_date_end = CommonHelper::getSelisihDay($date_end, $date_now);

        if ($selisih_date_end < 0) {
            //STAKING BELUM SELESAI
            if ($selisih_date_harvest > 0) {
                //DAPAT DIHARVEST 
                $data_master_staking = MasterStaking::getStakingData($data_trx->id_product_staking);

                //GET FEE
                $id_coin_usd = MasterCoin::getIdCoin("USDV");
                $fee = SettingFee::where("fee_name", "HARVEST_FEE")->first()->value;
                $fee = PriceCoin::convertCoin($id_coin_usd, $data_master_staking->id_coin_reward, $fee);

                //HITUNG HASIL
                $profit = $data_master_staking->profit;
                $profit = floatval(($profit / 100) / 30) * $selisih_date_harvest;

                $hasil_farming = $data_trx->amount_stake * $profit;

                //POTONG SHARING PROFIT
                $sp = SharingProfit::getSharingProfit($data_master_staking->id_coin_reward, $data_trx->amount_stake);
                $sharing_profit = $hasil_farming * ($sp->sharing_profit / 100);
                $hasil_farming = $hasil_farming - $sharing_profit;

                //POTONG FEE HARVEST
                $hasil_farming = $hasil_farming - $fee;

                //CREATE TRANSAKSI HISTORY HARVEST
                $history = new TrxHistory();
                $history->id_user = $user->id;
                $history->id_coin = $data_master_staking->id_coin_reward;
                $history->id_ref = $data_trx->id;
                $history->trx_type = "STAKING_HARVEST";
                $history->status = 1;
                $history->total_fee = $fee + $sharing_profit;
                $history->amount = $hasil_farming;
                $history->save();
            }
        }

        //HITUNG FEE COMITMENT / FEE CANCEL STAKING
        $percentage_fee = $data_master_staking->comitment_fee;
        $fee = $data_trx->amount_stake * ($percentage_fee / 100);

        //CREATE TRANSAKSI HISTORY WITHDRAW STAKING
        $history = new TrxHistory();
        $history->id_user = $user->id;
        $history->id_coin = $data_master_staking->id_coin_reward;
        $history->id_ref = $data_trx->id;
        $history->trx_type = "STAKING_WITHDRAW";
        $history->status = 1;
        $history->total_fee = $fee;
        $history->amount = $data_trx->amount_stake - $fee;
        $history->save();

        //UPDATE TRANSAKSI STAKING STATUS = 1 -> SELESAI
        $data_trx = TrxStaking::where("id", $request->id_trx_staking)->first();
        $data_trx->status = -1;
        $data_trx->save();

        return $this->createSuccessMessage($history);
    }

    public function detail(Request $request)
    {
        $id = $request->id;
        $data = TrxStaking::where("id", $id)->first();
        $data->history = TrxHistory::where("trx_history.id_ref", $id)
            ->join("master_coin", "master_coin.id", "trx_history.id_coin")
            ->where("trx_history.trx_type", "like", "STAKING%")
            ->select("trx_history.*", "master_coin.code")
            ->get();
        $data->product = MasterStaking::where("id", $data->id_product_staking)->first();
        return $this->createSuccessMessage($data);
    }

    public function edit(Request $request)
    {
        $data = MasterStaking::where("id", $request->id)->first();
        $data->product_name = $request->product_name;
        $data->id_coin_stake = $request->id_coin_stake;
        $data->id_coin_reward = $request->id_coin_reward;
        $data->tenor = $request->tenor;
        $data->comitment_fee = $request->comitment_fee;
        $data->profit = $request->profit;
        $data->status = $request->status;
        $data->save();

        return $this->createSuccessMessage($data);
    }
}
