<?php

namespace App\Http\Controllers\WebServices;

use App\Helpers\CoinBaseHelper;
use App\Helpers\CommonHelper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\MasterCoin;
use App\Models\PriceCoin;
use App\Models\SettingFee;
use App\Models\TrxDeposit;
use App\Models\TrxHistory;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\URL;
use stdClass;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class CoinWebServiceController extends Controller
{
    //

    function getAllCoin(Request $request)
    {
        if (isset($request->kode)) {
            $data = MasterCoin::where("code", $request->kode)->first();
        } else {
            $data = MasterCoin::get();
        }

        return $this->createSuccessMessage($data);
    }

    function insertMasterCoin(Request $request)
    {
        if (isset($request->coin_name) && isset($request->code)) {
            $data = new MasterCoin();
            $data->coin_name = $request->coin_name;
            $data->code = $request->code;

            $file = $request->file;
            $category = "coin";

            $url_path = $this->uploadImage($file, $category);
            $data->url_icon = $url_path;
            $data->save();

            DB::commit();

            return $this->createSuccessMessage("Insert Coin Berhasil", 200);
        } else {
            return $this->createErrorMessage("Parameter tidak lengkap", 400);
        }
    }

    function updateMasterCoin(Request $request)
    {
        if (isset($request->coin_id)) {
            $data = MasterCoin::where("id", $request->coin_id)->first();

            $data->coin_name = $request->coin_name;
            $data->code = $request->code;
            if (isset($request->file)) {
                $file = $request->file;
                $category = "coin";

                $url_path = $this->uploadImage($file, $category);
                $data->url_icon = $url_path;
            }

            $data->save();
            DB::commit();

            return $this->createSuccessMessage("Update Coin Berhasil", 200);
        } else {
            return $this->createErrorMessage("Parameter id wajib diisi", 400);
        }
    }

    function deleteMasterCoin(Request $request)
    {
        if (isset($request->coin_id)) {
            $data = MasterCoin::where("id", $request->coin_id)->first();
            $data->delete();

            DB::commit();

            return $this->createSuccessMessage("Delete Coin Berhasil", 200);
        } else {
            return $this->createErrorMessage("Parameter id wajib diisi", 400);
        }
    }

    public function generateAddress(Request $request)
    {
        $user = Auth::user();

        if ($user) {
            // $data = CoinBaseHelper::getMethod("/v2/payment-methods");
            // $data = CoinBaseHelper::getMethod("/v2/accounts/869beabe-98e8-5b83-8d7d-11269da3ab57/addresses");
            // print_r($user->fullname);


            $id_coin = MasterCoin::getIdCoin($request->type);
            $check = TrxDeposit::where("id_user", $user->id)->where("id_coin", $id_coin)->where("status", 0)->orderBy("created_at", "DESC")->first();
            if ($check) {
                //SUDAH ADA ADDRESS
                // print_r($check->address);
                $data = new stdClass();
                $data->address = $check->address;
                $data->type = $request->type;
                $statusCode = 201;
                $message = "Address sudah ada";
            } else {
                //GENERATE NEW ADDRESS
                $body = new stdClass();
                $body->name = $user->name;
                $parameter = json_encode($body);
                if ($request->type == "BTC") {
                    $coinBaseWallet = getenv("COINBASE_WALLET_BTC");
                } else if ($request->type == "ETH") {
                    $coinBaseWallet = getenv("COINBASE_WALLET_ETH");
                } else if ($request->type == "USDC") {
                    $coinBaseWallet = getenv("COINBASE_WALLET_USDC");
                } else {
                    return $this->createErrorMessage("Tipe tidak exist", 400);
                }
                $dt = CoinBaseHelper::postMethod("/v2/accounts/" . $coinBaseWallet . "/addresses", $parameter);
                print_r($dt);
                // // if (!isset($dt)) {
                // //     return $this->createErrorMessage("Coinbase API error", 400);
                // // }
                // // GENERATE QR CODE
                // // $qrLink = 'QR_address_' . $dt["data"]["address"] . '.png';
                // // // $qrLink = 'QR_address_123.png';
                // // // echo $qrLink;
                // // QrCode::format('png')
                // //     ->size(300)
                // //     ->generate($dt["data"]["address"], storage_path('/app/public/image/QRcode/' . $qrLink));

                // // QrCode::format('png')
                // //     ->size(300)
                // //     ->generate("1234", storage_path('/app/public/image/QRcode/' . $qrLink));

                // // echo  URL::to('/') . '/storage/image/QRcode/' . $qrLink;

                // $crypto = new TrxDeposit();
                // $crypto["id_user"] = $user->id;
                // $crypto["id_coin"] = $id_coin;
                // $crypto["trx_type"] = "DEPOSIT";
                // $crypto["status"] = 0;
                // $crypto["address"] = $request->type;
                // $crypto["address"] = $dt["data"]["address"];
                // // $crypto["qr_link"] = $qrLink;

                // $crypto->save();

                // $data = new stdClass();
                // $data->address = $dt["data"]["address"];
                // $data->type = $request->type;

                // DB::commit();
                // $statusCode = 200;
                // $message = "Berhasil generate address baru";
            }

            return $this->createSuccessMessage($data, $statusCode, $message);
        } else {
            return $this->createErrorMessage("Silahkan Login terlebih dahulu", 400);
        }
    }

    public function sendCoin(Request $request)
    {
        $user = Auth::user();
        if (!$user) {
            return $this->createErrorMessage("Silahkan Login terlebih dahulu", 400);
        }

        $saldo = User::getSaldo($request->currency, $user->id);

        if ($saldo < $request->amount) {
            return $this->createErrorMessage("Saldo tidak cukup", 400);
        }

        //HITUNG FEE
        $total_fee = $this->getFee($request->currency);
        $net_ammount = floatval($request->amount) - floatval($total_fee->value);
        $total_fee = floatval($total_fee->value);
        if ($net_ammount <= 0) {
            return $this->createErrorMessage("Insufficient Fee Transfer", 400);
        }

        $id_coin = MasterCoin::getIdCoin($request->currency);
        $verify_code = Hash::make($this->generateRandomString(16));
        //GENERATE HISTORY TRANSAKSI
        $crypto = new TrxDeposit();
        $crypto["id_user"] = $user->id;
        $crypto["id_coin"] = $id_coin;
        $crypto["trx_type"] = "WITHDRAW";
        $crypto["status"] = 0;
        $crypto["address"] = $request->address;
        $crypto["total_fee"] = $total_fee;
        $crypto["fee"] = $total_fee;
        $crypto["amount"] = $net_ammount;
        $crypto["verify_code"] = $verify_code;
        $crypto->save();

        $data = new TrxHistory();
        $data["id_user"] = $user->id;
        $data["id_coin"] = $id_coin;
        $data["id_ref"] = $crypto->id;
        $data["trx_type"] = "WITHDRAW";
        $data["status"] = 0;
        $data["total_fee"] = $total_fee;
        $data["amount"] = $request->amount * -1;
        $data->save();

        DB::commit();

        $view = 'withdrawalmails';
        $to_name = $user->name;
        $to_email = $user->email;
        $from = env('MAIL_USERNAME', 'noreplysupport@gmail.com');
        $emailname = env('MAIL_NAME', 'VINIX');
        $link = URL('/verify_withdraw') . "?id=" . $crypto->id  . "&kode=" . $verify_code . "&id_hist=" . $data->id;
        $data = array(
            "kode" => $verify_code,
            "name" => $user->name,
            "address" => $crypto->address,
            "amount" => $request->amount,
            "currency" => $request->currency,
            "link" => $link
        );
        CommonHelper::sendMail($view, $data, $to_name, $to_email, "Confirmation Withdrawal Vinix", $from, $emailname);

        return $this->createSuccessMessage($crypto);

        // if ($request->currency == "BTC") {
        //     $coinBaseWallet = getenv("COINBASE_WALLET_BTC");
        // } else if ($request->currency == "ETH") {
        //     $coinBaseWallet = getenv("COINBASE_WALLET_ETH");
        // } else if ($request->currency == "USDC") {
        //     $coinBaseWallet = getenv("COINBASE_WALLET_USDC");
        // } else {
        //     return $this->createErrorMessage("Tipe tidak exist", 400);
        // }

        // $id_coin = MasterCoin::getIdCoin($request->currency);

        // //KODE UNIK   
        // $idem = date("Ymdhis");

        // $body = new stdClass();
        // $body->type = "send";
        // $body->to = $request->address;
        // $body->amount = $net_ammount;
        // $body->currency = $request->currency;
        // // $body->fee = "0.00220500";
        // $body->idem = $idem;
        // $parameter = json_encode($body);
        // $dt = CoinBaseHelper::postMethod("/v2/accounts/" . $coinBaseWallet . "/transactions", $parameter);

        // if (isset($dt["data"])) {
        //     //BERHASIL TRANSFER
        //     //GENERATE HISTORY TRANSAKSI
        //     $crypto = new TrxDeposit();
        //     $crypto["id_user"] = $user->id;
        //     $crypto["id_coin"] = $id_coin;
        //     $crypto["trx_type"] = "WITHDRAW";
        //     $crypto["status"] = 1;
        //     $crypto["address"] = $request->address;
        //     $crypto["total_fee"] = $total_fee;
        //     $crypto["fee"] = $total_fee - $dt["data"]["network"]["transaction_fee"]["amount"];
        //     $crypto["amount"] = $request->amount * -1;
        //     $crypto->save();

        //     $data = new TrxHistory();
        //     $data["id_user"] = $user->id;
        //     $data["id_coin"] = $id_coin;
        //     $data["id_ref"] = $crypto->id;
        //     $data["trx_type"] = "WITHDRAW";
        //     $data["status"] = 1;
        //     $data["total_fee"] = $total_fee;
        //     $data["amount"] = $request->amount * -1;
        //     $data->save();

        //     DB::commit();

        //     return $this->createSuccessMessage($dt);
        // } else if (isset($dt["errors"])) {
        //     if ($dt["errors"][0]["message"] == "You can't send a payment from an account to itself.") {
        //         //TRANSFER SESAMA VINIX
        //         $penerima = TrxDeposit::where("address", $request->address)->first();
        //         if (!isset($penerima)) {
        //             return $this->createErrorMessage("Address not found", 400);
        //         }
        //         if ($penerima->id_user != $user->id) {

        //             //GENERATE HISTORY TRANSAKSI
        //             $data = new TrxHistory();
        //             $data["id_user"] = $user->id;
        //             $data["id_coin"] = $id_coin;
        //             $data["id_ref"] = -1;
        //             $data["trx_type"] = "TRANSFER";
        //             $data["status"] = 1;
        //             $data["total_fee"] = $total_fee;
        //             $data["amount"] = $request->amount * -1;
        //             $data->save();

        //             //GENERATE HISTORY PENERIMA
        //             $to = new TrxHistory();
        //             $to["id_user"] = $penerima->id_user;
        //             $to["id_coin"] = $id_coin;
        //             $to["id_ref"] = $penerima->id;
        //             $to["trx_type"] = "TRANSFER";
        //             $to["status"] = 1;
        //             $to["total_fee"] = 0;
        //             $to["amount"] = $net_ammount;
        //             $to->save();

        //             DB::commit();

        //             return $this->createSuccessMessage("Success Transfer Between Vinix user");
        //         } else {
        //             return $this->createErrorMessage("Tidak dapat transfer ke diri sendiri", 400);
        //         }
        //     } else {
        //         return $this->createErrorMessage($dt["errors"][0]["message"], 400);
        //     }
        // } else {
        //     return $this->createErrorMessage($dt, 400);
        // }
    }

    public function verifyWithdrawal(Request $request)
    {
        $id = $request->id;
        $kode = $request->kode;

        $data = TrxDeposit::where("id", $id)
            ->where("verify_code", $kode)
            ->first();
        // print_r($data);
        if (!isset($data)) {
            return $this->createErrorMessage("Verification Code Expired", 400);
        }
        if ($data->verified == 1) {
            return $this->createErrorMessage("Verification already done", 400);
        }

        $data2 = TrxHistory::where("id", $request->id_hist)->first();
        $kode = MasterCoin::getKodeCoin($data2->id_coin);
        // print_r($request->id);
        if (User::getSaldo(MasterCoin::getKodeCoin($data2->id_coin), $data2->id_user) < $data2->amount * -1) {
            return $this->createErrorMessage("Insufficient Balance", 400);
        }

        $data->verified = 1;
        $data->verify_code = "";
        $data->save();

        $data2->status = 1;
        $data2->save();


        return $this->createSuccessMessage("Verification Withdraw Success");
    }


    public function getFee($type)
    {
        if ($type == "ETH") {
            $result = SettingFee::where("fee_name", "WITHDRAW_FEE_ETH")->first();
        } else if ($type == "BTC") {
            $result = SettingFee::where("fee_name", "WITHDRAW_FEE_BTC")->first();
        } else if ($type == "USDC") {
            $result = SettingFee::where("fee_name", "WITHDRAW_FEE_USDC")->first();
        }

        return $result;
    }

    public function callback(Request $request)
    {
        $bodyContent = $request->getContent();
        $bodyContent = json_decode($bodyContent);

        // print_r($bodyContent->data->address);

        // print_r($bodyContent->additional_data->amount);
        $data = TrxDeposit::where("address", $bodyContent->data->address)->orderBy("created_at", "DESC")->first();

        if (!isset($data)) {
            return $this->createErrorMessage("Address " . $bodyContent->data->address . " not found", 400);
        }
        if ($data->status == 0) {
            $data->status = 1;
            $data->amount = $bodyContent->additional_data->amount->amount;
            $data->save();
        }
        $dt = new TrxHistory();
        $dt["id_user"] = $data->id_user;
        $dt["id_coin"] = $data->id_coin;
        $dt["id_ref"] = $data->id;
        $dt["trx_type"] = "DEPOSIT";
        $dt["status"] = 1;
        $dt["total_fee"] = 0;
        $dt["amount"] = $bodyContent->additional_data->amount->amount;
        $dt->save();

        DB::commit();
        return $this->createSuccessMessage("Transaksi dengan address " . $bodyContent->data->address . " Selesai");
    }

    public function getAddress(Request $request)
    {
        $user = CommonHelper::checkLogin();
        if (!isset($user)) {
            return $this->createErrorMessage("Please Login First", 400);
        }

        $dataCoin = MasterCoin::get();

        //INCLUDE COIN YANG BISA DI GENERATE ADDRESS
        $included = ["ETH", "USDC", "BTC"];
        foreach ($dataCoin as $index => $key) {
            $include = false;
            foreach ($included as $e) {
                if ($key->code == $e) {
                    $include = true;
                }
            }
            if ($include) {
                $d = TrxDeposit::where("id_user", $user->id)
                    ->where("id_coin", $key->id)
                    ->where("trx_type", "DEPOSIT")
                    ->orderBy("created_at", "desc")->first();

                if (isset($d)) {
                    if ($d->qr_link == null) {
                        if ($d->address != null) {
                            // $qrLink = 'QR_address_' . $d->address . '.png';
                            // // $qrLink = 'QR_address_123.png';
                            // // echo $qrLink;
                            // QrCode::format('png')
                            //     ->size(300)
                            //     ->generate($d->address, storage_path('/app/public/image/QRcode/' . $qrLink));
                            // $d->qr_link = $qrLink;
                            // $d->save();
                            // DB::commit();
                        }
                    }
                }
                if (isset($d)) {
                    $data[$index]["address"] = $d->address;
                    // $data[$index]["url_qr"] = URL::to('/') . '/storage/image/QRcode/' . $d->qr_link;
                } else {
                    $data[$index]["address"] = null;
                    $data[$index]["url_qr"] = null;
                }
                if (!isset($data[$index]["address"])) {
                    $data[$index]["address"] = null;
                }
                $data[$index]["coin_name"] = $key->coin_name;
                $data[$index]["code"] = $key->code;
                $data[$index]["url_logo"] = $key->url_icon;
                $data[$index]["saldo"] = User::getSaldo($key->code, $user->id);
            }
        }

        return $this->createSuccessMessage($data);
    }

    public function convertFee(Request $request)
    {
        $to_coin = $request->to_coin;
        $amount = $request->amount;


        $price_from = PriceCoin::getPrice("USDV");
        $price_to = PriceCoin::getPrice($to_coin);
        if (!isset($price_from) || !isset($price_to)) {
            return $this->createErrorMessage("Coin not exist", 400);
        }

        $rate = $price_from->price / $price_to->price;
        $result = $amount * $rate;

        return $this->createSuccessMessage($result);
    }

    public function editAddress(Request $request)
    {
        $address = $request->address;
        $id = $request->id;

        $data = MasterCoin::where("id", $id)->first();
        if (!isset($data)) {
            return $this->createErrorMessage("Coin not found", 400);
        }
        $data->address = $address;
        $data->save();

        return $this->createSuccessMessage($data);
    }

    public function test(Request $request)
    {
        $data = CoinBaseHelper::getMethod("/v2/accounts");
        return $this->createSuccessMessage($data);
    }

    public function postTest(Request $request)
    {
        // echo "HE";
        $file = $request->file;
        $category = $request->category;

        $data = $this->uploadImage($file, $category);

        return $this->createSuccessMessage($data);
    }

    function generateRandomString($length = 10)
    {
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}
