<?php

namespace App\Http\Controllers\WebServices;

use App\Helpers\CommonHelper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\MasterCoin;
use App\Models\MasterMining;
use App\Models\MasterStaking;
use App\Models\PriceCoin;
use App\Models\SettingFee;
use App\Models\SettingGeneral;
use App\Models\SharingProfit;
use App\Models\TrxHistory;
use App\Models\TrxMining;
use App\Models\TrxReferal;
use App\Models\TrxStaking;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use stdClass;

class ReferalWebServiceController extends Controller
{
    //
    public function get_my_referal()
    {
        if (!Auth::check()) {
            return $this->createErrorMessage("Please Login First", 400);
        }

        $user = Auth::user();

        $data_trx_referal = TrxReferal::where("id_user", $user->id)
            ->where("status", 0)
            ->get();

        //GET PERCENTAGE REFERAL PER LEVEL
        $temp = SettingGeneral::where("setting_name", "like", "PERCENTAGE_REFERAL%")->get();
        foreach ($temp as $dt) {
            $data_percentage_commision[$dt->setting_name] = $dt;
        }

        $total_commision_staking = 0;
        $total_commision_mining = 0;

        //GET WALLET COMMISION STAKING
        $data_commision_staking = DB::table('master_staking')
            ->join("master_coin", "master_coin.id", "=", "master_staking.id_coin_reward")
            ->select("master_staking.id_coin_reward", "master_coin.url_icon", "master_coin.coin_name", "master_coin.code", DB::raw('0 as total_referal'))
            ->groupBy("master_staking.id_coin_reward", "master_coin.url_icon", "master_coin.coin_name", "master_coin.code")->get();

        //GET WALLET COMMISION MINING
        $data_commision_mining = DB::table('master_mining')
            ->join("master_coin", "master_coin.id", "=", "master_mining.id_coin_reward")
            ->select("master_mining.id_coin_reward", "master_coin.url_icon", "master_coin.coin_name", "master_coin.code", DB::raw('0 as total_referal'))
            ->groupBy("master_mining.id_coin_reward", "master_coin.url_icon", "master_coin.coin_name", "master_coin.code")->get();

        // return $this->createSuccessMessage($data_commision);
        $date_now = date("Y-m-d H:i:s");
        $id_usd = MasterCoin::getIdCoin("USDV");
        foreach ($data_trx_referal as $dt) {
            if ($dt->type == "REFERAL_STAKING") {
                //REFERAL STAKING
                $string_var_commison = "PERCENTAGE_REFERAL_LEVEL_" . $dt->level;
                $commision = $data_percentage_commision[$string_var_commison]->value;
                $trx_staking = TrxStaking::where("id", $dt->id_ref)->first();
                if (isset($trx_staking)) {
                    $data_master_staking = MasterStaking::getStakingData($trx_staking->id_product_staking);
                    if ($trx_staking->status == 0) {
                        //STAKING MASIH BERJALAN
                        $selisih_date_end = CommonHelper::getSelisihMinutes($date_now, $trx_staking->end_at);
                        if ($selisih_date_end > 0) {
                            //STAKING MASIH AKTIF
                            $selisih_date_harvest = CommonHelper::getSelisihMinutes($dt->last_harvest, $date_now);

                            //HITUNG PROFIT STAKING
                            $profit = $data_master_staking->profit;
                            $profit = floatval(($profit / 100) / 30 / 24 / 60) * $selisih_date_harvest;
                            $hasil_farming = $profit * $trx_staking->amount_stake;

                            $sp = SharingProfit::getSharingProfit($data_master_staking->id_coin_reward, $trx_staking->amount_stake);
                            $sharing_profit = $hasil_farming * ($sp->sharing_profit / 100);
                            // echo $sharing_profit . "\n";

                            //HITUNG KOMISI DARI PROFIT STAKING
                            $hasil_farming = $sharing_profit * ($commision / 100);

                            foreach ($data_commision_staking as $com) {
                                if ($com->id_coin_reward == $data_master_staking->id_coin_reward) {
                                    $com->total_referal = $com->total_referal + $hasil_farming;
                                }
                            }
                            $hasil_farming = PriceCoin::convertCoin($data_master_staking->id_coin_reward, $id_usd, $hasil_farming);
                            $total_commision_staking = $total_commision_staking + $hasil_farming;
                        }
                    }
                }
            } else if ($dt->type == "REFERAL_MINING") {
                //REFERAL MINING
                $string_var_commison = "PERCENTAGE_REFERAL_LEVEL_" . $dt->level;
                $commision = $data_percentage_commision[$string_var_commison]->value;
                $trx_mining = TrxMining::where("id", $dt->id_ref)->first();
                if (isset($trx_mining)) {
                    if ($trx_mining->id_product_mining != null) {
                        $data_master_mining = MasterMining::getData($trx_mining->id_product_mining);
                    } else {
                        $data_master_mining = new stdClass();
                        $data_master_mining->id_coin_reward = $trx_mining->id_coin_reward;
                    }
                    if ($trx_mining->status == 0) {
                        //MINING MASIH BERJALAN
                        $selisih_date_end = CommonHelper::getSelisihMinutes($date_now, $trx_mining->end_at);
                        if ($selisih_date_end > 0) {
                            //MINING MASIH AKTIF
                            $selisih_date_harvest = CommonHelper::getSelisihMinutes($dt->last_harvest, $date_now);

                            ///////////////
                            //GET VALUE MHS TO USD
                            $kode_coin = MasterCoin::getKodeCoin($data_master_mining->id_coin_reward);
                            $mhs_to_usd = SettingGeneral::where("setting_name", "MHS_" . $kode_coin . "_PER_DAY")->first();
                            $mining_speed = $trx_mining->mining_speed * ($commision / 100);
                            $income_mining = (($mhs_to_usd->value / 24 / 60) * $mining_speed) * $selisih_date_harvest;

                            //CONVERT PROFIT USD TO KOIN REWARD
                            $income_mining = PriceCoin::convertCoin($id_usd, $data_master_mining->id_coin_reward, $income_mining);

                            foreach ($data_commision_mining as $com) {
                                if ($com->id_coin_reward == $data_master_mining->id_coin_reward) {
                                    $com->total_referal = $com->total_referal + $income_mining;
                                }
                            }
                            $hasil_farming = PriceCoin::convertCoin($data_master_mining->id_coin_reward, $id_usd, $income_mining);
                            $total_commision_mining = $total_commision_staking + $hasil_farming;
                        }
                    }
                }
            }
        }
        $data["commision_staking"] = $data_commision_staking;
        $data["total_commision_staking_in_usd"] = $total_commision_staking;
        $data["commision_mining"] = $data_commision_mining;
        $data["total_commision_mining_in_usd"] = $total_commision_mining;



        $referal_list = User::select("id", "name", "referal_code")->where("referal_parent", $user->referal_code)->get();
        foreach ($referal_list as $rf) {

            $data_staking = TrxStaking::where("id_user", $rf->id)->where("referal_status", 1)->where("status", 0)->count("id");
            $rf["total_active_staking"] = $data_staking;
            $data_mining = TrxMining::where("id_user", $rf->id)->where("referal_status", 1)->where("status", 0)->count("id");
            $rf["total_active_mining"] = $data_mining;
            $rf["referal"] = User::select("id", "name", "referal_code")->where("referal_parent", $rf->referal_code)->get();
            foreach ($rf["referal"] as $r) {
                $data_staking = TrxStaking::where("id_user", $r->id)->where("referal_status", 1)->where("status", 0)->count("id");
                $r["total_active_staking"] = $data_staking;
                $data_mining = TrxMining::where("id_user", $r->id)->where("referal_status", 1)->where("status", 0)->count("id");
                $r["total_active_mining"] = $data_mining;
            }
        }

        $data["referal_list"] = $referal_list;
        $data["percentage_commission"] = $data_percentage_commision;

        return $this->createSuccessMessage($data);
    }

    public function get_referal_staking()
    {
        if (!Auth::check()) {
            return $this->createErrorMessage("Please Login First", 400);
        }

        $user = Auth::user();

        $data_trx_referal = TrxReferal::where("id_user", $user->id)
            ->where("type", "REFERAL_STAKING")
            ->where("status", 0)
            ->get();

        //GET PERCENTAGE REFERAL PER LEVEL
        $temp = SettingGeneral::where("setting_name", "like", "PERCENTAGE_REFERAL%")->get();
        foreach ($temp as $dt) {
            $data_percentage_commision[$dt->setting_name] = $dt;
        }

        $total_commision_staking = 0;

        //GET WALLET COMMISION STAKING
        $data_commision_staking = DB::table('master_staking')
            ->join("master_coin", "master_coin.id", "=", "master_staking.id_coin_reward")
            ->select("master_staking.id_coin_reward", "master_coin.coin_name", "master_coin.url_icon", "master_coin.code", DB::raw('0 as total_referal'))
            ->groupBy("master_staking.id_coin_reward", "master_coin.coin_name", "master_coin.code", "master_coin.url_icon")
            ->get();

        // return $this->createSuccessMessage($data_commision);
        $date_now = date("Y-m-d H:i:s");
        $id_usd = MasterCoin::getIdCoin("USDV");
        foreach ($data_trx_referal as $dt) {
            //REFERAL STAKING
            $string_var_commison = "PERCENTAGE_REFERAL_LEVEL_" . $dt->level;
            $commision = $data_percentage_commision[$string_var_commison]->value;
            $trx_staking = TrxStaking::where("trx_staking.id", $dt->id_ref)
                ->join("users", "users.id", "trx_staking.id_user")
                ->join("master_staking", "master_staking.id", "trx_staking.id_product_staking")
                ->select("trx_staking.*", "users.name", "master_staking.product_name")
                ->first();
            if (isset($trx_staking)) {
                $data_master_staking = MasterStaking::getStakingData($trx_staking->id_product_staking);
                if ($trx_staking->status == 0) {
                    //STAKING MASIH BERJALAN
                    $selisih_date_end = CommonHelper::getSelisihMinutes($date_now, $trx_staking->end_at);
                    if ($selisih_date_end > 0) {
                        //STAKING MASIH AKTIF
                        $selisih_date_harvest = CommonHelper::getSelisihMinutes($dt->last_harvest, $date_now);

                        //HITUNG PROFIT STAKING
                        $profit = $data_master_staking->profit;
                        $profit = floatval(($profit / 100) / 30 / 24 / 60) * $selisih_date_harvest;
                        $hasil_farming = $profit * $trx_staking->amount_stake;

                        $sp = SharingProfit::getSharingProfit($data_master_staking->id_coin_reward, $trx_staking->amount_stake);
                        $sharing_profit = $hasil_farming * ($sp->sharing_profit / 100);
                        // echo $sharing_profit . "\n";

                        //HITUNG KOMISI DARI PROFIT STAKING
                        $hasil_farming = $sharing_profit * ($commision / 100);
                        $hasil_farming = number_format($hasil_farming, 8);
                        $dt->amount_referal = $hasil_farming;
                        $dt->detail_staking = $trx_staking;

                        foreach ($data_commision_staking as $com) {
                            if ($com->id_coin_reward == $data_master_staking->id_coin_reward) {
                                $com->data_referal[] = $dt;
                                $com->total_referal = number_format($com->total_referal + $hasil_farming, 8);
                            }
                        }
                        $hasil_farming = PriceCoin::convertCoin($data_master_staking->id_coin_reward, $id_usd, $hasil_farming);
                        $total_commision_staking = $total_commision_staking + $hasil_farming;
                    }
                }
            }
        }
        $data["commision_staking"] = $data_commision_staking;
        $data["total_commision_staking_in_usd"] = $total_commision_staking;

        return $this->createSuccessMessage($data);
    }

    public function get_referal_mining()
    {
        if (!Auth::check()) {
            return $this->createErrorMessage("Please Login First", 400);
        }

        $user = Auth::user();

        $data_trx_referal = TrxReferal::where("id_user", $user->id)
            ->where("status", 0)
            ->where("type", "REFERAL_MINING")
            ->get();

        //GET PERCENTAGE REFERAL PER LEVEL
        $temp = SettingGeneral::where("setting_name", "like", "PERCENTAGE_REFERAL%")->get();
        foreach ($temp as $dt) {
            $data_percentage_commision[$dt->setting_name] = $dt;
        }

        $total_commision_staking = 0;
        $total_commision_mining = 0;

        //GET WALLET COMMISION MINING
        $data_commision_mining = DB::table('master_mining')
            ->join("master_coin", "master_coin.id", "=", "master_mining.id_coin_reward")
            ->select("master_mining.id_coin_reward", "master_coin.coin_name", "master_coin.url_icon", "master_coin.code", DB::raw('0 as total_referal'), DB::raw('0 as total_speed'))
            ->groupBy("master_mining.id_coin_reward", "master_coin.coin_name", "master_coin.code", "master_coin.url_icon")
            ->get();

        // return $this->createSuccessMessage($data_commision);
        $date_now = date("Y-m-d H:i:s");
        $id_usd = MasterCoin::getIdCoin("USDV");
        foreach ($data_trx_referal as $dt) {
            //REFERAL MINING
            $string_var_commison = "PERCENTAGE_REFERAL_LEVEL_" . $dt->level;
            $commision = $data_percentage_commision[$string_var_commison]->value;
            $trx_mining = TrxMining::where("trx_mining.id", $dt->id_ref)
                ->join("users", "trx_mining.id_user", "users.id")
                ->select("trx_mining.*", "users.name")
                ->first();
            if (isset($trx_mining)) {
                if ($trx_mining->id_product_mining != null) {
                    $data_master_mining = MasterMining::getData($trx_mining->id_product_mining);
                } else {
                    $data_master_mining = new stdClass();
                    $data_master_mining->id_coin_reward = $trx_mining->id_coin_reward;
                    $data_master_mining->product_name = "Custom Mining";
                }
                if ($trx_mining->status == 0) {
                    //MINING MASIH BERJALAN
                    $selisih_date_end = CommonHelper::getSelisihMinutes($date_now, $trx_mining->end_at);
                    if ($selisih_date_end > 0) {
                        //MINING MASIH AKTIF
                        $selisih_date_harvest = CommonHelper::getSelisihMinutes($dt->last_harvest, $date_now);
                    } else {
                        $selisih_date_harvest = CommonHelper::getSelisihMinutes($dt->last_harvest, $trx_mining->end_at);
                    }
                } else if ($trx_mining->status == 1) {
                    $selisih_date_harvest = CommonHelper::getSelisihMinutes($dt->last_harvest, $trx_mining->end_at);
                } else {
                    $selisih_date_harvest = CommonHelper::getSelisihMinutes($dt->last_harvest, $trx_mining->last_harvest);
                }

                ///////////////
                //GET VALUE MHS TO USD
                $kode_coin = MasterCoin::getKodeCoin($data_master_mining->id_coin_reward);
                $mhs_to_usd = SettingGeneral::where("setting_name", "MHS_" . $kode_coin . "_PER_DAY")->first();
                $mining_speed = $trx_mining->mining_speed * ($commision / 100);
                $income_mining = (($mhs_to_usd->value / 24 / 60) * $mining_speed) * $selisih_date_harvest;


                //CONVERT PROFIT USD TO KOIN REWARD
                $income_mining = PriceCoin::convertCoin($id_usd, $data_master_mining->id_coin_reward, $income_mining);

                $income_mining = number_format($income_mining, 8);
                $dt->amount_referal = $income_mining;
                $dt->commision_speed = $trx_mining->mining_speed * ($commision / 100);
                $dt->detail_mining = $trx_mining;
                $dt->detail_mining->product_name = $data_master_mining->product_name;
                foreach ($data_commision_mining as $com) {
                    if ($com->id_coin_reward == $data_master_mining->id_coin_reward) {
                        $com->total_referal = number_format($com->total_referal + $income_mining, 8);
                        $com->total_speed = $com->total_speed + ($trx_mining->mining_speed * ($commision / 100));
                        $com->data_referal[] = $dt;
                    }
                }
                $hasil_farming = PriceCoin::convertCoin($data_master_mining->id_coin_reward, $id_usd, $income_mining);
                $total_commision_mining = $total_commision_staking + $hasil_farming;
            }
        }
        $data["commision_mining"] = $data_commision_mining;
        $data["total_commision_mining_in_usd"] = $total_commision_mining;

        return $this->createSuccessMessage($data);
    }

    public function harvest_referal_staking(Request $request)
    {
        if (!Auth::check()) {
            return $this->createErrorMessage("Please Login First", 400);
        }

        $user = Auth::user();

        $data_trx_referal = TrxReferal::where("id_user", $user->id)
            ->where("type", "REFERAL_STAKING")
            ->where("status", 0)
            ->get();

        //GET PERCENTAGE REFERAL PER LEVEL
        $temp = SettingGeneral::where("setting_name", "like", "PERCENTAGE_REFERAL%")->get();
        foreach ($temp as $dt) {
            $data_percentage_commision[$dt->setting_name] = $dt;
        }

        $total_commision_staking = 0;

        $id_coin = MasterCoin::getIdCoin($request->type);

        //GET WALLET COMMISION STAKING
        $data_commision_staking = DB::table('master_staking')
            ->join("master_coin", "master_coin.id", "=", "master_staking.id_coin_reward")
            ->where("master_coin.id", $id_coin)
            ->select("master_staking.id_coin_reward", "master_coin.coin_name", "master_coin.code", DB::raw('0 as total_referal'))
            ->groupBy("master_staking.id_coin_reward", "master_coin.coin_name", "master_coin.code")
            ->get();

        // return $this->createSuccessMessage($data_commision_staking);

        DB::commit();
        // return $this->createSuccessMessage($data_commision);
        $date_now = date("Y-m-d H:i:s");
        $index = 0;
        $changedid = [];
        $id_usd = MasterCoin::getIdCoin("USDV");
        foreach ($data_trx_referal as $dt) {
            //REFERAL STAKING
            $string_var_commison = "PERCENTAGE_REFERAL_LEVEL_" . $dt->level;
            $commision = $data_percentage_commision[$string_var_commison]->value;
            $trx_staking = TrxStaking::where("id", $dt->id_ref)->first();
            if (isset($trx_staking)) {
                $data_master_staking = MasterStaking::getStakingData($trx_staking->id_product_staking);
                if ($data_master_staking->id_coin_reward == $id_coin) {
                    if ($trx_staking->status == 0) {
                        //STAKING MASIH BERJALAN
                        $selisih_date_end = CommonHelper::getSelisihMinutes($date_now, $trx_staking->end_at);
                        if ($selisih_date_end > 0) {
                            //STAKING MASIH AKTIF
                            $selisih_date_harvest = CommonHelper::getSelisihMinutes($dt->last_harvest, $date_now);

                            //HITUNG PROFIT STAKING
                            $profit = $data_master_staking->profit;
                            $profit = floatval(($profit / 100) / 30 / 24 / 60) * $selisih_date_harvest;
                            $hasil_farming = $profit * $trx_staking->amount_stake;

                            $sp = SharingProfit::getSharingProfit($data_master_staking->id_coin_reward, $trx_staking->amount_stake);
                            $sharing_profit = $hasil_farming * ($sp->sharing_profit / 100);
                            // echo $sharing_profit . "\n";

                            //HITUNG KOMISI DARI PROFIT STAKING
                            $hasil_farming = $sharing_profit * ($commision / 100);
                            $hasil_farming = number_format($hasil_farming, 8);

                            foreach ($data_commision_staking as $com) {
                                if ($com->id_coin_reward == $data_master_staking->id_coin_reward) {
                                    $com->data_referal[] = $dt;
                                    $com->total_referal = number_format($com->total_referal + $hasil_farming);
                                }
                            }

                            $changedid[$index] = $dt->id;
                            $index++;
                            // $dt->last_harvest = $date_now;
                            // $dt->save();
                            // $hasil_farming = PriceCoin::convertCoin($data_master_staking->id_coin_reward, $id_usd, $hasil_farming);
                            $total_commision_staking = $total_commision_staking + $hasil_farming;
                        }
                    }
                }
            }
        }

        $fee_harvest = SettingFee::where("fee_name", "REFERAL_HARVEST_FEE")->first()->value;
        //change to coin reward
        $fee_harvest = PriceCoin::convertCoin($id_usd, $id_coin, $fee_harvest);

        if ($total_commision_staking < $fee_harvest) {
            DB::rollBack();
            return $this->createErrorMessage("Insufficient Balance", 400);
        }

        $netto_referal = $total_commision_staking - $fee_harvest;

        foreach ($data_trx_referal as $dt) {
            foreach ($changedid as $ch) {
                if ($dt->id == $ch) {
                    $dt->last_harvest = $date_now;
                    $dt->save();
                }
            }
        }
        //CREATE HISTORY TRANSAKSI (PENAMBAHAN SALDO)
        $history_from = new TrxHistory();
        $history_from->id_user = $user->id;
        $history_from->id_coin = $id_coin;
        $history_from->id_ref = "-1";
        $history_from->trx_type = "REFERAL_STAKING_REWARD";
        $history_from->status = 1;
        $history_from->total_fee = $fee_harvest;
        $history_from->amount = $netto_referal;
        $history_from->save();

        DB::commit();
        return $this->createSuccessMessage($history_from);
    }

    public function harvest_referal_mining(Request $request)
    {
        if (!Auth::check()) {
            return $this->createErrorMessage("Please Login First", 400);
        }

        $user = Auth::user();

        $data_trx_referal = TrxReferal::where("id_user", $user->id)
            ->where("type", "REFERAL_MINING")
            ->where("status", 0)
            ->get();

        //GET PERCENTAGE REFERAL PER LEVEL
        $temp = SettingGeneral::where("setting_name", "like", "PERCENTAGE_REFERAL%")->get();
        foreach ($temp as $dt) {
            $data_percentage_commision[$dt->setting_name] = $dt;
        }

        $total_commision_mining = 0;

        $id_coin = MasterCoin::getIdCoin($request->type);


        //GET WALLET COMMISION MINING
        $data_commision_mining = DB::table('master_mining')
            ->join("master_coin", "master_coin.id", "=", "master_mining.id_coin_reward")
            ->select("master_mining.id_coin_reward", "master_coin.coin_name", "master_coin.code", DB::raw('0 as total_referal'))
            ->groupBy("master_mining.id_coin_reward", "master_coin.coin_name", "master_coin.code")
            ->get();


        // return $this->createSuccessMessage($data_commision_staking);

        DB::commit();
        // return $this->createSuccessMessage($data_commision);
        $date_now = date("Y-m-d H:i:s");
        $id_usd = MasterCoin::getIdCoin("USDV");
        $changedid = [];
        $index = 0;
        foreach ($data_trx_referal as $dt) {
            //REFERAL MINING
            $string_var_commison = "PERCENTAGE_REFERAL_LEVEL_" . $dt->level;
            $commision = $data_percentage_commision[$string_var_commison]->value;
            $trx_mining = TrxMining::where("id", $dt->id_ref)->first();
            if (isset($trx_mining)) {
                if ($trx_mining->id_product_mining != null) {
                    $data_master_mining = MasterMining::getData($trx_mining->id_product_mining);
                } else {
                    $data_master_mining = new stdClass();
                    $data_master_mining->id_coin_reward = $trx_mining->id_coin_reward;
                }
                if ($data_master_mining->id_coin_reward == $id_coin) {
                    if ($trx_mining->status == 0) {
                        //MINING MASIH BERJALAN
                        $selisih_date_end = CommonHelper::getSelisihMinutes($date_now, $trx_mining->end_at);
                        if ($selisih_date_end > 0) {
                            //MINING MASIH AKTIF
                            $selisih_date_harvest = CommonHelper::getSelisihMinutes($dt->last_harvest, $date_now);

                            ///////////////
                            //GET VALUE MHS TO USD
                            $kode_coin = MasterCoin::getKodeCoin($data_master_mining->id_coin_reward);
                            $mhs_to_usd = SettingGeneral::where("setting_name", "MHS_" . $kode_coin . "_PER_DAY")->first();
                            $mining_speed = $trx_mining->mining_speed * ($commision / 100);
                            $income_mining = (($mhs_to_usd->value / 24 / 60) * $mining_speed) * $selisih_date_harvest;
                            $income_mining = number_format($income_mining, 8);

                            //CONVERT PROFIT USD TO KOIN REWARD
                            $income_mining = PriceCoin::convertCoin($id_usd, $data_master_mining->id_coin_reward, $income_mining);

                            foreach ($data_commision_mining as $com) {
                                if ($com->id_coin_reward == $data_master_mining->id_coin_reward) {
                                    $com->total_referal = number_format($com->total_referal + $income_mining);
                                }
                            }
                            $changedid[$index] = $dt->id;
                            $index++;
                            // $hasil_farming = PriceCoin::convertCoin($data_master_mining->id_coin_reward, $id_usd, $income_mining);
                            $total_commision_mining = $total_commision_mining + $income_mining;
                        }
                    }
                }
            }
        }

        $fee_harvest = SettingFee::where("fee_name", "REFERAL_HARVEST_FEE")->first()->value;
        //change to coin reward
        $fee_harvest = PriceCoin::convertCoin($id_usd, $id_coin, $fee_harvest);

        if ($total_commision_mining < $fee_harvest) {
            DB::rollBack();
            return $this->createErrorMessage("Insufficient Balance", 400);
        }

        $netto_referal = $total_commision_mining - $fee_harvest;

        foreach ($data_trx_referal as $dt) {
            foreach ($changedid as $ch) {
                if ($dt->id == $ch) {
                    $dt->last_harvest = $date_now;
                    $dt->save();
                }
            }
        }

        //CREATE HISTORY TRANSAKSI (PENAMBAHAN SALDO)
        $history_from = new TrxHistory();
        $history_from->id_user = $user->id;
        $history_from->id_coin = $id_coin;
        $history_from->id_ref = "-1";
        $history_from->trx_type = "REFERAL_MINING_REWARD";
        $history_from->status = 1;
        $history_from->total_fee = $fee_harvest;
        $history_from->amount = $netto_referal;
        $history_from->save();

        DB::commit();
        return $this->createSuccessMessage($history_from);
    }
}
