<?php

namespace App\Http\Controllers\WebServices;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\SettingFee;
use App\Models\SettingGeneral;
use stdClass;

class SettingWebServiceController extends Controller
{
    //
    public function get_all_setting()
    {
        $setting_fee = SettingFee::get();
        $setting_general = SettingGeneral::get();

        $data = new stdClass();
        $data->setting_fee = $setting_fee;
        $data->setting_general = $setting_general;

        return $this->createSuccessMessage($data);
    }

    public function update_setting_general(Request $request)
    {
        $data = SettingGeneral::where("id", $request->id)->first();
        $data->value = $request->value;
        $data->save();

        return $this->createSuccessMessage($data);
    }

    public function update_setting_fee(Request $request){
        $data = SettingFee::where("id",$request->id)->first();
        $data->value = $request->value;
        $data->save();

        return $this->createSuccessMessage($data);
    }
}
