<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\CommonHelper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Route;

class LoginController extends Controller
{
    //
    public function index()
    {
        $initialData = CommonHelper::checkSession();
        if (isset($initialData)) {
            if ($initialData->payload->user->role == 2) {
                return redirect("/home");
            } else {
                return view("Admin.login");
            }
        }
        return view("Admin.login");
    }

    public function dologin()
    {
        $newRequest = Request::create('/api/login', 'POST');
        $response = Route::dispatch($newRequest);
        $res = json_decode($response->getContent());
        if ($response->getStatusCode() == 200) {
            if ($res->payload->role == 2) {
                echo '
                <script>
                    window.location.href = "dashboard";
                </script>';
            } else {
                CommonHelper::showAlert("Sign In Failed", "User not allowed", "error", "/admin");
            }
        } else {
            CommonHelper::showAlert("Sign In Failed", $res->error_msg, "error", "/admin");
        }
    }
}
