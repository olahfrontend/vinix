<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\CommonHelper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Route;

class MasterStakingController extends Controller
{
    //
    public function index()
    {

        $initialData = CommonHelper::checkSession();
        if (!isset($initialData)) {
            return redirect("/admin");
        }
        // print_r($initialData->payload->user->role);
        if ($initialData->payload->user->role != 2) {
            return redirect("/admin");
        }
        $data = CommonHelper::getAPI("/api/staking/product");
        // print_r($data->payload);
        return view(
            "Admin.Staking.index",
            [
                "dataStaking" => $data->payload
            ]
        );
    }

    public function add()
    {
        $initialData = CommonHelper::checkSession();
        if (!isset($initialData)) {
            return redirect("/admin");
        }
        // print_r($initialData->payload->user->role);
        if ($initialData->payload->user->role != 2) {
            return redirect("/admin");
        }
        $data = CommonHelper::getAPI("/api/coin/get_all");
        // print_r($data->payload);
        return view(
            "Admin.Staking.add",
            [
                "dataCoin" => $data->payload
            ]
        );
    }

    public function edit(Request $request)
    {
        $initialData = CommonHelper::checkSession();
        if (!isset($initialData)) {
            return redirect("/admin");
        }
        // print_r($initialData->payload->user->role);
        if ($initialData->payload->user->role != 2) {
            return redirect("/admin");
        }
        $data = CommonHelper::getAPI("/api/coin/get_all");
        $dataStaking = CommonHelper::getAPI("/api/staking/product");
        // print_r($dataStaking->payload);
        return view(
            "Admin.Staking.edit",
            [
                "dataStaking" => $dataStaking->payload,
                "dataCoin" => $data->payload
            ]
        );
    }

    public function do_add()
    {
        $newRequest = Request::create('/api/staking/add', 'POST');
        $response = Route::dispatch($newRequest);
        $res = json_decode($response->getContent());
        if ($response->getStatusCode() == 200) {
            CommonHelper::showAlert("Success Add Product", "Success add product", "success", "/admin/master_staking");
        } else {
            CommonHelper::showAlert("Add Product Fail", $res->error_msg, "error", "back");
        }
    }

    public function do_edit()
    {
        $newRequest = Request::create('/api/staking/edit', 'POST');
        $response = Route::dispatch($newRequest);
        $res = json_decode($response->getContent());
        if ($response->getStatusCode() == 200) {
            CommonHelper::showAlert("Success Edit Product", "Success edit product", "success", "/admin/master_staking");
        } else {
            CommonHelper::showAlert("Edit Product Fail", $res->error_msg, "error", "back");
        }
    }
}
