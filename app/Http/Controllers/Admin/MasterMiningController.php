<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\CommonHelper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Route;

class MasterMiningController extends Controller
{
    //
    public function index()
    {
        $initialData = CommonHelper::checkSession();
        if (!isset($initialData)) {
            return redirect("/admin");
        }
        // print_r($initialData->payload->user->role);
        if ($initialData->payload->user->role != 2) {
            return redirect("/admin");
        }
        $data = CommonHelper::getAPI("/api/mining/product");
        // print_r($data->payload);
        return view(
            "Admin.Mining.index",
            [
                "dataMining" => $data->payload
            ]
        );
    }

    public function add()
    {
        $initialData = CommonHelper::checkSession();
        if (!isset($initialData)) {
            return redirect("/admin");
        }
        // print_r($initialData->payload->user->role);
        if ($initialData->payload->user->role != 2) {
            return redirect("/admin");
        }
        $data = CommonHelper::getAPI("/api/coin/get_all");
        // print_r($data->payload);
        return view(
            "Admin.Mining.add",
            [
                "dataCoin" => $data->payload
            ]
        );
    }

    public function edit(Request $request)
    {
        $initialData = CommonHelper::checkSession();
        if (!isset($initialData)) {
            return redirect("/admin");
        }
        // print_r($initialData->payload->user->role);
        if ($initialData->payload->user->role != 2) {
            return redirect("/admin");
        }
        $data = CommonHelper::getAPI("/api/coin/get_all");
        $dataStaking = CommonHelper::getAPI("/api/mining/product");
        // print_r($dataStaking->payload);
        return view(
            "Admin.Mining.edit",
            [
                "dataMining" => $dataStaking->payload,
                "dataCoin" => $data->payload
            ]
        );
    }

    public function do_add()
    {
        $newRequest = Request::create('/api/mining/add', 'POST');
        $response = Route::dispatch($newRequest);
        $res = json_decode($response->getContent());
        if ($response->getStatusCode() == 200) {
            CommonHelper::showAlert("Success Add Product", "Success add product", "success", "/admin/master_mining");
        } else {
            CommonHelper::showAlert("Add Product Fail", $res->error_msg, "error", "back");
        }
    }
    public function do_edit()
    {
        $newRequest = Request::create('/api/mining/edit', 'POST');
        $response = Route::dispatch($newRequest);
        $res = json_decode($response->getContent());
        if ($response->getStatusCode() == 200) {
            CommonHelper::showAlert("Success Edit Product", "Success Edit product", "success", "/admin/master_mining");
        } else {
            CommonHelper::showAlert("Edit Product Fail", $res->error_msg, "error", "back");
        }
    }

    public function editmhs()
    {
        $newRequest = Request::create('/api/mining/edit-mhs', 'POST');
        $response = Route::dispatch($newRequest);
        $res = json_decode($response->getContent());
        if ($response->getStatusCode() == 200) {
            CommonHelper::showAlert("Success Edit Speed", "Success Edit Speed", "success", "back");
        } else {
            CommonHelper::showAlert("Edit Speed Fail", $res->error_msg, "error", "back");
        }
    }

    public function addcustommining()
    {
        $newRequest = Request::create('/api/mining/start_custom_admin', 'POST');
        $response = Route::dispatch($newRequest);
        $res = json_decode($response->getContent());
        if ($response->getStatusCode() == 200) {
            CommonHelper::showAlert("Success", "Success Add Custom Mining", "success", "back");
        } else {
            CommonHelper::showAlert("Add Custom Mining Failed", $res->error_msg, "error", "back");
        }
    }
}
