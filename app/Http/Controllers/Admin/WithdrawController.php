<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\CommonHelper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class WithdrawController extends Controller
{
    //
    public function index()
    {
        $initialData = CommonHelper::checkSession();
        if (!isset($initialData)) {
            return redirect("/admin");
        }
        // print_r($initialData->payload->user->role);
        if ($initialData->payload->user->role != 2) {
            return redirect("/admin");
        }
        $data = CommonHelper::getAPI("/api/get_list_withdraw");
        // print_r($data->payload);
        return view(
            "Admin.Withdraw.index",
            [
                "dataWithdraw" => $data->payload
            ]
        );
    }
}
