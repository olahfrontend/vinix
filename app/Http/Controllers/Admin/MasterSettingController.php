<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\CommonHelper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Route;

class MasterSettingController extends Controller
{
    //
    public function index()
    {
        $initialData = CommonHelper::checkSession();
        if (!isset($initialData)) {
            return redirect("/admin");
        }
        // print_r($initialData->payload->user->role);
        if ($initialData->payload->user->role != 2) {
            return redirect("/admin");
        }
        $data = CommonHelper::getAPI("/api/setting/get_all_setting");
        // print_r($data->payload);

        return view("Admin.Setting.index", [
            "dataSetting" => $data->payload
        ]);
    }

    public function update_general()
    {
        $newRequest = Request::create('/api/setting/update_setting_general', 'POST');
        $response = Route::dispatch($newRequest);
        $res = json_decode($response->getContent());
        if ($response->getStatusCode() == 200) {
            CommonHelper::showAlert("Success Edit Setting", "Success Edit Setting", "success", "/admin/master_setting");
        } else {
            CommonHelper::showAlert("Edit Setting Failed", $res->error_msg, "error", "back");
        }
    }

    public function update_fee()
    {
        $newRequest = Request::create('/api/setting/update_setting_fee', 'POST');
        $response = Route::dispatch($newRequest);
        $res = json_decode($response->getContent());
        if ($response->getStatusCode() == 200) {
            CommonHelper::showAlert("Success Edit Setting", "Success Edit Setting", "success", "/admin/master_setting");
        } else {
            CommonHelper::showAlert("Edit Setting Failed", $res->error_msg, "error", "back");
        }
    }
}
