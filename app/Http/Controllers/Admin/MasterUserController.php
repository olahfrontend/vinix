<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\CommonHelper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Route;

class MasterUserController extends Controller
{
    //
    public function index()
    {
        $initialData = CommonHelper::checkSession();
        if (!isset($initialData)) {
            return redirect("/admin");
        }
        // print_r($initialData->payload->user->role);
        if ($initialData->payload->user->role != 2) {
            return redirect("/admin");
        }
        $data = CommonHelper::getAPI("/api/master_user/get_all");
        // print_r($data->payload);
        return view(
            "Admin.User.index",
            [
                "data" => $data->payload
            ]
        );
    }

    public function detail($id)
    {
        Input::merge(['id' => $id]);
        $data = CommonHelper::getAPI("/api/master_user/get_detail");
        $data_coin = CommonHelper::getAPI("/api/coin/get_all");
        return view("Admin.User.detail", [
            "data" => $data->payload,
            "data_coin" => $data_coin->payload
        ]);
    }

    public function dotopup(Request $request)
    {
        $newRequest = Request::create('/api/topup_admin', 'POST');
        $response = Route::dispatch($newRequest);
        $res = json_decode($response->getContent());
        if ($response->getStatusCode() == 200) {
            CommonHelper::showAlert("Success", "Success Add Balance", "success", "back");
        } else {
            CommonHelper::showAlert("Add Balance Failed", $res->error_msg, "error", "back");
        }
    }
}
