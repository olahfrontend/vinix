<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\CommonHelper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    //
    public function index()
    {
        $initialData = CommonHelper::checkSession();
        if (!isset($initialData)) {
            return redirect("/admin");
        }
        // print_r($initialData->payload->user->role);
        if ($initialData->payload->user->role != 2) {
            return redirect("/admin");
        }
        $data = CommonHelper::getAPI("/api/get_dashboard_admin");
        $history = CommonHelper::getAPI("/api/get_dashboard_history_admin");
        return view("Admin.Dashboard.index", [
            "data" => $data->payload,
            "datahistory" => $history->payload
        ]);
    }
}
