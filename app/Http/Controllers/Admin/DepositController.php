<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\CommonHelper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\MasterCoin;

class DepositController extends Controller
{
    //
    public function index()
    {
        $initialData = CommonHelper::checkSession();
        if (!isset($initialData)) {
            return redirect("/admin");
        }
        // print_r($initialData->payload->user->role);
        if ($initialData->payload->user->role != 2) {
            return redirect("/admin");
        }
        $dataCoin = CommonHelper::getAPI("/api/coin/get_all");
        return view("Admin.Deposit.index", [
            "dataCoin" => $dataCoin->payload
        ]);
    }
}
