<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;
use Image;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;


    public function createErrorMessage($message, $errorCode)
    {
        $result = [
            "payload" => null,
            "error_msg" => $message,
            "code" => $errorCode
        ];
        return Response::json($result, $errorCode);
    }

    public function createSuccessMessage($payload, $statusCode = 200, $message = '')
    {
        $result = [
            "payload" => ($payload == "") ? null : $payload,
            "error_msg" => $message,
            "code" => $statusCode
        ];
        return Response::json($result);
    }


    public static function guid()
    {
        if (function_exists('com_create_guid')) {
            return com_create_guid();
        } else {
            mt_srand((float)microtime() * 10000); //optional for php 4.2.0 and up.
            $charid = strtoupper(md5(uniqid(rand(), true)));
            $hyphen = chr(45); // "-"
            $uuid = //chr(123)// "{"
                substr($charid, 0, 8) . $hyphen
                . substr($charid, 8, 4) . $hyphen
                . substr($charid, 12, 4) . $hyphen
                . substr($charid, 16, 4) . $hyphen
                . substr($charid, 20, 12);
            //.chr(125);// "}"
            return $uuid;
        }
    }

    public function uploadImage($file, $category)
    {
        $file_name = $this->guid() . "." . $file->getClientOriginalExtension();
        $tujuan_upload = "image/" . $category . "/";

        $url_path = URL::to("/") . "/" . $tujuan_upload . $file_name;
        // echo $url_path;
        // upload file
        $file->move($tujuan_upload, $file_name);
        return $url_path;
    }
}
