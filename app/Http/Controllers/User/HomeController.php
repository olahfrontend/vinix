<?php

namespace App\Http\Controllers\User;

use App\Helpers\CommonHelper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    //
    public function index()
    {
        $initialData = CommonHelper::checkSession();
        if (!isset($initialData)) {
            return redirect("/");
        }
        if ($initialData->payload->user->role != 1) {
            return redirect("/");
        }
        $data = CommonHelper::getAPI("/api/get_dashboard");
        return view("User.home", [
            "dataUser" => $initialData->payload,
            "dataDashboard" => $data->payload
        ]);
    }

    public function maintenance()
    {
        $initialData = CommonHelper::checkSession();
        if (!isset($initialData)) {
            return redirect("/");
        }
        if ($initialData->payload->user->role != 1) {
            return redirect("/");
        }
        return view("maintenance", [
            "dataUser" => $initialData->payload
        ]);
    }
}
