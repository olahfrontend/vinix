<?php

namespace App\Http\Controllers\User;

use App\Helpers\CommonHelper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Route;

class ReferalController extends Controller
{
    //
    public function index()
    {

        $initialData = CommonHelper::checkSession();
        if (!isset($initialData)) {
            return redirect("/");
        }
        if ($initialData->payload->user->role != 1) {
            return redirect("/");
        }
        $dataReferal = CommonHelper::getAPI("/api/referal/myreferal");
        // print_r($dataWallet);
        return view(
            "User.Referal.index",
            [
                "dataReferal" => $dataReferal->payload,
                "dataUser" => $initialData->payload
            ]
        );
    }

    public function staking()
    {
        $initialData = CommonHelper::checkSession();
        if (!isset($initialData)) {
            return redirect("/");
        }
        if ($initialData->payload->user->role != 1) {
            return redirect("/");
        }
        $dataReferal = CommonHelper::getAPI("/api/referal/referal_staking");
        // print_r($dataWallet);
        return view(
            "User.Referal.staking",
            [
                "dataReferal" => $dataReferal->payload,
                "dataUser" => $initialData->payload
            ]
        );
    }

    public function mining()
    {
        $initialData = CommonHelper::checkSession();
        if (!isset($initialData)) {
            return redirect("/");
        }
        if ($initialData->payload->user->role != 1) {
            return redirect("/");
        }
        $dataReferal = CommonHelper::getAPI("/api/referal/referal_mining");
        // print_r($dataWallet);
        return view(
            "User.Referal.mining",
            [
                "dataReferal" => $dataReferal->payload,
                "dataUser" => $initialData->payload
            ]
        );
    }

    public function harvestStaking()
    {
        $newRequest = Request::create('/api/referal/harvest_referal_staking', 'POST');
        $response = Route::dispatch($newRequest);
        $res = json_decode($response->getContent());
        if ($response->getStatusCode() == 200) {
            CommonHelper::showAlert("Harvest Referal Success", "Your balance automatically updated", "success", "back");
        } else {
            CommonHelper::showAlert("Harvest Referal Failed", $res->error_msg, "error", "back");
        }
    }

    public function harvestMining()
    {
        $newRequest = Request::create('/api/referal/harvest_referal_mining', 'POST');
        $response = Route::dispatch($newRequest);
        $res = json_decode($response->getContent());
        if ($response->getStatusCode() == 200) {
            CommonHelper::showAlert("Harvest Referal Success", "Your balance automatically updated", "success", "back");
        } else {
            CommonHelper::showAlert("Harvest Referal Failed", $res->error_msg, "error", "back");
        }
    }
}
