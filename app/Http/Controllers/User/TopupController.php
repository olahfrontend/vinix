<?php

namespace App\Http\Controllers\User;

use App\Helpers\CommonHelper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Route;

class TopupController extends Controller
{
    //
    public function index()
    {
        $initialData = CommonHelper::checkSession();
        if (!isset($initialData)) {
            return redirect("/");
        }
        if ($initialData->payload->user->role != 1) {
            return redirect("/");
        }
        $dataAddress = CommonHelper::getAPI("/api/coin/address");
        return view(
            "User.Topup.index",
            [
                "dataAddress" => $dataAddress->payload,
                "dataUser" => $initialData->payload
            ]
        );
    }

    public function generateAddress(Request $request)
    {
        $newRequest = Request::create('/api/coin/address/generate', 'POST');
        $response = Route::dispatch($newRequest);
        $res = json_decode($response->getContent());
        if ($response->getStatusCode() == 200) {
            CommonHelper::showAlert("Generate Address Success", "Your address succesfully generated", "success", "/topup");
        } else {
            CommonHelper::showAlert("Generate Address Fail", $res->error_msg, "error", "back");
        }
    }
}
