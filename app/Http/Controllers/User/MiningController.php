<?php

namespace App\Http\Controllers\User;

use App\Helpers\CommonHelper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Route;

class MiningController extends Controller
{
    //
    public function index()
    {
        $initialData = CommonHelper::checkSession();
        Input::merge([
            'type' => "ETH"
        ]);
        $dataProduct = CommonHelper::getAPI("/api/mining/product?type=ETH");
        if (!isset($initialData)) {
            return redirect("/");
        }
        if ($initialData->payload->user->role != 1) {
            return redirect("/");
        }
        return view(
            "User.Mining.index",
            [
                "dataUser" => $initialData->payload,
                "dataProduct" => $dataProduct->payload
            ]
        );
    }

    public function index2()
    {
        $initialData = CommonHelper::checkSession();
        Input::merge([
            'type' => "RVN"
        ]);
        $dataProduct = CommonHelper::getAPI("/api/mining/product?type=RVN");
        if (!isset($initialData)) {
            return redirect("/");
        }
        if ($initialData->payload->user->role != 1) {
            return redirect("/");
        }
        return view(
            "User.Mining.indexraven",
            [
                "dataUser" => $initialData->payload,
                "dataProduct" => $dataProduct->payload
            ]
        );
    }


    public function index3()
    {
        $initialData = CommonHelper::checkSession();
        Input::merge([
            'type' => "NFT"
        ]);
        // $dataProduct = CommonHelper::getAPI("/api/mining/product?type=NFT");
        if (!isset($initialData)) {
            return redirect("/");
        }
        if ($initialData->payload->user->role != 1) {
            return redirect("/");
        }

        return view(
            "User.Mining.indexnft",
            [
                "dataUser" => $initialData->payload,
                // "dataProduct" => $dataProduct->payload
            ]
        );
    }

    public function my_farm()
    {

        $initialData = CommonHelper::checkSession();
        $dataProduct = CommonHelper::getAPI("/api/mining/list_farm");
        if (!isset($initialData)) {
            return redirect("/");
        }
        if ($initialData->payload->user->role != 1) {
            return redirect("/");
        }
        return view(
            "User.Mining.myfarms",
            [
                "dataUser" => $initialData->payload,
                "dataFarms" => $dataProduct->payload
            ]
        );
    }

    public function start_mining()
    {
        $newRequest = Request::create('/api/mining/start', 'POST');
        $response = Route::dispatch($newRequest);
        $res = json_decode($response->getContent());
        if ($response->getStatusCode() == 200) {
            CommonHelper::showAlert("Success Start Mining", "Success start mining", "success", "/my-mining");
        } else {
            CommonHelper::showAlert("Mining Fail", $res->error_msg, "error", "back");
        }
    }

    public function do_harvest()
    {
        $newRequest = Request::create('/api/mining/harvest', 'POST');
        $response = Route::dispatch($newRequest);
        $res = json_decode($response->getContent());
        if ($response->getStatusCode() == 200) {
            CommonHelper::showAlert("Success Harvest Mining", "Success Harvest mining your balance automatically updated", "success", "/my-mining");
        } else {
            CommonHelper::showAlert("Harvest Failed", $res->error_msg, "error", "back");
        }
    }

    public function start_mining_custom()
    {
        $newRequest = Request::create('/api/mining/start_custom', 'POST');
        $response = Route::dispatch($newRequest);
        $res = json_decode($response->getContent());
        if ($response->getStatusCode() == 200) {
            CommonHelper::showAlert("Success Start Mining", "Success start mining", "success", "/my-mining");
        } else {
            CommonHelper::showAlert("Mining Fail", $res->error_msg, "error", "back");
        }
    }

    public function detailfarm($id)
    {
        $initialData = CommonHelper::checkSession();
        if (!isset($initialData)) {
            return redirect("/");
        }
        if ($initialData->payload->user->role != 1) {
            return redirect("/");
        }
        Input::merge([
            'id' => $id
        ]);
        $dataMining = CommonHelper::getAPI("/api/mining/detail");
        return view(
            "User.Mining.detailmining",
            [
                "dataUser" => $initialData->payload,
                "dataMining" => $dataMining->payload
            ]
        );
    }

    public function logfarm($id)
    {
        $initialData = CommonHelper::checkSession();
        if (!isset($initialData)) {
            return redirect("/");
        }
        Input::merge([
            'id' => $id
        ]);
        $dataMining = CommonHelper::getAPI("/api/mining/detail");
        $dataLog = CommonHelper::getAPI("api/mining/log");
        return view(
            "User.Mining.logmining",
            [
                "dataUser" => $initialData->payload,
                "dataMining" => $dataMining->payload,
                "dataLog" => $dataLog->payload
            ]
        );
    }
}
