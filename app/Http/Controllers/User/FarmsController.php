<?php

namespace App\Http\Controllers\User;

use App\Helpers\CommonHelper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Route;

class FarmsController extends Controller
{
    //
    public function index()
    {
        $initialData = CommonHelper::checkSession();
        if (!isset($initialData)) {
            return redirect("/");
        }
        if ($initialData->payload->user->role != 1) {
            return redirect("/");
        }
        $dataProduct = CommonHelper::getAPI("/api/staking/product");

        return view(
            "User.Farms.index",
            [
                "dataUser" => $initialData->payload,
                "dataProduct" => $dataProduct->payload
            ]
        );
    }

    public function doFarm(Request $request)
    {
        $newRequest = Request::create('/api/staking/farm', 'POST');
        $response = Route::dispatch($newRequest);
        $res = json_decode($response->getContent());
        if ($response->getStatusCode() == 200) {
            CommonHelper::showAlert("Staking Success", "Staking success", "success", "/my-farms");
        } else {
            CommonHelper::showAlert("Staking Fail", $res->error_msg, "error", "back");
        }
    }

    public function myfarms()
    {
        $initialData = CommonHelper::checkSession();
        if (!isset($initialData)) {
            return redirect("/");
        }
        if ($initialData->payload->user->role != 1) {
            return redirect("/");
        }
        $dataMyFarms = CommonHelper::getAPI("/api/list-farm");
        return view(
            "User.Farms.myfarm",
            [
                "dataUser" => $initialData->payload,
                "dataMyFarms" => $dataMyFarms->payload
            ]
        );
    }

    public function doHarvest(Request $request)
    {
        $newRequest = Request::create('/api/staking/harvest', 'POST');
        $response = Route::dispatch($newRequest);
        $res = json_decode($response->getContent());
        if ($response->getStatusCode() == 200) {
            CommonHelper::showAlert("Harvest Success", "Harvest success, your balance updated automatically", "success", "/my-farms");
        } else {
            CommonHelper::showAlert("Harvest Fail", $res->error_msg, "error", "back");
        }
    }

    public function doHarvestAll(Request $request)
    {
        $newRequest = Request::create('/api/staking/withdraw', 'POST');
        $response = Route::dispatch($newRequest);
        $res = json_decode($response->getContent());
        if ($response->getStatusCode() == 200) {
            CommonHelper::showAlert("Harvest Success", "Harvest success, your balance updated automatically", "success", "/my-farms");
        } else {
            CommonHelper::showAlert("Harvest Fail", $res->error_msg, "error", "back");
        }
    }

    public function detailfarm($id)
    {
        $initialData = CommonHelper::checkSession();
        if (!isset($initialData)) {
            return redirect("/");
        }
        if ($initialData->payload->user->role != 1) {
            return redirect("/");
        }

        Input::merge([
            'id' => $id
        ]);
        $dataStaking = CommonHelper::getAPI("/api/staking/detail");
        return view(
            "User.Farms.detailfarm",
            [
                "dataUser" => $initialData->payload,
                "dataStaking" => $dataStaking->payload
            ]
        );
    }
}
