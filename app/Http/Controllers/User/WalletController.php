<?php

namespace App\Http\Controllers\User;

use App\Helpers\CommonHelper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Route;

class WalletController extends Controller
{
    //
    public function index()
    {
        $initialData = CommonHelper::checkSession();
        if (!isset($initialData)) {
            return redirect("/");
        }
        if ($initialData->payload->user->role != 1) {
            return redirect("/");
        }
        $dataWallet = CommonHelper::getAPI("/api/get_saldo");
        // print_r($dataWallet);
        //EXCLUDE CURRENCY YANG TIDAK BISA DI TRANSFER
        $exclude = ["VNX", "USDV", "RVN"];
        return view(
            "User.Wallet.index",
            [
                "exclude" => $exclude,
                "dataWallet" => $dataWallet->payload,
                "dataUser" => $initialData->payload
            ]
        );
    }

    public function transfer()
    {
        $newRequest = Request::create('/api/coin/withdraw', 'POST');
        $response = Route::dispatch($newRequest);
        $res = json_decode($response->getContent());
        if ($response->getStatusCode() == 200) {
            CommonHelper::showAlert("Withdrawal Request Created", "Your Withdraw request created, please verify to confirm withdrawal.", "success", "/wallet");
        } else {
            CommonHelper::showAlert("Withdraw Request Failed", $res->error_msg, "error", "back");
        }
    }

    public function history_deposit()
    {
        $initialData = CommonHelper::checkSession();
        if (!isset($initialData)) {
            return redirect("/");
        }
        if ($initialData->payload->user->role != 1) {
            return redirect("/");
        }
        $dataWithdraw = CommonHelper::getAPI("/api/get_list_withdraw");
        // print_r($dataWallet);
        return view(
            "User.Wallet.history",
            [
                "dataWithdraw" => $dataWithdraw->payload,
                "dataUser" => $initialData->payload
            ]
        );
    }

    public function do_verify_withdrawal(Request $request)
    {
        $newRequest = Request::create('/api/coin/verify_withdraw', 'GET');
        $response = Route::dispatch($newRequest);
        // $res = json_decode($response->getContent());
        if ($response->getStatusCode() == 200) {
            return view("User.Wallet.landingverify", ["success" => true]);
        } else {
            return view("User.Wallet.landingverify", ["success" => false]);
        }
    }

    public function detail_history(Request $request)
    {
        $initialData = CommonHelper::checkSession();
        if (!isset($initialData)) {
            return redirect("/");
        }
        if ($initialData->payload->user->role != 1) {
            return redirect("/");
        }
        $newRequest = Request::create('/api/detail_history', 'GET');
        $response = Route::dispatch($newRequest);
        $res = json_decode($response->getContent());
        if ($response->getStatusCode() == 200) {
            return view("User.Wallet.detail_history", [
                "data" => $res->payload,
                "dataUser" => $initialData->payload
            ]);
        }
    }
}
