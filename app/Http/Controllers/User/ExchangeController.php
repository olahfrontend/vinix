<?php

namespace App\Http\Controllers\User;

use App\Helpers\CommonHelper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Route;

class ExchangeController extends Controller
{
    //
    public function index()
    {
        $dataCoin = CommonHelper::getAPI("/api/coin/get_all");
        $initialData = CommonHelper::checkSession();
        if (!isset($initialData)) {
            return redirect("/");
        }
        if ($initialData->payload->user->role != 1) {
            return redirect("/");
        }
        return view(
            "User.Exchange.index",
            [
                "dataUser" => $initialData->payload,
                "dataCoin" => $dataCoin->payload,
            ]
        );
    }

    public function doExchange(Request $request)
    {
        Input::merge([
            "from_coin" => $request->usd_from_currency,
            "to_coin" => $request->usd_to_currency,
            "amount" => $request->usd_from
        ]);

        // echo $request->usd_from_currency."\n";
        // echo $request->usd_to_currency."\n";
        // echo $request->usd_from;

        $newRequest = Request::create('/api/exchange', 'POST');
        $response = Route::dispatch($newRequest);
        $res = json_decode($response->getContent());
        if ($response->getStatusCode() == 200) {
            CommonHelper::showAlert("Exchange Success", "Exchange success", "success", "back");
        } else {
            CommonHelper::showAlert("Exchange Fail", $res->error_msg, "error", "back");
        }
    }
}
