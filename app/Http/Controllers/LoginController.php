<?php

namespace App\Http\Controllers;

use App\Helpers\CommonHelper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

class LoginController extends Controller
{
    //
    public function index()
    {
        $initialData = CommonHelper::checkSession();
        // print_r($initialData);
        if (isset($initialData)) {
            if ($initialData->payload->user->role == 2) {
                return redirect('/admin/dashboard');
            } else {
                return redirect('/home');
            }
        }
        return view("welcome");
    }

    public function register()
    {
        return view("register");
    }

    public function forgotpassword()
    {
        return view("forgotpassword");
    }

    public function resetpassword()
    {
        return view("resetpassword");
    }

    public function doRegister(Request $request)
    {
        if ($request->password != $request->cpassword) {
            CommonHelper::showAlert("Sign Up Failed", "Wrong confirmation password", "error", "/sign-up");
        } else {
            $newRequest = Request::create('/api/register', 'POST');
            $response = Route::dispatch($newRequest);
            $res = json_decode($response->getContent());
            if ($response->getStatusCode() == 200) {
                CommonHelper::showAlert("Sign Up Success", "verification email has been sent to your email, please verify your email to activate your account.", "success", "/landing_register");
            } else {
                CommonHelper::showAlert("Sign Up Failed", $res->error_msg, "error", "/sign-up");
            }
        }
    }

    public function landing_verify()
    {
        return view("landingverify");
    }

    public function doverifyemail(Request $request)
    {
        $newRequest = Request::create('/api/verify_email', 'GET');
        $response = Route::dispatch($newRequest);
        // $res = json_decode($response->getContent());
        if ($response->getStatusCode() == 200) {
            return view("landingverify", ["success" => true]);
        } else {
            return view("landingverify", ["success" => false]);
        }
    }

    public function doLogin(Request $request)
    {
        $newRequest = Request::create('/api/login', 'POST');
        $response = Route::dispatch($newRequest);
        $res = json_decode($response->getContent());
        if ($response->getStatusCode() == 200) {
            echo '
            <script>
                window.location.href = "home";
            </script>';
        } else {
            CommonHelper::showAlert("Sign In Failed", $res->error_msg, "error", "/");
        }
    }

    public function doLogout()
    {
        $newRequest = Request::create('/api/logout', 'POST');
        $response = Route::dispatch($newRequest);
        return redirect('/');
    }

    public function doRequestForgotCode(Request $request)
    {
        $newRequest = Request::create('/api/forgot_password', 'POST');
        $response = Route::dispatch($newRequest);
        $res = json_decode($response->getContent());
        if ($response->getStatusCode() == 200) {
            CommonHelper::showAlert("Request Success", "Request Reset password code had been sent to your email.", "success", "/forgot-password?wait=true");
        } else {
            CommonHelper::showAlert("Request Failed", $res->error_msg, "error", "/forgot-password");
        }
    }

    public function doResetPassword(Request $request)
    {
        if ($request->new_password == $request->cpassword) {
            $newRequest = Request::create('/api/change_password', 'POST');
            $response = Route::dispatch($newRequest);
            $res = json_decode($response->getContent());
            if ($response->getStatusCode() == 200) {
                CommonHelper::showAlert("Change Password Success", "Your Password has been changed, you can login again now.", "success", "/");
            } else {
                CommonHelper::showAlert("Change Password Failed", $res->error_msg, "error", "back");
            }
        } else {
            CommonHelper::showAlert("Change Password Failed", "Your Confirmation Password not same", "error", "back");
        }
    }
}
