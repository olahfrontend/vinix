<?php

namespace App\Console\Commands;

use App\Helpers\CoinBaseHelper;
use App\Helpers\CommonHelper;
use App\Models\ExchangeRates;
use App\Models\MasterCoin;
use App\Models\PriceCoin;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class ExchangeRateCryptoCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ExchangeRateCryptoCommand';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get Exchange Rate Crypto from other sources';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //ETH
        $price = CoinBaseHelper::getPriceTokoCrypto("ETH_BUSD");
        $harga = 0;
        $harga = $price["data"]["bids"][0][0];

        $id_coin = MasterCoin::where("code", "ETH")->first()->id;
        $check = PriceCoin::where("id_coin", $id_coin)->first();
        if (isset($check)) {
            $check->id_coin = $id_coin;
            $check->price = floatval($harga);
            $check->save();
        } else {
            $data = new PriceCoin();
            $data["id_coin"] = $id_coin;
            $data["price"] = floatval($harga);
            $data->save();
        }

        //BTC
        $price = CoinBaseHelper::getPriceTokoCrypto("BTC_BUSD");
        $harga = 0;
        $harga = $price["data"]["bids"][0][0];

        $id_coin = MasterCoin::where("code", "BTC")->first()->id;
        $check = PriceCoin::where("id_coin", $id_coin)->first();
        if (isset($check)) {
            $check->id_coin = $id_coin;
            $check->price = floatval($harga);
            $check->save();
        } else {
            $data = new PriceCoin();
            $data["id_coin"] = $id_coin;
            $data["price"] = floatval($harga);
            $data->save();
        }

        //VINIX -> 80% DARI CAKE
        $price = CoinBaseHelper::getPriceTokoCrypto("CAKE_BUSD");
        $harga = 0;
        $harga = $price["data"]["bids"][0][0];
        //CONVERT KE VINIX
        $harga = $harga * 0.8;

        $id_coin = MasterCoin::where("code", "VNX")->first()->id;
        $check = PriceCoin::where("id_coin", $id_coin)->first();
        if (isset($check)) {
            $check->id_coin = $id_coin;
            $check->price = floatval($harga);
            $check->save();
        } else {
            $data = new PriceCoin();
            $data["id_coin"] = $id_coin;
            $data["price"] = floatval($harga);
            $data->save();
        }

        //BUSD
        $harga = 1;
        $id_coin = MasterCoin::where("code", "USDC")->first()->id;
        $check = PriceCoin::where("id_coin", $id_coin)->first();
        if (isset($check)) {
            $check->id_coin = $id_coin;
            $check->price = floatval($harga);
            $check->save();
        } else {
            $data = new PriceCoin();
            $data["id_coin"] = $id_coin;
            $data["price"] = floatval($harga);
            $data->save();
        }

        //USD
        $harga = 1;
        $id_coin = MasterCoin::where("code", "USDV")->first()->id;
        $check = PriceCoin::where("id_coin", $id_coin)->first();
        if (isset($check)) {
            $check->id_coin = $id_coin;
            $check->price = floatval($harga);
            $check->save();
        } else {
            $data = new PriceCoin();
            $data["id_coin"] = $id_coin;
            $data["price"] = floatval($harga);
            $data->save();
        }

        //RVN
        $price = CommonHelper::getAPIExternal("https://api.nanopool.org/v1/rvn/prices");
        $harga = 0;
        $harga = $price["data"]["price_usd"];

        if (isset($price)) {
            $id_coin = MasterCoin::where("code", "RVN")->first()->id;
            $check = PriceCoin::where("id_coin", $id_coin)->first();
            if (isset($check)) {
                $check->id_coin = $id_coin;
                $check->price = floatval($harga);
                $check->save();
            } else {
                $data = new PriceCoin();
                $data["id_coin"] = $id_coin;
                $data["price"] = floatval($harga);
                $data->save();
            }
        }
    }
}
