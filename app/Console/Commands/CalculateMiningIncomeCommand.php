<?php

namespace App\Console\Commands;

use App\Helpers\CommonHelper;
use App\Models\LogTrxMining;
use App\Models\MasterCoin;
use App\Models\MasterMining;
use App\Models\PriceCoin;
use App\Models\SettingGeneral;
use App\Models\TrxHistory;
use App\Models\TrxMining;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class CalculateMiningIncomeCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'CalculateMiningIncomeCommand';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Hitung hasil mining harian dari semua user yang melakukan mining';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        //GET HARGA MHS_ETH_PER_DAY
        $url = "https://api.nanopool.org/v1/eth/approximated_earnings/1";
        $data = CommonHelper::getAPIExternal($url);
        error_log($data["data"]["day"]["coins"]);
        $percentage_speed_mhs = SettingGeneral::where("setting_name", "PERCENTAGE_SPEED_MHS")->first()->value;

        //GET VALUE MHS ETH
        if (isset($data)) {
            $mhs_eth = SettingGeneral::where("setting_name", "MHS_ETH_PER_DAY")->first();
            $mhs_eth->value = $data["data"]["day"]["coins"] * $percentage_speed_mhs;
            $mhs_eth->save();
        }
        //GET HARGA MHS_RVN_PER_DAY
        $url = "https://api.nanopool.org/v1/rvn/approximated_earnings/1";
        $data = CommonHelper::getAPIExternal($url);
        error_log($data["data"]["day"]["coins"]);

        //GET VALUE MHS RVN TO USD
        if (isset($data)) {
            $mhs_rvn = SettingGeneral::where("setting_name", "MHS_RVN_PER_DAY")->first();
            $mhs_rvn->value = $data["data"]["day"]["coins"] * $percentage_speed_mhs;
            $mhs_rvn->save();
        }
        $date_now = date("Y-m-d H:i:s");
        $data_trx_mining = TrxMining::where("status", 0)->get();
        foreach ($data_trx_mining as $dtm) {
            // error_log($dtm->id);
            $data_log = LogTrxMining::where("id_trx_mining", $dtm->id)->orderBy("created_at", "DESC")->first();
            // error_log($data_log->created_at);
            // error_log(isset($data_log->created_at));
            if (isset($data_log->created_at)) {
                $date_last_harvest = $data_log->created_at;
            } else {
                $date_last_harvest = $dtm->last_harvest;
            }
            $selisih_date_harvest = CommonHelper::getSelisihMinutes($date_last_harvest, $date_now);
            // error_log($selisih_date_harvest);
            if ($selisih_date_harvest >= 60) {
                //CEK APAKAH MINING MAU SELESAI
                $date_end_at = CommonHelper::convertDateFormat($dtm->end_at);
                $selisih_date_end = CommonHelper::getSelisihMinutes($date_end_at, $date_now);

                $data_product = MasterMining::getData($dtm->id_product_mining);

                //HITUNG INCOME MINING
                if ($selisih_date_end >= 0) {
                    //SELESAIKAN MINING
                    $dtm->status = 1;
                    $selisih_date_harvest = CommonHelper::getSelisihMinutes($date_last_harvest, $date_end_at);
                }

                $id_coin_reward = $dtm->id_coin_reward;
                if ($dtm->id_coin_reward == null) {
                    $id_coin_reward = $data_product->id_coin_reward;
                }
                $kode_coin = MasterCoin::getKodeCoin($id_coin_reward);
                $mhs_to_usd = SettingGeneral::where("setting_name", "MHS_" . $kode_coin . "_PER_DAY")->first();

                $income_mining = (($mhs_to_usd->value / 24 / 60) * $dtm->mining_speed) * $selisih_date_harvest;

                //CONVERT PROFIT USD TO KOIN REWARD
                // $id_usdv = MasterCoin::getIdCoin("USDV");
                // $income_mining = PriceCoin::convertCoin($id_usdv, $id_coin_reward, $income_mining);

                $log = new LogTrxMining();
                $log->id_trx_mining = $dtm->id;
                $log->earning = $income_mining;
                $log->save();

                $dtm->can_harvest = $dtm->can_harvest + $income_mining;
                $dtm->save();
                error_log("harvested trx_mining_id : " . $dtm->id . ", last harvest : " . $date_last_harvest . ", income : " . $income_mining);
            }
        }
    }
}
