<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MasterMining extends Model
{
    //
    protected $table = 'master_mining';

    public static function getData($id_product_mining){
        $data = MasterMining::where("id",$id_product_mining)->first();
        if(!isset($data)){
            return null;
        }
        return $data;
    }
}
