<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SettingFee extends Model
{
    //
    protected $table = 'setting_fee';

    public static function getValue($fee_name){
        $result = SettingFee::where("fee_name",$fee_name)->first();
        if(!isset($result)){
            return null;
        }
        return $result;
    }
}
