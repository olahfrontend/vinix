<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MasterCoin extends Model
{
    //
    protected $table = 'master_coin';

    public static function getIdCoin($type)
    {
        //GET ID COIN DARI KODENYA
        $id_coin = MasterCoin::where("code", $type)->first();
        if(isset($id_coin->id)){
            return $id_coin->id;
        }
        else{
            return null;
        }
    }

    public static function getKodeCoin($id_coin){
        //GET KODE KOIN BERDASARKAN ID KOIN
        $result = MasterCoin::where("id",$id_coin)->first();
        if(isset($result)){
            return $result->code;
        }
        return null;
    }
    
    public static function getURLCoin($id_coin){
        //GET KODE KOIN BERDASARKAN ID KOIN
        $result = MasterCoin::where("id",$id_coin)->first();
        if(isset($result)){
            return $result->url_icon;
        }
        return null;
    }
}
