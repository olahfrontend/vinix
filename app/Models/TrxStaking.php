<?php

namespace App\Models;

use App\Helpers\CommonHelper;
use Illuminate\Database\Eloquent\Model;
use stdClass;

class TrxStaking extends Model
{
    //
    protected $table = 'trx_staking';

    public static function getDetailStaking($id_user, $id_trx_staking)
    {
        $result = TrxStaking::where("id", $id_trx_staking)
            ->where("id_user", $id_user)
            ->first();
        if (isset($result)) {
            return $result;
        }
        return null;
    }

    public static function getCoinReward($id_trx_staking)
    {
        $data = TrxStaking::where("id", $id_trx_staking)->first();

        $result = MasterStaking::where("id", $data->id_product_staking)->first();

        return $result->id_coin_reward;
    }

    public static function getTotalCanHarvestStaking($id_coin_reward, $id_user)
    {
        $data_trx_staking = MasterStaking::where("master_staking.id_coin_reward", $id_coin_reward)
            ->join("trx_staking", "trx_staking.id_product_staking", "master_staking.id")
            ->where("trx_staking.id_user", $id_user)
            ->where("trx_staking.status", "0")
            ->get();

        $date_now = date("Y-m-d H:i:s");
        $total_can_harvest = 0;
        $total_staking = 0;
        foreach ($data_trx_staking as $dt) {
            $total_staking = $total_staking + $dt->amount_stake;
            //HITUNG PROFIT YANG DAPAT DIHARVEST
            $selisih = CommonHelper::getSelisihMinutes($dt->harvest_at, $date_now);
            $profit = $dt->profit;
            $profit = floatval(($profit / 100) / 30 / 24 / 60) * $selisih;

            $hasil_farming = $profit * $dt->amount_stake;

            //POTONG SHARING PROFIT
            $sp = SharingProfit::getSharingProfit($dt->id_coin_reward, $dt->amount_stake);
            $sharing_profit = $hasil_farming * ($sp->sharing_profit / 100);
            $hasil_farming = $hasil_farming - $sharing_profit;
            $total_can_harvest = $total_can_harvest + $hasil_farming;
        }
        $data = new stdClass();
        $data->total_can_harvest = $total_can_harvest;
        $data->total_staking = $total_staking;
        return $data;
    }
}
