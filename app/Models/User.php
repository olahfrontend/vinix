<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class User extends Model
{
    //
    protected $table = 'users';
    protected $primaryKey = 'id';

    public static function getSaldo($type, $iduser)
    {
        //GET SALDO USER BERDASARKAN TIPE COIN
        $id_coin = MasterCoin::getIdCoin($type);
        // echo $id_coin;
        $amount = DB::table('trx_history')->where("id_coin", $id_coin)->where("id_user", $iduser)->where("status", 1)->sum("amount");

        // print_r($amount);
        return $amount;
    }

    public static function getData($iduser)
    {
        $data = User::where("id", $iduser)->first();
        return $data;
    }

    public static function getDataFromReferal($referal_code)
    {
        $data = User::where("referal_code", $referal_code)->first();
        return $data;
    }
}
