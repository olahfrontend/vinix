<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TrxReferal extends Model
{
    //
    protected $table = 'trx_referal';

    public static function insertData($id_user, $id_ref, $type, $last_harvest, $level)
    {
        $data = new TrxReferal();
        $data->id_user = $id_user;
        $data->id_ref = $id_ref;
        $data->status = 0;
        $data->type = $type;
        $data->last_harvest = $last_harvest;
        $data->level = $level;
        $data->save();
    }
}
