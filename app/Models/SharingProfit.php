<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SharingProfit extends Model
{
    //
    protected $table = 'sharing_profit';

    public static function getSharingProfit($id_coin, $amount)
    {
        $data = SharingProfit::where("id_coin", $id_coin)->orderBy("min_value")->get();
        if (!isset($data)) {
            return null;
        }
        foreach ($data as $dt) {
            if ($amount >= $dt->min_value && $amount <= $dt->max_value) {
                return $dt;
            }
        }
        return null;
    }

    
}
