<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MasterStaking extends Model
{
    //
    protected $table = 'master_staking';

    public static function getStakingData($id_product){
        $result = MasterStaking::where("id",$id_product)->first();
        if(isset($result)){
            return $result;
        }
        return null;
    }

}
