<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PriceCoin extends Model
{
    protected $table = 'price_coin';
    //

    public static function getPrice($type)
    {
        //GET PRICE COIN DARI TYPENYA
        $id_coin = MasterCoin::getIdCoin($type);
        if (!isset($id_coin)) {
            return null;
        }

        $data = PriceCoin::where("id_coin", $id_coin)->first();
        if (!isset($data)) {
            return null;
        }
        return $data;
    }

    public static function getPricebyId($id)
    {
        $data = PriceCoin::where("id_coin", $id)->first();
        if (!isset($data)) {
            return null;
        }
        return $data;
    }

    public static function convertCoin($id_from, $id_to, $amount)
    {
        // echo $id_from;
        $price_coin_from = PriceCoin::getPricebyId($id_from)->price;
        $price_coin_to = PriceCoin::getPricebyId($id_to)->price;

        $rate = $price_coin_from / $price_coin_to;

        return $amount * $rate;
    }
}
