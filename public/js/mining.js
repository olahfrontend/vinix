var price = 0;

$(document).ready(function () {
    sliderChange();
});

//FUNCTION MINING NORMAL
function openModal(data) {
    $("#modalBuy").modal();
    price = data.price;
    $("#id_product_mining").val(data.id);
    $("#modal-name").html(data.product_name);
    $("#modal-type").html(data.product_type);
    $("#modal-tenor").html(data.tenor + " Months");
    $("#modal-speed").html(data.mining_speed + " MHS");
    $("#modal-earn").html("$" + data.earn_per_day);
    changeWallet();
}

function changeWallet() {
    $("#modal-price-loading").css("display", "block");
    $("#modal-price").css("display", "none");
    convertPrice();
    var saldo = 0;
    walletUser.forEach(function (item, index) {
        if (item.id == $("#cbWallet").val()) {
            saldo = item.saldo;
        }
    });
    $("#balance").html("BALANCE : " + saldo + " " + $("#cbWallet option:selected").text());
    $("#id_coin_currency").val($("#cbWallet").val());
}

function convertPrice() {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            var jsonResponse = $.parseJSON(this.response);
            $("#modal-price-loading").css("display", "none");
            $("#modal-price").css("display", "block");
            $("#modal-price").html(jsonResponse.payload + " " + $("#cbWallet option:selected").text());
        }
    };
    xhttp.open("GET", "/api/convert_fee?to_coin=" + $("#cbWallet option:selected").text() + "&amount=" + price,
        true);
    xhttp.send();
}

//FUNCTION CUSTOM MINING
function openModalCustom($type_coin) {
    var mining_speed = $("#sliderSpeed").val();
    var earn_perday = 0;
    if ($type_coin == "ETH") {
        price = mining_speed * settingGeneral.PRICE_MHS_USD.value;
        earn_perday = mining_speed * settingGeneral.MHS_ETH_PER_DAY.value;
    } else if ($type_coin == "RVN") {
        price = mining_speed * settingGeneral.PRICE_MHS_USD.value;
        earn_perday = mining_speed * settingGeneral.MHS_RVN_PER_DAY.value;
    }

    $("#mining_speed_custom").val(mining_speed);
    $("#type_coin_reward").val($type_coin);
    $("#tenor").val(24);

    $("#modal-name-custom").html("CUSTOM");
    $("#modal-type-custom").html("CUSTOM MINING");
    $("#modal-tenor-custom").html("24 Months");
    $("#modal-speed-custom").html(mining_speed + " MHS");
    $("#modal-earn-custom").html("$" + earn_perday);
    changeWalletCustom();
    $("#modalBuyCustom").modal();
}

function changeWalletCustom() {
    $("#modal-price-loading-custom").css("display", "block");
    $("#modal-price-custom").css("display", "none");
    convertPriceCustom();
    var saldo = 0;
    walletUser.forEach(function (item, index) {
        if (item.id == $("#cbWalletCustom").val()) {
            saldo = item.saldo;
        }
    });
    $("#balance-custom").html("BALANCE : " + saldo + " " + $("#cbWalletCustom option:selected").text());
    $("#id_coin_currency_custom").val($("#cbWalletCustom").val());
}

function convertPriceCustom() {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            var jsonResponse = $.parseJSON(this.response);
            $("#modal-price-loading-custom").css("display", "none");
            $("#modal-price-custom").css("display", "block");
            $("#modal-price-custom").html(jsonResponse.payload + " " + $("#cbWalletCustom option:selected").text());
        }
    };
    xhttp.open("GET", "/api/convert_fee?to_coin=" + $("#cbWalletCustom option:selected").text() + "&amount=" + price,
        true);
    xhttp.send();
}

function sliderChange() {
    var speed = $("#sliderSpeed").val();
    price = speed * settingGeneral.PRICE_MHS_USD.value;
    $("#speed").html(speed + " MHS");
    $("#custom-price").html("$" + numberWithCommas(price));
}

function sliderChangeNFT() {
    var speed = $("#sliderSpeed").val();
    price = speed * settingGeneral.PRICE_AETHER_USD.value;
    $("#speed").html(speed + " AETHER");
    $("#custom-price").html("$" + numberWithCommas(price));
}

function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}