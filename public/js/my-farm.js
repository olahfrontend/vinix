function harvest(data) {
    console.log(data);
    $("#modal-name").html(data.product.product_name);
    $("#modal-apr").html(data.product.APR + "%");
    $("#modal-earn").html(data.coin_reward);
    $("#modal-start").html(data.created_at_format);
    $("#modal-end").html(data.end_at_format);
    $("#modal-last").html(data.harvest_at_format + "(" + data.selisih_harvest + " days)");
    $("#modal-staked").html(data.amount_stake + " " + data.coin_reward);
    $("#modal-earned").html(data.can_harvest + " " + data.coin_reward);
    $("#modal-fee").html(data.fee + " " + data.coin_reward);
    $("#modal-total").html((data.can_harvest - data.fee) + " " + data.coin_reward);
    $("#modal-img").attr("src", data.url_coin);
    if(data.coin_reward == "VNX"){
        $("#modal-double").html("20X");
    }
    else{
        $("#modal-double").html("5X");
    }
    if (data.can_harvest - data.fee <= 0) {
        $("#modal-total").css('color', 'red');
        $(".warning").css('visibility', "visible");
        $("#btnHarvest").attr('disabled', "disabled");
    }
    else {
        $("#modal-total").css('color', 'green');
        $(".warning").css('visibility', "hidden");
        $("#btnHarvest").removeAttr("disabled", "disabled");
    }
    $("#id_trx_staking").val(data.id);
    $("#harvestModal").modal();
}


function harvestall(data) {
    console.log(data);
    if (data.product.progress_percentage >= 100) {
        harvest(data);
    }
    else {
        //WITHDRAW STAKE
        $("#harvestAllModal").modal();
        $("#modalall-name").html(data.product.product_name);
        $("#modalall-apr").html(data.product.APR + "%");
        $("#modalall-earn").html(data.coin_reward);
        $("#modalall-start").html(data.created_at_format);
        $("#modalall-end").html(data.end_at_format);
        $("#modalall-last").html(data.harvest_at_format);
        $("#modalall-total").html(data.can_harvest - data.fee);
        $("#modalall-staked").html(data.amount_stake);
        $("#modalall-commitment").html(data.amount_stake * (data.product.comitment_fee / 100) * -1);
        $("#modal-img2").attr("src", data.url_coin);
        if(data.coin_reward == "VNX"){
            $("#modal-double").html("20X");
        }
        else{
            $("#modal-double").html("5X");
        }
        var withdraw = data.amount_stake - (data.amount_stake * (data.product.comitment_fee / 100));
        $("#modalall-withdraw").html(withdraw);

        if (data.can_harvest - data.fee < 0) {
            $("#modalall-total").css("color", "red");
        }
        else {
            $("#modalall-total").css("color", "green");
        }
        $("#modalall-commitment").css("color", "red");
        $("#id_trx_staking2").val(data.id);
    }
}