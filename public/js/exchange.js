var selectableCoin = "from";

//INPUT FORM USD
var input_usd_from = document.getElementById("usd_from");
var input_usd_to = document.getElementById("usd_to");

//CURRENCY FORM USD
var usd_from_currency = document.getElementById("usd_from_currency");
var usd_to_currency = document.getElementById("usd_to_currency");

//CONTAINER FORM USD
var usd_from_container = document.getElementById("usd_from_container");
var usd_to_container = document.getElementById("usd_to_container");

//ICON FORM USD
var usd_from_icon = document.getElementById("icon_usd_from");
var usd_to_icon = document.getElementById("icon_usd_to");

//ID FORM USD
var usd_from_id = document.getElementById("id_usd_from");
var usd_to_id = document.getElementById("id_usd_to");

var rate = 0;
var modal_fee = fee_exchange;

$(document).ready(function () {
    getRate();
});

function clickSwap() {


    // console.log(input_usd_to.value);

    var temp = input_usd_from.value;
    input_usd_from.value = input_usd_to.value;
    input_usd_to.value = temp;

    temp = usd_from_icon.src;
    usd_from_icon.src = usd_to_icon.src;
    usd_to_icon.src = temp;

    temp = usd_from_currency.innerHTML;
    usd_from_currency.innerHTML = usd_to_currency.innerHTML;
    usd_to_currency.innerHTML = temp;

    usd_from_id.value = usd_from_currency.innerHTML;
    usd_to_id.value = usd_to_currency.innerHTML;

    temp = usd_from_id.value;
    usd_from_id.value = usd_to_id.value;
    usd_to_id.value = temp

    // if (usd_from_container.classList.contains("fix")) {
    //     selectableCoin = "from";
    //     usd_from_container.classList.remove("fix");
    //     usd_to_container.classList.add("fix");
    // }
    // else {
    //     selectableCoin = "to";
    //     usd_to_container.classList.remove("fix");
    //     usd_from_container.classList.add("fix");
    // }
    // console.log(temp);
    convertFee();
    getRate();
    console.log(selectableCoin);
}

function SelectToken(id, url_icon, name) {
    input_usd_to.value = 0;
    input_usd_from.value = 0;
    console.log(selectableCoin);
    if (selectableCoin == "from") {
        usd_from_id.value = id;
        usd_from_currency.innerHTML = name;
        usd_from_icon.src = url_icon;
    }
    else {
        usd_to_id.value = id;
        usd_to_currency.innerHTML = name;
        usd_to_icon.src = url_icon;
        convertFee();
    }

    getRate();

    $("#myModal").modal('hide');

}

function getRate() {

    walletUser.forEach(function (item, index) {
        if (item.code == usd_from_currency.innerHTML) {
            var num = item.saldo;
            if (num > 10) {
                $("#saldo_from").html("Balance : " + parseFloat(num).toFixed(2));
            }
            else {
                $("#saldo_from").html("Balance : " + num);
            }
        }
        if (item.code == usd_to_currency.innerHTML) {
            var num = item.saldo;
            if (num > 10) {
                $("#saldo_to").html("Balance : " + parseFloat(num).toFixed(2));
            }
            else {
                $("#saldo_to").html("Balance : " + num);
            }
        }

    });
    usd_from_id.value = usd_from_currency.innerHTML;
    usd_to_id.value = usd_to_currency.innerHTML;
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            var jsonResponse = $.parseJSON(this.response);
            rate = jsonResponse.payload.rate;
        }
        console.log(this.readyState);
    };
    xhttp.open("GET", "/api/get_rate?from_coin=" + usd_from_currency.innerHTML + "&to_coin=" + usd_to_currency.innerHTML, true);
    xhttp.send();
}

function convertFee() {
    usd_to_id.value = usd_to_currency.innerHTML;
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            var jsonResponse = $.parseJSON(this.response);
            modal_fee = jsonResponse.payload;
        }
    };
    xhttp.open("GET", "/api/convert_fee?to_coin=" + usd_to_currency.innerHTML + "&amount=" + fee_exchange,
        true);
    xhttp.send();
}

function openModal(type) {
    if (type == "from") {
        selectableCoin = "from"
        $("#myModal").modal('toggle');
    }
    else {
        selectableCoin = "to";
        $("#myModal").modal('toggle');
    }
}

function inputUSDChange(type) {

    if (type == "from") {
        var num = input_usd_from.value;
        walletUser.forEach(function (item) {
            if (item.code == usd_from_currency.innerHTML) {
                var saldo = item.saldo;
                if (num > saldo) {
                    //number lebih besar dari saldo
                    input_usd_from.value = saldo;
                }
            }
        });
        input_usd_to.value = input_usd_from.value * rate;
    }
    else {
        var num = input_usd_to.value;
        walletUser.forEach(function (item) {
            if (item.code == usd_from_currency.innerHTML) {
                var saldo = item.saldo;
                if (num > saldo) {
                    //number lebih besar dari saldo
                    input_usd_from.value = saldo;
                }
            }
        });
        input_usd_from.value = input_usd_to.value / rate;
    }
}

function confirmModal() {
    $("#confirmmodal-from").html(input_usd_from.value + " " + usd_from_currency.innerHTML);
    $("#confirmmodal-to").html(input_usd_to.value + " " + usd_to_currency.innerHTML);
    $("#confirmmodal-fee").html(modal_fee + " " + usd_to_currency.innerHTML);
    $("#confirmmodal-total").html((input_usd_to.value - modal_fee) + " " + usd_to_currency.innerHTML);
    if (input_usd_to.value - modal_fee <= 0) {
        $("#confirmmodal-total").css("color", "red");
    }
    else {
        $("#confirmmodal-total").css("color", "green");
    }
    // console.log(fee_exchange);
    $("#confirmModal").modal();
}

function exchange() {
    $("#formExchange").submit();
}

function clickMax(type) {
    console.log(usd_from_currency.innerHTML);
    if (type == "from") {
        walletUser.forEach(function (item) {
            if (item.code == usd_from_currency.innerHTML) {
                console.log(item.saldo);
                var saldo = item.saldo;
                input_usd_from.value = saldo + 0;
                input_usd_to.value = input_usd_from.value * rate;
            }
        });
    }
    else {
        walletUser.forEach(function (item) {
            if (item.code == usd_to_currency.innerHTML) {
                var saldo = item.saldo;
                input_usd_to.value = saldo;
                input_usd_from.value = input_usd_to.value * rate;
            }
        });
    }
    inputUSDChange(type);
}
