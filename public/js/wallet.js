function showModalTransfer(data) {
    console.log(data);
    console.log(fee);
    $("#modal-name").html(data.coin_name);
    $("#input_currency").val(data.code);
    if (data.code == "ETH") {
        $("#modal-fee").html(fee.WITHDRAW_FEE_ETH.value);
    } else if (data.code == "BTC") {
        $("#modal-fee").html(fee.WITHDRAW_FEE_BTC.value);
    } else if (data.code == "USDC") {
        $("#modal-fee").html(fee.WITHDRAW_FEE_USDC.value);
    }
    $("#modal-image").attr("src", data.url_icon);
    $("#modal-saldo").html(data.saldo);
    $("#modalTransfer").modal();
}