function showAddress(data) {

    console.log(data);
    $("#modal-name").html(data.coin_name);
    $("#modal-saldo").html(data.saldo);
    if (data.address == null) {
        $(".container-address").css("display", "none");
        $("#modal-address").html("-");
        $("#formGenerate").css("display", "block");
    }
    else {
        $(".container-address").css("display", "block");
        $("#modal-address").html(data.address);
        $("#formGenerate").css("display", "none");
    }
    $("#modal-icon").attr("src", data.url_logo);
    $("#type").val(data.code);
    if (data.url_qr == null) {
        $("#container-qr").css("display","none");
        $("#img-qr").attr("src", null);
    }
    else {
        $("#container-qr").css("display","block");
        $("#img-qr").attr("src", data.url_qr);
    }
    $("#addressModal").modal();
}

function copyToClipBoard() {
    /* Get the text field */
    var temp = document.getElementById("modal-address").innerHTML;
    // console.log(copyText);
    $("#inp_copy").val(temp);
    // console.log($("#inp_copy").val());
    var copyText = document.getElementById("inp_copy");
    copyText.select();
    // copyText.setSelectionRange(0, 99999); /* For mobile devices */

    /* Copy the text inside the text field */
    document.execCommand("copy");
    $("#tooltip").css("opacity", "1");
    setTimeout(function () { $("#tooltip").css("opacity", "0") }, 1000)
}