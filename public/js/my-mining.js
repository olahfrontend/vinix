function harvestModal(data) {
    // console.log(data);
    $("#modal-name").html(data.product_name);
    $("#modal-type").html(data.product_type);
    $("#id_trx_mining").val(data.id);
    $("#modal-speed").html(data.mining_speed + " MHS");
    $("#modal-tenor").html(data.tenor + " Months");
    $("#modal-start").html(data.created_at_format);
    $("#modal-end").html(data.end_at_format);
    $("#modal-last").html(data.last_harvest_format + " (" + data.selisih + " days ago)");
    $("#modal-earned").html(data.can_harvest);
    $("#modal-fee").html(data.fee);
    $("#modal-total").html(data.can_harvest - data.fee);
    if (data.can_harvest - data.fee <= 0 || data.can_harvest < data.min_withdraw) {
        $("#modal-total").css("color", "red");
        $("#btnHarvest").attr("disabled", "disabled");
        if (data.can_harvest - data.fee <= 0) {
            $(".warning").css("display", "block");
            $(".warning-withdraw").css("display", "none");
        }
        else {
            $(".warning").css("display", "none");
            $(".warning-withdraw").css("display", "block");
            $(".warning-withdraw").html("Minimum withdraw not reached<br>Minimum withdrawal " + data.min_withdraw + " " + data.reward_coin);
        }
    }
    else {
        $("#modal-total").css("color", "green");
        $("#btnHarvest").removeAttr("disabled", "disabled");
        $(".warning").css("display", "none");
    }
    $("#harvestModal").modal();
}