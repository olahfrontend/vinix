var price = 0;

$(document).ready(function () {
    sliderChange();
});

//FUNCTION CUSTOM MINING
function openModalCustom($type_coin) {
    price = $("#sliderSpeed").val();
    var mining_speed = price / settingGeneral.PRICE_AETHER_USD.value;
    var earn_perday = 0;
    if ($type_coin == "AETHER") {
        earn_perday = mining_speed * 24;
    }

    $("#mining_speed_custom").val(mining_speed);
    $("#type_coin_reward").val($type_coin);
    $("#tenor").val(24);

    $("#modal-name-custom").html("CUSTOM");
    $("#modal-type-custom").html("CUSTOM MINING");
    $("#modal-tenor-custom").html("24 Months");
    $("#modal-speed-custom").html(numberWithCommas(mining_speed) + " AETHER / Hour");
    $("#modal-earn-custom").html(numberWithCommas(earn_perday) + " AETHER");
    changeWalletCustom();
    $("#modalBuyCustom").modal();
}

function changeWalletCustom() {
    $("#modal-price-loading-custom").css("display", "block");
    $("#modal-price-custom").css("display", "none");
    convertPriceCustom();
    var saldo = 0;
    walletUser.forEach(function (item, index) {
        if (item.id == $("#cbWalletCustom").val()) {
            saldo = item.saldo;
        }
    });
    $("#balance-custom").html("BALANCE : " + saldo + " " + $("#cbWalletCustom option:selected").text());
    $("#id_coin_currency_custom").val($("#cbWalletCustom").val());
} 

$("#custom-price").bind('keyup mouseup', function () {
    priceChange();
});

function priceChange() {
    var price = $("#custom-price").val();
    if (price < 100) {
        $("#custom-price").val(100);
        price = 100;
    }
    else if (price > 100000) {
        $("#custom-price").val(100000);
        price = 100000;
    }
    $("#sliderSpeed").val(price);
    sliderChange();
}

function convertPriceCustom() {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            var jsonResponse = $.parseJSON(this.response);
            $("#modal-price-loading-custom").css("display", "none");
            $("#modal-price-custom").css("display", "block");
            $("#modal-price-custom").html(jsonResponse.payload + " " + $("#cbWalletCustom option:selected").text());
        }
    };
    xhttp.open("GET", "/api/convert_fee?to_coin=" + $("#cbWalletCustom option:selected").text() + "&amount=" + price,
        true);
    xhttp.send();
}

function sliderChange() {
    price = $("#sliderSpeed").val();
    speed = price / settingGeneral.PRICE_AETHER_USD.value;
    $("#speed").html(numberWithCommas(speed) + " AETHER / Hour");
    $("#custom-price").val(price);
}

function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}