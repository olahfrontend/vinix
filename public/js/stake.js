var modalStaking = document.getElementById("startStakingModal");
var modalprofit = 0;
var sharing_profit;

function startStaking(data) {
    console.log(data);
    console.log(data.product_name);
    $("#input_id_product").val(data.id);
    $("#modal-name").html(data.product_name);
    $("#modal-apr").html(data.APR + "%");
    $("#modal-earn").html(data.coin_reward);
    $("#modal-tenor").html(data.tenor + " Month");
    $("#modal-from").html(data.coin_stake);
    $("#modal-fee").html(data.comitment_fee + "%");
    $("#amount").val(null);
    $("#modal-result").html("0.0");
    $("#modal-img").attr("src", data.url_coin);
    console.log(dataUser);
    dataUser.wallet.forEach(function (item, index) {
        if (item.code == data.coin_stake) {
            $("#modal-balance").html("Balance : " + item.saldo + " " + data.coin_stake);
        }
    });
    sharing_profit = [];
    var i = 0;
    dataUser.sharing_profit.forEach(function (item, index) {
        // console.log(item.id_coin);
        if (item.id_coin == data.id_coin_reward) {
            sharing_profit[i] = item;
            i = i + 1;
        }
    });
    $("#modal-sharing-profit").html(sharing_profit[0].sharing_profit + " %");
    modalprofit = data.profit;
    $("#startStakingModal").modal()
}

function amountChange() {
    var amount = $("#amount").val();
    var sharingprofit = 0;
    sharing_profit.forEach(function (item, index) {
        if (amount >= Number(item.min_value) && amount <= Number(item.max_value)) {
            sharingprofit = item.sharing_profit;
        }
    });

    var aprday = (modalprofit / 30);
    var coin_earned_perday = (aprday * amount) / 100;
    var fee_sharing_profit = coin_earned_perday * (sharingprofit / 100);
    var result = coin_earned_perday - fee_sharing_profit;
    // console.log(modalprofit / 30);

    $("#modal-coin-earned").html(coin_earned_perday.toFixed(8));
    $("#modal-sharing-profit").html(fee_sharing_profit.toFixed(8) + " (" + sharingprofit + " %)");
    $("#modal-result").html(result.toFixed(8));
}

function showInfo(APR) {
    $("#infoModal").modal();
    $("#apr_1d").html((APR / 365).toFixed(2) + "%");
    $("#get_1d").html("$" + (APR / 365).toFixed(2) * 1000 / 100);
    
    $("#apr_7d").html((APR / 48).toFixed(2) + "%");
    $("#get_7d").html("$" + (APR / 48).toFixed(2) * 1000 / 100);
    
    $("#apr_30d").html((APR / 12).toFixed(2) + "%");
    $("#get_30d").html("$" + (APR / 12).toFixed(2) * 1000 / 100);
    
    $("#apr_365d").html((APR).toFixed(2) + "%");
    $("#get_365d").html("$" + (APR).toFixed(2) * 1000 / 100);
}