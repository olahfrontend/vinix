<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::get('/', "LoginController@index");
Route::get("/sign-up", "LoginController@register");
Route::get("/forgot-password", "LoginController@forgotpassword");
Route::get("/reset-password", "LoginController@resetpassword");
Route::get("/landing_register", "LoginController@landing_verify");
Route::get("/verify_email", "LoginController@doverifyemail");
Route::get("/verify_withdraw", "User\WalletController@do_verify_withdrawal");

Route::post("/do-reset-password", "LoginController@doResetPassword");

Route::post("/register", "LoginController@doRegister");
Route::post("/do-login", "LoginController@doLogin");
Route::post("/logout", "LoginController@doLogout");
Route::post("/do-request-forgot", "LoginController@doRequestForgotCode");


//MAINTENANCE
// Route::get("/mining-eth", "User\HomeController@maintenance");
// Route::get("/mining-rvn", "User\HomeController@maintenance");
// Route::get("/mining-nft", "User\HomeController@maintenance");
// Route::get("/my-mining", "User\HomeController@maintenance");
// Route::get("/wallet", "User\HomeController@maintenance");
// Route::get("/referal", "User\HomeController@maintenance");
// Route::get("/history-deposit", "User\HomeController@maintenance");
// Route::get("/topup", "User\HomeController@maintenance");
// Route::get("/exchange", "User\HomeController@maintenance");
// Route::get("/home", "User\HomeController@maintenance");


Route::get("/topup", "User\TopupController@index");
Route::post("/topup/generateAddress", "User\TopupController@generateAddress");

Route::get("/exchange", "User\ExchangeController@index");
Route::post("/exchange/do_exchange", "User\ExchangeController@doExchange");

// STAKING
Route::get("/farms", "User\FarmsController@index");
Route::get("/my-farms", "User\FarmsController@myfarms");
Route::get("/my-farms/{id}", "User\FarmsController@detailfarm");
Route::post("/farms/do-farm", "User\FarmsController@doFarm");
Route::post("/farms/do-harvest", "User\FarmsController@doHarvest");
Route::post("/farms/do-harvestall", "User\FarmsController@doHarvestAll");

Route::get("/wallet", "User\WalletController@index");
Route::get("/home", "User\HomeController@index");

Route::post("/wallet/transfer", "User\WalletController@transfer");
Route::get("/wallet/detail", "User\WalletController@detail_history");

// MINING
Route::get("/mining-eth", "User\MiningController@index");
Route::get("/mining-rvn", "User\MiningController@index2");
Route::get("/mining-nft", "User\MiningController@index3");
Route::get("/my-mining", "User\MiningController@my_farm");
Route::post("mining/do-mining", "User\MiningController@start_mining");
Route::post("mining/do-mining-custom", "User\MiningController@start_mining_custom");
Route::post("/mining/do-harvest", "User\MiningController@do_harvest");
Route::get("/my-mining/{id}", "User\MiningController@detailfarm");
Route::get("/my-mining/log/{id}", "User\MiningController@logfarm");

Route::get("/referal", "User\ReferalController@index");
Route::get("/referal/staking", "User\ReferalController@staking");
Route::get("/referal/mining", "User\ReferalController@mining");
Route::post("/referal/do-harvest-staking", "User\ReferalController@harvestStaking");
Route::post("/referal/do-harvest-mining", "User\ReferalController@harvestMining");

Route::get("/history-deposit", "User\WalletController@history_deposit");

//ADMIN
Route::get("/admin", "Admin\LoginController@index");
Route::post("/admin/do-login", "Admin\LoginController@dologin");
Route::get("/admin/dashboard", "Admin\DashboardController@index");
Route::get("/admin/withdraw", "Admin\WithdrawController@index");

Route::get("/admin/master_staking", "Admin\MasterStakingController@index");
Route::get("/admin/master_staking/add", "Admin\MasterStakingController@add");
Route::post("/admin/master_staking/do_add", "Admin\MasterStakingController@do_add");
Route::get("/admin/master_staking/edit", "Admin\MasterStakingController@edit");
Route::post("/admin/master_staking/do_edit", "Admin\MasterStakingController@do_edit");

Route::get("/admin/master_mining", "Admin\MasterMiningController@index");
Route::get("/admin/master_mining/add", "Admin\MasterMiningController@add");
Route::post("/admin/master_mining/do_add", "Admin\MasterMiningController@do_add");
Route::get("/admin/master_mining/edit", "Admin\MasterMiningController@edit");
Route::post("/admin/master_mining/do_edit", "Admin\MasterMiningController@do_edit");

Route::get("/admin/master_setting", "Admin\MasterSettingController@index");
Route::post("/admin/setting/update_general", "Admin\MasterSettingController@update_general");
Route::post("/admin/setting/update_fee", "Admin\MasterSettingController@update_fee");

Route::get("/admin/master_user", "Admin\MasterUserController@index");
Route::get("/admin/master_user/{id}", "Admin\MasterUserController@detail");

Route::post("/admin/do_edit_mhs", "Admin\MasterMiningController@editmhs");
Route::post("/admin/do_add_custom_mining", "Admin\MasterMiningController@addcustommining");

Route::post("/admin/do_topup_admin", "Admin\MasterUserController@dotopup");

Route::get("/admin/deposit", "Admin\DepositController@index");
