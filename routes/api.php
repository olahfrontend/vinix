<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post("/register", "WebServices\UserWebServiceController@register");
Route::post("/login", "WebServices\UserWebServiceController@login");
Route::get("/get_saldo", "WebServices\UserWebServiceController@getSaldo");
Route::get("/initial_data", "WebServices\UserWebServiceController@getInitialData");
Route::post("/logout", "WebServices\UserWebServiceController@logout");
Route::post("/forgot_password", "WebServices\UserWebServiceController@forgotPassword");
Route::post("/change_password", "WebServices\UserWebServiceController@changePassword");
Route::get("/verify_email", "WebServices\UserWebServiceController@verifyemail");

Route::get("/master_user/get_all", "WebServices\UserWebServiceController@getAllUser");
Route::get("/master_user/get_detail", "WebServices\UserWebServiceController@getDetailUser");
Route::get("/get_dashboard_admin", "WebServices\UserWebServiceController@getDashboardAdmin");
Route::get("/get_dashboard_history_admin", "WebServices\UserWebServiceController@getDataHistoryDashboard");

//MASTER COIN
Route::get("/coin/get_all", "WebServices\CoinWebServiceController@getAllCoin");
Route::post("/coin/add", "WebServices\CoinWebServiceController@insertMasterCoin");
Route::post("/coin/update", "WebServices\CoinWebServiceController@updateMasterCoin");
Route::post("/coin/delete", "WebServices\CoinWebServiceController@deleteMasterCoin");
Route::get("/coin/address", "WebServices\CoinWebServiceController@getAddress");
Route::post("/coin/address/generate", "WebServices\CoinWebServiceController@generateAddress");
Route::post("/coin/callback", "WebServices\CoinWebServiceController@callback");
Route::post("/coin/withdraw", "WebServices\CoinWebServiceController@sendCoin");
Route::get("/coin/verify_withdraw", "WebServices\CoinWebServiceController@verifyWithdrawal");
Route::post("/coin/edit_address","WebServices\CoinWebServiceController@editAddress");

Route::get("/convert_fee", "WebServices\CoinWebServiceController@convertFee");


Route::get("/test", "WebServices\CoinWebServiceController@test");
Route::post("/post_test", "WebServices\CoinWebServiceController@Posttest");

//EXCHANGE
Route::post("/exchange", "WebServices\TrxExchangeWebServiceController@exchange");
Route::get("/get_rate", "WebServices\TrxExchangeWebServiceController@checkRateExchange");

//STAKING
Route::post("/staking/add", "WebServices\StakingWebServiceController@add_staking");
Route::get("/staking/product", "WebServices\StakingWebServiceController@get_product_staking");
Route::post("/staking/farm", "WebServices\StakingWebServiceController@farm_staking");
Route::post("/staking/harvest", "WebServices\StakingWebServiceController@harvest");
Route::get("/list-farm", "WebServices\StakingWebServiceController@myfarm");
Route::post("/staking/withdraw", "WebServices\StakingWebServiceController@withdraw");
Route::get("/staking/detail", "WebServices\StakingWebServiceController@detail");
Route::post("/staking/edit", "WebServices\StakingWebServiceController@edit");

//MINING
Route::get("/mining/product", "WebServices\MiningWebServiceController@get_product");
Route::post("/mining/add", "WebServices\MiningWebServiceController@add_master");
Route::post("/mining/start", "WebServices\MiningWebServiceController@start_mining");
Route::get("/mining/list_farm", "WebServices\MiningWebServiceController@list_mining");
Route::post("/mining/harvest", "WebServices\MiningWebServiceController@harvest");
Route::post("/mining/start_custom", "WebServices\MiningWebServiceController@start_mining_custom");
Route::get("/mining/detail", "WebServices\MiningWebServiceController@detail");
Route::post("/mining/edit", "WebServices\MiningWebServiceController@edit");
Route::post("/mining/start_custom_admin", "WebServices\MiningWebServiceController@start_mining_custom_admin");
Route::post("/mining/edit-mhs", "WebServices\MiningWebServiceController@edit_mhs");
Route::get("/mining/log","WebServices\MiningWebServiceController@logmining");

//REFERRAL
Route::get("/referal/myreferal", "WebServices\ReferalWebServiceController@get_my_referal");
Route::get("/referal/referal_staking", "WebServices\ReferalWebServiceController@get_referal_staking");
Route::get("/referal/referal_mining", "WebServices\ReferalWebServiceController@get_referal_mining");
Route::post("/referal/harvest_referal_staking", "WebServices\ReferalWebServiceController@harvest_referal_staking");
Route::post("/referal/harvest_referal_mining", "WebServices\ReferalWebServiceController@harvest_referal_mining");

Route::get("/get_dashboard", "WebServices\UserWebServiceController@getDashboard");

Route::get("/get_list_withdraw", "WebServices\TrxExchangeWebServiceController@getListWithdraw");
Route::post("/confirm_withdraw", "WebServices\TrxExchangeWebServiceController@confirmWithdraw");

Route::get("/setting/get_all_setting", "WebServices\SettingWebServiceController@get_all_setting");
Route::post("/setting/update_setting_general", "WebServices\SettingWebServiceController@update_setting_general");
Route::post("/setting/update_setting_fee", "WebServices\SettingWebServiceController@update_setting_fee");

Route::get("/detail_history", "WebServices\TrxExchangeWebServiceController@detail_history");

Route::post("/topup_admin", "WebServices\UserWebServiceController@topupadmin");
