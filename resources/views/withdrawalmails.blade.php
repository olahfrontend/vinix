<body style="color:black;">
    <h4>Hello,{{$data["name"]}}</h4>
    <h4>You create request withdrawal from your account, here the detail of withdrawal</h4>
    <table style="text-align:center;font-size:16px;border:1px solid black;">
        <thead style="font-weight:bold;background-color:#e05227;color:white;text-align:center;">
            <td style="padding:0px 5px; border:1px solid black;">To Address</td>
            <td style="padding:0px 5px; border:1px solid black;">Amount</td>
            <td style="padding:0px 5px; border:1px solid black;">Currency</td>
        </thead>
        <tbody style="text-align:left;">
            <tr>
                <td style="padding:0px 5px; border:1px solid black;">{{$data["address"]}}</td>
                <td style="padding:0px 5px; border:1px solid black;">{{$data["amount"]}}</td>
                <td style="padding:0px 5px; border:1px solid black;">{{$data["currency"]}}</td>
            </tr>
        </tbody>
    </table>
    <p>Click <a href="{{$data['link']}}">here</a> to confirm this withdrawal,<br> if you didn't do withdrawal on vinix you can contact our customer service</p>
</body>