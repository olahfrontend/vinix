@include('User.header',['activePage' => ''])

<link rel="stylesheet" href="{{ URL::asset('css/mining.css') }}">
<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div id="content1" style="margin-top:50px;">
            <img src="https://cdn.iconscout.com/icon/free/png-512/building-maintenance-2027068-1714203.png" width="200px" />
            <h1>VINIX IS UNDER MAINTENANCE</h1>
            <h4>The vinix server is under maintenance, we'll be back soon please be patient.<br> Don't worry, all staking and mining are still running as usual and your assets are still safe.</h4>
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->
@include('User.footer')