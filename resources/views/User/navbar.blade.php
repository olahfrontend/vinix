<!-- NAVBAR -->
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="brand">
        <a href="/"><img src="{{URL::asset('assets/img/logo-dark.png')}}" alt="Klorofil Logo" class="img-responsive logo"></a>
    </div>
    <div class="container-fluid">
        <div class="navbar-btn">
            <button type="button" class="btn-toggle-fullwidth"><i class="lnr lnr-arrow-left-circle"></i></button>
        </div>
        <div class="navbar-btn navbar-btn-right">
            <form action="{{URL('logout')}}" method="POST" id="form-logout">
                @csrf
                <a class="btn btn-primary btn-login" onclick="Logout()"><span>SIGN OUT</span></a>
            </form>
        </div>
        <div class="navbar-btn navbar-btn-right" style="margin-right:25px;">
            <h5 style="font-weight:bold;color:#E05227;margin:5px 0px 0px 5px;">BALANCE :</h5>
            <h4 style="margin:0px 0px 0px 5px;font-weight:bold;">${{number_format($dataUser->total_asset,2,'.',',')}}</h4>
        </div>
    </div>
</nav>
<!-- END NAVBAR -->
<!-- LEFT SIDEBAR -->
<div id="sidebar-nav" class="sidebar">
    <div class="sidebar-scroll">
        <nav>
            <ul class="nav">
                <li><a href="/home" class="{{$activePage == 'home' ? 'active' :''}}"><i class="lnr lnr-home"></i> <span>Home</span></a></li>
                <!-- <li><a href="/topup" class="{{$activePage == 'topup' ? 'active' :''}}"><i class="lnr lnr-cart"></i> <span>Deposit</span></a></li> -->
                <li><a href="/exchange" class="{{$activePage == 'exchange' ? 'active' : ''}}"><i class="fas fa-exchange-alt"></i> <span>Exchange</span></a></li>

                <!-- <li>
                    <a href="#subPages" data-toggle="collapse" class="{{$activePage == 'farms' || $activePage == 'my-farms' ? 'active' : 'collapsed'}}" aria-expanded="{{$activePage == 'farms' || $activePage == 'my-farms' ? 'true' : 'false'}}"><i class="fas fa-truck"></i> <span>Farms</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
                    <div id="subPages" class="{{$activePage == 'farms' || $activePage == 'my-farms' ? 'collapse in' : 'collapse'}}">
                        <ul class="nav">
                            <li><a href="/farms" class="{{$activePage == 'farms' ? 'active' : ''}}">Stake</a></li>
                            <li><a href="/my-farms" class="{{$activePage == 'my-farms' ? 'active' :''}}">My Farms</a></li>
                        </ul>
                    </div>
                </li> -->
                <li>
                    <a href="#subPages2" data-toggle="collapse" class="{{$activePage == 'mining-eth' || $activePage == 'mining-rvn' || $activePage == 'my-mining' || $activePage == 'mining-nft' ? 'active' : 'collapsed'}}" aria-expanded="{{$activePage == 'mining' || $activePage == 'my-mining' ? 'true' : 'false'}}"><i class="fas fa-server"></i> <span>Mining</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
                    <div id="subPages2" class="{{$activePage == 'mining-eth' ||$activePage == 'mining-rvn' || $activePage == 'my-mining' || $activePage == 'mining-nft' ? 'collapse in' : 'collapse'}}">
                        <ul class="nav">
                            <li>
                                <a href="#subPages3" data-toggle="collapse" class="{{$activePage == 'mining-eth' || $activePage == 'mining-rvn' || $activePage == 'mining-nft' ? 'active' : 'collapsed'}}" aria-expanded="false"><span>Start Mining</span></a>
                                <div id="subPages3" class="{{$activePage == 'mining-eth' || $activePage == 'mining-rvn' || $activePage == 'mining-nft' ?  'collapse in' : 'collapse'}}">
                                    <ul class="nav" style="margin-left:5px;">
                                        <li><a href="/mining-eth" class="{{$activePage == 'mining-eth' ? 'active' : ''}}"><i class="fa fa-chevron-right" style="font-size:12px"></i>Ethereum</a></li>
                                        <li><a href="/mining-rvn" class="{{$activePage == 'mining-rvn' ? 'active' : ''}}"><i class="fa fa-chevron-right" style="font-size:12px"></i>Raven</a></li>
                                        <li><a href="/mining-nft" class="{{$activePage == 'mining-nft' ? 'active' : ''}}"><i class="fa fa-chevron-right" style="font-size:12px"></i>NFT</a></li>
                                    </ul>
                                </div>
                            </li>
                            <li><a href="/my-mining" class="{{$activePage == 'my-mining' ? 'active' :''}}">My List</a></li>
                        </ul>
                    </div>
                </li>
                <li><a href="/wallet" class="{{$activePage == 'wallet' ? 'active' : ''}}"><i class="fa fa-wallet"></i> <span>Wallet</span></a></li>
                <li><a href="/referal" class="{{$activePage == 'referal' ? 'active' : ''}}"><i class="fa fa-link"></i><span>Referral</span></a></li>
           </ul>
        </nav>
    </div>
    <div class="navbar-footer">
        <h5>VINIX COIN</h5>
        <img src="{{URL::asset('image/logo-vnx.png')}}" width="25px" height="25px" /> ${{$dataUser->price_vnx->price}}
    </div>
</div>
<!-- END LEFT SIDEBAR -->