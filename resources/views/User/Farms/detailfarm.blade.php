@include('User.header',['activePage' => 'my-farms'])

<link rel="stylesheet" href="{{URL::asset('css/detailfarm.css')}}">
<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div id="content">
            <h1>{{$dataStaking->product->product_name}}</h1>
            <h5>{{$dataStaking->created_at}}</h5>

            <div class="container-desc">
                <div class="desc">
                    <h5>Start Staking</h5>
                    <p>{{$dataStaking->created_at}}</p>
                </div>
                <div class="desc">
                    <h5>End Staking</h5>
                    <p>{{$dataStaking->end_at}}</p>
                </div>
                <div class="desc">
                    <h5>Last Harvest</h5>
                    <p>{{$dataStaking->harvest_at}}</p>
                </div>
            </div>
            <div class="container-desc">
                <div class="desc">
                    <h5>Duration</h5>
                    <p>{{$dataStaking->product->tenor}} Month</p>
                </div>
                <div class="desc">
                    <h5>Comitment Fee</h5>
                    <p>{{$dataStaking->product->comitment_fee}}%</p>
                </div>
                <div class="desc">
                    <h5>Profit Monthly</h5>
                    <p>{{$dataStaking->product->profit}}%</p>
                </div>
            </div>
            <div class="desc">
                <h5>Amount Stake</h5>
                <p>{{$dataStaking->amount_stake}}</p>
            </div>

            <h5 style="font-weight:bold;">HISTORY TRANSACTION</h5>
            <div style="overflow-x:scroll">
                <table style="overflow-x:auto;">
                    <thead>
                        <th>Date Transaction</th>
                        <th>Description</th>
                        <th>Currency</th>
                        <th>Amount</th>
                        <th>Fee</th>
                        <th>Netto</th>
                    </thead>
                    <tbody>
                        @foreach($dataStaking->history as $dt)
                        <tr>
                            <td>{{$dt->created_at}}</td>
                            <td>{{$dt->trx_type}}</td>
                            <td>{{$dt->code}}</td>
                            <td>{{number_format($dt->total_fee + $dt->amount,8,'.',',')}}</td>
                            <td>{{$dt->total_fee}}</td>
                            <td>{{$dt->amount}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->
<script src="{{URL::asset('js/my-farm.js')}}"></script>
@include('User.footer')