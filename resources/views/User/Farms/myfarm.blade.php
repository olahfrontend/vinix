@include('User.header',['activePage' => 'my-farms'])

<link rel="stylesheet" href="{{URL::asset('css/farms.css')}}">
<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div id="content1">
            <h1>MY FARMS</h1>
            <h4>Control and Harvest your active cryptocurrency stake.</h4>
        </div>
        <!-- <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item active">
                <a class="nav-link" id="usd-tab" data-toggle="tab" href="#usd" role="tab" aria-controls="usd" aria-selected="true">USD</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="vinix-tab" data-toggle="tab" href="#vinix" role="tab" aria-controls="vinix" aria-selected="false">VINIX</a>
            </li>
        </ul> -->
        <!-- <div class="tab-content" id="myTabContent">
            <div class="tab-pane active" id="usd" role="tabpanel" aria-labelledby="usd-tab"> -->
        <div class="row">
            @foreach($dataMyFarms as $dt)
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="item">
                    <div class="desc">
                        <div class="left-side">
                            <img src="{{$dt->url_coin}}" width="50px" height="50px">
                        </div>
                        <h3 class="right-side title">
                            {{$dt->product->product_name}} <br>
                            <div class="sub-title">
                                <div class="core"><i class="fa fa-check-circle"></i> Core</div>
                                <div class="double">20X</div>
                            </div>
                        </h3>
                    </div>
                    <div class="desc">
                        <h4 class="left-side">P.A:</h4>
                        <h4 class="right-side">{{number_format($dt->product->APR)}}%</h4>
                    </div>
                    <div class="desc">
                        <h4 class="left-side">Earn:</h4>
                        <h4 class="right-side">{{$dt->coin_reward}}</h4>
                    </div>

                    <p class="title-result">PROGRESS</p>
                    <div class="progress">
                        <div class="progress-bar" role="progressbar" aria-valuenow="{{$dt->progress_percentage}}" aria-valuemin="0" aria-valuemax="100" style="width:{{$dt->progress_percentage}}%">
                            {{$dt->progress_percentage}}%
                        </div>
                    </div>
                    <div class="desc">
                        <h4 class="left-side">Start:</h4>
                        <h4 class="right-side">
                            {{$dt->created_at_format}}
                        </h4>
                    </div>
                    <div class="desc">
                        <h4 class="left-side">End:</h4>
                        <h4 class="right-side">
                            {{$dt->end_at_format}}
                        </h4>
                    </div>
                    <div class="desc">
                        <h4 class="left-side">Last harvest:</h4>
                        <h4 class="right-side">
                            {{$dt->harvest_at_format}}
                            ({{$dt->selisih_harvest}} days ago)
                        </h4>
                    </div>
                    <p class="title-result">{{$dt->coin_reward}} EARNED</p>
                    <div class="result">
                        @if($dt->can_harvest<1000) <h4>{{number_format($dt->can_harvest,7,".",",")}}</h4>
                            @else
                            <h4>{{number_format($dt->can_harvest,2,".",",")}}</h4>
                            @endif
                            @if($dt->can_harvest > 0)
                            <button class="btn btn-primary" onclick="harvest({{json_encode($dt)}})">Harvest</button>
                            @else
                            <button class="btn btn-primary" disabled>Harvest</button>
                            @endif
                    </div>
                    <p class="title-result">{{$dt->coin_reward}} STAKED</p>
                    <div class="result">
                        @if($dt->amount_stake<1000) <h4>{{number_format($dt->amount_stake,6,".",",")}}</h4>
                            @else
                            <h4>{{number_format($dt->amount_stake,2,".",",")}}</h4>
                            @endif
                            <button class="btn btn-primary" onclick="harvestall({{json_encode($dt)}})">Harvest All</button>
                    </div>
                    <div class="container-btn">
                        <a href="my-farms/{{$dt->id}}">
                            <button class="btn btn-primary">Details</button>
                        </a>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
    <!-- <div class="tab-pane fade" id="vinix" role="tabpanel" aria-labelledby="vinix-tab">
                @foreach($dataMyFarms as $dt)
                @if($dt->coin_reward == "VNX")
                <div class="col-xl-4 col-lg-4 col-md-5 col-sm-12 item">
                    <div class="desc">
                        <div class="left-side">
                            <img src="{{URL::asset('image/logo-usd.png')}}" width="50px" height="50px">
                        </div>
                        <h3 class="right-side title">
                            {{$dt->product->product_name}} <br>
                            <div class="sub-title">
                                <div class="core"><i class="fa fa-check-circle"></i> Core</div>
                                <div class="double">20X</div>
                            </div>
                        </h3>
                    </div>
                    <div class="desc">
                        <h4 class="left-side">APR:</h4>
                        <h4 class="right-side">{{$dt->product->APR}}%</h4>
                    </div>
                    <div class="desc">
                        <h4 class="left-side">Earn:</h4>
                        <h4 class="right-side">{{$dt->coin_reward}}</h4>
                    </div>

                    <p class="title-result">PROGRESS</p>
                    <div class="progress">
                        <div class="progress-bar" role="progressbar" aria-valuenow="{{$dt->progress_percentage}}" aria-valuemin="0" aria-valuemax="100" style="width:{{$dt->progress_percentage}}%">
                            {{$dt->progress_percentage}}%
                        </div>
                    </div>
                    <div class="desc">
                        <h4 class="left-side">Start:</h4>
                        <h4 class="right-side">
                            {{$dt->created_at_format}}
                        </h4>
                    </div>
                    <div class="desc">
                        <h4 class="left-side">End:</h4>
                        <h4 class="right-side">
                            {{$dt->end_at_format}}
                        </h4>
                    </div>
                    <div class="desc">
                        <h4 class="left-side">Last harvest:</h4>
                        <h4 class="right-side">
                            {{$dt->harvest_at_format}}
                            ({{$dt->selisih_harvest}} days ago)
                        </h4>
                    </div>
                    <p class="title-result">{{$dt->coin_reward}} EARNED</p>
                    <div class="result">
                        @if($dt->can_harvest<1000) <h4>{{number_format($dt->can_harvest,8,",",".")}}</h4>
                            @else
                            <h4>{{number_format($dt->can_harvest,2,",",".")}}</h4>
                            @endif
                            @if($dt->can_harvest > 0)
                            <button class="btn btn-primary" onclick="harvest({{json_encode($dt)}})">Harvest</button>
                            @else
                            <button class="btn btn-primary" disabled>Harvest</button>
                            @endif
                    </div>
                    <p class="title-result">{{$dt->coin_reward}} STAKED</p>
                    <div class="result">
                        @if($dt->amount_stake<1000) <h4>{{number_format($dt->amount_stake,8,",",".")}}</h4>
                            @else
                            <h4>{{number_format($dt->amount_stake,2,",",".")}}</h4>
                            @endif
                            <button class="btn btn-primary" onclick="harvestall({{json_encode($dt)}})">Harvest All</button>
                    </div>
                    <div class="container-btn">
                        <button class="btn btn-primary">Details</button>
                    </div>
                </div>
                @endif
                @endforeach
            </div> -->
    <!-- </div>
    </div> -->
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->

<!-- Modal -->
<div id="harvestModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Harvest</h4>
            </div>
            <div class="modal-body">
                <div class="desc">
                    <div class="left-side">
                        <img src="{{URL::asset('image/logo-usd.png')}}" id="modal-img" width="50px" height="50px">
                    </div>
                    <h3 class="right-side title">
                        <div id="modal-name"></div>
                        <div class="sub-title">
                            <div class="core"><i class="fa fa-check-circle"></i> Core</div>
                            <div class="double">20X</div>
                        </div>
                    </h3>
                </div>
                <div class="desc">
                    <h4 class="left-side">P.A:</h4>
                    <h4 class="right-side" id="modal-apr"></h4>
                </div>
                <div class="desc">
                    <h4 class="left-side">Earn:</h4>
                    <h4 class="right-side" id="modal-earn"></h4>
                </div>
                <div class="desc">
                    <h4 class="left-side">Start:</h4>
                    <h4 class="right-side" id="modal-start">
                    </h4>
                </div>
                <div class="desc">
                    <h4 class="left-side">End:</h4>
                    <h4 class="right-side" id="modal-end">
                    </h4>
                </div>
                <div class="desc">
                    <h4 class="left-side">Last harvest:</h4>
                    <h4 class="right-side" id="modal-last">
                    </h4>
                </div>
                <p class="title-result">COIN STAKED</p>
                <div class="result">
                    <h4 id="modal-staked"></h4>
                </div>
                <p class="title-result">COIN EARNED</p>
                <div class="result">
                    <h4 id="modal-earned"></h4>
                </div>
                <p class="title-result">HARVEST FEE</p>
                <div class="result">
                    <h4 id="modal-fee"></h4>
                </div>
                <p class="title-result">TOTAL HARVESTED COIN</p>
                <div class="result">
                    <h4 id="modal-total"></h4>
                </div>
                <p class="warning">you can't harvest because you don't have insufficient earning to pay fee</p>
                <form action="{{URL('/farms/do-harvest')}}" method="POST">
                    @csrf
                    <input type="hidden" id="id_trx_staking" name="id_trx_staking" />
                    <div class="container-btn">
                        <button class="btn btn-primary" id="btnHarvest">HARVEST</button>
                    </div>
                </form>

            </div>
        </div>

    </div>
</div>

<div id="harvestAllModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Harvest</h4>
            </div>
            <div class="modal-body">
                <div class="desc">
                    <div class="left-side">
                        <img id="modal-img2" src="{{URL::asset('image/logo-usd.png')}}" width="50px" height="50px">
                    </div>
                    <h3 class="right-side title">
                        <div id="modalall-name"></div>
                        <div class="sub-title">
                            <div class="core"><i class="fa fa-check-circle"></i> Core</div>
                            <div class="double">20X</div>
                        </div>
                    </h3>
                </div>
                <div class="desc">
                    <h4 class="left-side">APY:</h4>
                    <h4 class="right-side" id="modalall-apr"></h4>
                </div>
                <div class="desc">
                    <h4 class="left-side">Earn:</h4>
                    <h4 class="right-side" id="modalall-earn"></h4>
                </div>
                <div class="desc">
                    <h4 class="left-side">Start:</h4>
                    <h4 class="right-side" id="modalall-start">
                    </h4>
                </div>
                <div class="desc">
                    <h4 class="left-side">End:</h4>
                    <h4 class="right-side" id="modalall-end">
                    </h4>
                </div>
                <div class="desc">
                    <h4 class="left-side">Last harvest:</h4>
                    <h4 class="right-side" id="modalall-last">
                    </h4>
                </div>
                <p class="title-result">STAKED COIN</p>
                <div class="result">
                    <h4 id="modalall-staked"></h4>
                </div>
                <p class="title-result">TOTAL HARVESTED COIN</p>
                <div class="result">
                    <h4 id="modalall-total"></h4>
                </div>
                <p class="title-result">COMMITMENT FEE</p>
                <div class="result">
                    <h4 id="modalall-commitment"></h4>
                </div>
                <p class="title-result">TOTAL WITHDRAW COIN</p>
                <div class="result">
                    <h4 id="modalall-withdraw"></h4>
                </div>
                <h5 class="warning"> Your staking not done yet, you will charged commitment fee.</h5>
                <form action="{{URL('/farms/do-harvestall')}}" method="POST">
                    @csrf
                    <input type="hidden" id="id_trx_staking2" name="id_trx_staking" />
                    <div class="container-btn">
                        <button class="btn btn-primary" id="btnHarvest">HARVEST ALL</button>
                    </div>
                </form>
            </div>
        </div>

    </div>
</div>
<script src="{{URL::asset('js/my-farm.js')}}"></script>
@include('User.footer')