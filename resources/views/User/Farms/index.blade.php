@include('User.header',['activePage' => 'farms'])

<link rel="stylesheet" href="{{URL::asset('css/farms.css')}}">
<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div id="content1">
            <h1>STAKE FARMS</h1>
            <h4>Stake your coin to make more of them.</h4>
        </div>
        <!-- <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item active">
                <a class="nav-link" id="usd-tab" data-toggle="tab" href="#usd" role="tab" aria-controls="usd" aria-selected="true">USDV</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="vinix-tab" data-toggle="tab" href="#vinix" role="tab" aria-controls="vinix" aria-selected="false">VINIX</a>
            </li>
        </ul> -->
        <!-- <div class="tab-content" id="myTabContent">
            <div class="tab-pane active" id="usd" role="tabpanel" aria-labelledby="usd-tab"> -->
        <div class="row">
            @foreach($dataProduct as $dt)
            <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-xs-12">
                <div class="item">
                    <div class="desc">
                        <div class="left-side">
                            <img src="{{$dt->url_coin}}" width="50px" height="50px">
                        </div>
                        <h3 class="right-side title">
                            {{$dt->product_name}} <br>
                            <div class="sub-title">
                                <div class="core"><i class="fa fa-check-circle"></i> Core</div>
                                @if($dt->coin_reward == "VNX")
                                <div class="double">20X</div>
                                @else
                                <div class="double">5X</div>
                                @endif
                            </div>
                        </h3>
                    </div>
                    <div class="desc">
                        <h4 class="left-side">P.A:</h4>
                        <h4 class="right-side">{{number_format($dt->APR)}}% <i class="fa fa-info-circle info" alt="info" onclick="showInfo({{$dt->APR}})"></i></h4>
                    </div>
                    <div class="desc">
                        <h4 class="left-side">Tenor:</h4>
                        <h4 class="right-side">{{$dt->tenor}} Month</h4>
                    </div>
                    <div class="desc">
                        <h4 class="left-side">Commitment Fee:</h4>
                        <h4 class="right-side">{{$dt->comitment_fee}}%</h4>
                    </div>
                    <div class="desc">
                        <h4 class="left-side">Stake from:</h4>
                        <h4 class="right-side">{{$dt->coin_stake}}</h4>
                    </div>
                    <div class="desc">
                        <h4 class="left-side">Earn:</h4>
                        <h4 class="right-side">{{$dt->coin_reward}}</h4>
                    </div>
                    <p class="title-result">{{$dt->coin_reward}} EARNED PER DAY</p>
                    <div class="result">
                        <h4>{{number_format($dt->profit/30,8,".",",")}}%</h4>
                        {{-- <button class="btn btn-primary">Harvest</button> --}}
                    </div>
                    <h5 class="sub-title-result">FROM YOUR STAKE</h5>
                    <div class="container-btn">
                        <button class="btn btn-primary" onclick="startStaking({{json_encode($dt)}})">START STAKING</button>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
    <!-- <div class="tab-pane fade" id="vinix" role="tabpanel" aria-labelledby="vinix-tab">
                @foreach($dataProduct as $dt)
                    @if($dt->coin_reward == "VNX")
                    <div class="col-xl-3 col-lg-3 col-md-5 col-sm-12 item">
                        <div class="desc">
                            <div class="left-side">
                                <img src="{{URL::asset('image/logo-usd.png')}}" width="50px" height="50px">
                            </div>
                            <h3 class="right-side title">
                                {{$dt->product_name}} <br>
                                <div class="sub-title">
                                    <div class="core"><i class="fa fa-check-circle"></i> Core</div>
                                    <div class="double">20X</div>
                                </div>
                            </h3>
                        </div>
                        <div class="desc">
                            <h4 class="left-side">APR:</h4>
                            <h4 class="right-side">{{$dt->APR}}%</h4>
                        </div>
                        <div class="desc">
                            <h4 class="left-side">Tenor:</h4>
                            <h4 class="right-side">{{$dt->tenor}} Month</h4>
                        </div>
                        <div class="desc">
                            <h4 class="left-side">Com. Fee:</h4>
                            <h4 class="right-side">{{$dt->comitment_fee}}%</h4>
                        </div>
                        <div class="desc">
                            <h4 class="left-side">Stake from:</h4>
                            <h4 class="right-side">{{$dt->coin_stake}}</h4>
                        </div>
                        <div class="desc">
                            <h4 class="left-side">Earn:</h4>
                            <h4 class="right-side">{{$dt->coin_reward}}</h4>
                        </div>
                        <p class="title-result">{{$dt->coin_reward}} EARNED PER DAY</p>
                        <div class="result">
                            <h4>{{$dt->profit/30}}%</h4>
                            {{-- <button class="btn btn-primary">Harvest</button> --}}
                        </div>
                        <h5 class="sub-title-result">FROM YOUR STAKE</h5>
                        <div class="container-btn">
                            <button class="btn btn-primary" onclick="startStaking({{json_encode($dt)}})">START STAKING</button>
                        </div>
                    </div>
                    @endif
                    @endforeach
            </div> -->
    <!-- </div> -->
</div>
<!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->
<!-- Modal -->
<div id="startStakingModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Start Staking</h4>
            </div>
            <div class="modal-body">
                <div class="desc">
                    <div class="left-side">
                        <img src="{{URL::asset('image/logo-usd.png')}}" id="modal-img" width="50px" height="50px">
                    </div>
                    <h3 class="right-side title">
                        <div id="modal-name" style="margin-bottom: 0px"></div>
                        <div class="sub-title">
                            <div class="core"><i class="fa fa-check-circle"></i> Core</div>
                            <div id="modal-double" class="double">20X</div>
                        </div>
                    </h3>
                </div>
                <div class="desc">
                    <h4 class="left-side">P.A:</h4>
                    <h4 class="right-side" id="modal-apr">25%</h4>
                </div>
                <div class="desc">
                    <h4 class="left-side">Earn:</h4>
                    <h4 class="right-side" id="modal-earn">USDV</h4>
                </div>
                <div class="desc">
                    <h4 class="left-side">Tenor:</h4>
                    <h4 class="right-side" id="modal-tenor">6 Month</h4>
                </div>
                <div class="desc">
                    <h4 class="left-side">Stake from:</h4>
                    <h4 class="right-side" id="modal-from">USDV</h4>
                </div>
                <div class="desc">
                    <h4 class="left-side">Commitment Fee:</h4>
                    <h4 class="right-side" id="modal-fee">20%</h4>
                </div>

                <form id="formStaking" action="{{URL('/farms/do-farm')}}" method="POST">
                    @csrf
                    <p class="title-result">AMOUNT STAKE</p>
                    <h5 id="modal-balance">Your balance : 25 USDV</h5>
                    <input type="number" step="any" id="amount" class="amount-staking" name="amount" onkeyup="amountChange()" placeholder="0.0" />
                    <p class="title-result">COIN EARNED PER DAY</p>
                    <div class="result">
                        <h4 id="modal-coin-earned">0.0</h4>
                    </div>
                    <p class="title-result">SHARING PROFIT</p>
                    <div class="result">
                        <h4 id="modal-sharing-profit">0.0 %</h4>
                    </div>
                    <p class="title-result">TOTAL COIN EARNED PER DAY</p>
                    <div class="result">
                        <h4 id="modal-result">0.0</h4>
                    </div>
                    <input type="hidden" id="input_id_product" name="id_product" value="">

                    <div class="container-btn">
                        <button class="btn btn-primary" style="width:80%;">
                            STAKE
                        </button>
                    </div>
                </form>

            </div>
        </div>

    </div>
</div>

<div id="infoModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">ROI</h4>
            </div>
            <div class="modal-body">
                <table>
                    <thead>
                        <th>TIMEFRAME</th>
                        <th>ROI</th>
                        <th>EARNING PER $1000</th>
                    </thead>
                    <tbody style="text-align:center;">
                        <tr>
                            <td>1d</td>
                            <td id="apr_1d">0.34%</td>
                            <td id="get_1d">0.31</td>
                        </tr>
                        <tr>
                            <td>7d</td>
                            <td id="apr_7d">0.34%</td>
                            <td id="get_7d">0.31</td>
                        </tr>
                        <tr>
                            <td>30d</td>
                            <td id="apr_30d">0.34%</td>
                            <td id="get_30d">0.31</td>
                        </tr>
                        <tr>
                            <td>365d</td>
                            <td id="apr_365d">0.34%</td>
                            <td id="get_365d">0.31</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

    </div>
</div>
<script>
    var dataUser = <?php echo json_encode($dataUser); ?>;
</script>
<script src="{{URL::asset('js/stake.js')}}"></script>

@include('User.footer')