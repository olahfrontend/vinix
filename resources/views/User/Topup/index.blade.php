@include('User.header',['activePage' => 'topup'])

<link rel="stylesheet" href="{{URL::asset('css/topup.css')}}">
<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div id="content1">
            <h1>DEPOSIT WALLET</h1>
            <h4>Deposit your crypto currency from another crypto services.</h4>
        </div>

        <div id="content2">
            <h3>Available Cryptocurrency services</h3>
            <div class="row">
                @foreach($dataAddress as $dt)
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="item">
                        <img src="{{$dt->url_logo}}" width="100px" height="auto" />
                        <h3>{{$dt->coin_name}}</h3>
                        <h5>Balance</h5>
                        <h4>{{$dt->saldo}}</h4>
                        <button class="btn btn-primary" onclick="showAddress({{json_encode($dt)}})">See Address</button>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->

<!-- Modal -->
<div id="addressModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Generate Address</h4>
            </div>
            <div class="modal-body">
                <div class="modal-content">
                    <img id="modal-icon" src="{{$dt->url_logo}}" width="100px" height="auto" />
                    <h3 id="modal-name"></h3>
                    <h4 style="font-weight:bold;color:#e05227;">Balance : </h4>
                    <h4 id="modal-saldo"></h4>
                    <div class="container-address">
                        <h4 style="font-weight:bold;color:#e05227;">Address :
                            <i id="icon-copy" class="fas fa-copy" aria-hidden="true" onclick="copyToClipBoard()"></i>
                        </h4>
                        <div id="tooltip">Address copied</div>
                        <input type="text" id="inp_copy" />
                    </div>
                    <h4 id="modal-address"></h4>
                    <div id="container-qr">
                        <h4 style="font-weight:bold;color:#e05227;">QR Code : </h4>
                        <img src="" id="img-qr" style="margin-top:0px;">
                    </div>
                    <form id="formGenerate" action="{{URL('topup/generateAddress')}}" method="POST">
                        @csrf
                        <input type="hidden" value="" id="type" name="type" />
                        <button class="btn btn-primary" type="submit">Generate Address</button>
                    </form>
                </div>
            </div>
        </div>

    </div>
</div>
<script src="{{URL::asset('js/deposit.js')}}"></script>
@include('User.footer')