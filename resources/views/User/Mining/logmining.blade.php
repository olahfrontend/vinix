@if($dataUser->user->role == 1)
@include('User.header',['activePage' => 'my-mining'])
@else
@include('Admin.header',['activePage' => 'master_user'])
@endif
<link rel="stylesheet" href="{{URL::asset('css/detailfarm.css')}}">
<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div id="content">
            <h1>{{$dataMining->product->product_name}}</h1>
            <h5 style="margin-top:0px;">{{$dataMining->product->product_type}}</h5>
            <h5>{{$dataMining->created_at}}</h5>
            <div class="container-desc">
                <div class="desc">
                    <h5>Start Mining</h5>
                    <p>{{$dataMining->created_at}}</p>
                </div>
                <div class="desc">
                    <h5>End Mining</h5>
                    <p>{{$dataMining->end_at}}</p>
                </div>
                <div class="desc">
                    <h5>Last Harvest</h5>
                    <p>{{$dataMining->last_harvest}}</p>
                </div>
            </div>
            <div class="container-desc">
                <div class="desc">
                    <h5>Cust Name</h5>
                    <p>{{$dataMining->cust_name}}</p>
                </div>
                <div class="desc">
                    <h5>Mining Speed</h5>
                    <p>{{$dataMining->mining_speed}} MHS</p>
                </div>
                <div class="desc">
                    <h5>Price</h5>
                    <p>{{$dataMining->product->price}}</p>
                </div>
            </div>
            <div class="btn btn-primary" id="exportexcel">Export Excel</div>
            <h5 style="font-weight:bold;margin-top:25px;">LOG REWARD</h5>
            <div style="padding:15px;">
                <table id="myTable" class="table table-bordered display">
                    <thead>
                        <th style="text-align:center;">Date Reward</th>
                        <th style="text-align:center;">Reward</th>
                        <th style="text-align:center;">Total Reward</th>
                    </thead>
                    <tbody>
                        @foreach($dataLog as $dt)
                        <tr>
                            <td>{{$dt->created_at}}</td>
                            <td>{{$dt->earning}}</td>
                            <td>{{$dt->total}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->
<table id="exportTable" class="hidden">
    <tr class="hidden">
        <td>Name</td>
        <td>{{$dataMining->cust_name}}</td>
    </tr>
    <tr class="hidden">
        <td>Speed</td>
        <td>{{$dataMining->mining_speed}} MHS</td>
    </tr>
    <tr class="hidden">
        <td>Currency</td>
        <td>{{$dataMining->history[0]->code}}</td>
    </tr>
    <tr class="hidden">
        <td>ID Transaction Mining</td>
        <td>{{$dataMining->id}}</td>
    </tr>
    <tr class="hidden"></tr>
    <thead>
        <th style="text-align:center;">Date Reward</th>
        <th style="text-align:center;">Reward</th>
        <th style="text-align:center;">Total Reward</th>
    </thead>
    <tbody>
        @foreach($dataLog as $dt)
        <tr>
            <td>{{$dt->created_at}}</td>
            <td>{{$dt->earning}}</td>
            <td>{{$dt->total}}</td>
        </tr>
        @endforeach
    </tbody>
</table>
<script src="{{URL::asset('js/my-farm.js')}}"></script>
@include('User.footer')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.css">
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="{{URL::asset('js/jquery.table2excel.js')}}"></script>
<script>
    $(document).ready(function() {
        $('#myTable').DataTable({
            "order": [
                [0, "desc"]
            ]
        });
    });

    $("#exportexcel").click(function() {
        var today = new Date();
        var fileName = "mining_log_" + <?php echo $dataMining->id; ?> + " (" + today.toUTCString() + ")";
        $("#exportTable").table2excel({
            // exclude CSS class
            exclude: ".noExl",
            name: "Worksheet Name",
            filename: fileName, //do not include extension
            fileext: ".xls" // file extension
        });
    });
</script>