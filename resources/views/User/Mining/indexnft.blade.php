@include('User.header',['activePage' => 'mining-nft'])

<link rel="stylesheet" href="{{ URL::asset('css/mining.css') }}">
<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div id="content1">
            <h1>MINING NFT</h1>
            <h4>Start mining NFT right now.</h4>
        </div>

        <div id="content3">
            <div class="item">
                <h4 class="offer">Coming back <br>(Limited Offer)</h4>
                <div class="desc-custom">
                    <div class="title">
                        <h2>Custom</h2>
                        <h5>Custom Mining</h5>
                    </div>
                    <div class="label">
                        <h5>100 $</h5>
                        <h5>100,000 $</h5>
                    </div>
                    <div class="slidecontainer">
                        <input type="range" min="100" max="100000" step="100" value="0" class="slider" id="sliderSpeed" onchange="sliderChange()">
                    </div>
                    <div class="price">
                        <h2>$<input type="number" class="numberbox" min="100" max="100000" step="100" id="custom-price" value="100"></h2>
                        <!-- <h2 id="custom-price">$100</h2> -->
                    </div>
                    <div class="speed">
                        <h3 id="speed">80 AETHER / Hour</h3>
                    </div>
                    <div class="desc">
                        24 Months NFT Mining * No Maintenance Fee
                    </div>
                    <div style="text-align:center">
                        <button class="btn btn-primary" style="width:50%;" onclick="openModalCustom('AETHER')">Buy</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->

<div id="modalBuyCustom" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Start Mining</h4>
            </div>
            <div class="modal-body">
                <div class="modal-content">
                    <div class="title">
                        <h2 id="modal-name-custom">CUSTOM</h2>
                        <h5 id="modal-type-custom"></h5>
                    </div>
                    <div class="desc">
                        <h4 class="left-side">Total Mined :</h4>
                        <h4 class="right-side" id="modal-speed-custom">80</h4>
                    </div>
                    <div class="desc">
                        <h4 class="left-side">Duration Mining:</h4>
                        <h4 class="right-side" id="modal-tenor-custom">24 Months</h4>
                    </div>
                    <div class="desc">
                        <h4 class="left-side">Estimated earn / day:</h4>
                        <h4 class="right-side" id="modal-earn-custom">$100</h4>
                    </div>
                    <div class="desc">
                        <h4 class="left-side">Price:</h4>
                        <div class="right-side" style="position:relative;display:none;" id="modal-price-loading-custom">
                            <div class="loader" style="position:absolute;right:0px;width:20px; height:20px;border: 2px solid #f3f3f3;border-top: 2px solid #E05227;">
                            </div>
                        </div>
                        <h4 class="right-side" id="modal-price-custom">100 USDV</h4>
                    </div>
                    <p class="title-result">PAY WITH</p>
                    <div class="result">
                        <h5 id="balance-custom">BALANCE : 0.25 ETH</h5>
                        <select class="form-control" onchange="changeWalletCustom()" id="cbWalletCustom">
                            @foreach ($dataUser->wallet as $wallet)
                            <option value="{{ $wallet->id }}">{{ $wallet->code }}</option>
                            @endforeach
                        </select>
                    </div>
                    <form action="{{ URL('/mining/do-mining-custom') }}" method="POST">
                        @csrf
                        <input type="hidden" id="mining_speed_custom" name="mining_speed" />
                        <input type="hidden" id="type_coin_reward" name="type_coin_reward" />
                        <input type="hidden" id="tenor" name="tenor" />
                        <input type="hidden" id="id_coin_currency_custom" name="id_coin_currency" />
                        <button type="submit" class="btn btn-primary">Buy</button>
                    </form>
                </div>
            </div>
        </div>

    </div>
</div>
<script>
    var walletUser = <?php echo json_encode($dataUser->wallet); ?>;
    var settingGeneral = <?php echo json_encode($dataUser->setting_general); ?>;
</script>

<script src="{{ URL::asset('js/miningnft.js') }}"></script>
@include('User.footer')