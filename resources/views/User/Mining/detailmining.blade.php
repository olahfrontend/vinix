@include('User.header',['activePage' => 'my-mining'])

<link rel="stylesheet" href="{{URL::asset('css/detailfarm.css')}}">
<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div id="content">
            <h1>{{$dataMining->product->product_name}}</h1>
            <h5 style="margin-top:0px;">{{$dataMining->product->product_type}}</h5>
            <h5>{{$dataMining->created_at}}</h5>

            <div class="container-desc">
                <div class="desc">
                    <h5>Start Mining</h5>
                    <p>{{$dataMining->created_at}}</p>
                </div>
                <div class="desc">
                    <h5>End Mining</h5>
                    <p>{{$dataMining->end_at}}</p>
                </div>
                <div class="desc">
                    <h5>Last Harvest</h5>
                    <p>{{$dataMining->last_harvest}}</p>
                </div>
            </div>
            <div class="container-desc">
                <div class="desc">
                    <h5>Duration</h5>
                    <p>{{$dataMining->product->tenor}} Month</p>
                </div>
                <div class="desc">
                    <h5>Mining Speed</h5>
                    <p>{{$dataMining->mining_speed}} MHS</p>
                </div>
                <div class="desc">
                    <h5>Price</h5>
                    <p>{{$dataMining->product->price}}</p>
                </div>
            </div>
            <div class="container-desc">
                <div class="desc" style="margin:5px auto;">
                    <a class="btn btn-primary" href="{{URL('/my-mining/log/').'/'.$dataMining->id}}">Log Reward</a>
                </div>
            </div>

            <h5 style="font-weight:bold;">HISTORY TRANSACTION</h5>
            <div style="overflow-x:scroll">
                <table style="overflow-x:auto;">
                    <thead>
                        <th>Date Transaction</th>
                        <th>Description</th>
                        <th>Currency</th>
                        <th>Amount</th>
                        <th>Fee</th>
                        <th>Netto</th>
                    </thead>
                    <tbody>
                        @foreach($dataMining->history as $dt)
                        <tr>
                            <td>{{$dt->created_at}}</td>
                            <td>{{$dt->trx_type}}</td>
                            <td>{{$dt->code}}</td>
                            <td>{{number_format($dt->total_fee + $dt->amount,8,'.',',')}}</td>
                            <td>{{$dt->total_fee}}</td>
                            <td>{{$dt->amount}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->
<script src="{{URL::asset('js/my-farm.js')}}"></script>
@include('User.footer')