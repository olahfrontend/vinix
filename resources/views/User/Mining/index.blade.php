@include('User.header',['activePage' => 'mining-eth'])

<link rel="stylesheet" href="{{ URL::asset('css/mining.css') }}">
<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div id="content1">
            <h1>MINING ETHEREUM</h1>
            <h4>Start mining ethereum right now.</h4>
        </div>

        <div id="content2">
            <div class="row">
                @foreach ($dataProduct as $dp)
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="item">
                        <h4 class="offer">Coming back <br>(Limited Offer)</h4>
                        <div class="title">
                            <h2>{{ $dp->product_name }}</h2>
                            <h5>{{ $dp->product_type }}</h5>
                        </div>
                        <div class="price">
                            <h2>${{ number_format($dp->price, 2, '.', ',') }}</h2>
                        </div>
                        <div class="speed">
                            <h3>{{ $dp->mining_speed }} MH/s</h3>
                        </div>
                        <div class="desc">
                            {{ $dp->tenor }} Months Ethereum Mining * Ethash Mining Algorithm No Maintenance Fee
                        </div>
                        <button class="btn btn-primary" onclick="openModal({{ json_encode($dp) }})">BUY</button>
                    </div>
                </div>
                @endforeach
            </div>
        </div>

        <div id="content3">
            <h3 class="title">
                Or Customize your speed
            </h3>
            <div class="item">
                <h4 class="offer">Coming back <br>(Limited Offer)</h4>
                <div class="desc-custom">
                    <div class="title">
                        <h2>Custom</h2>
                        <h5>Custom Mining</h5>
                    </div>
                    <div class="label">
                        <h5>50 MHS</h5>
                        <h5>550 MHS</h5>
                    </div>
                    <div class="slidecontainer">
                        <input type="range" min="50" max="550" step="10" value="0" class="slider" id="sliderSpeed" onchange="sliderChange()">
                    </div>
                    <div class="price">
                        <h2 id="custom-price">${{ number_format($dp->price, 2, ',', '.') }}</h2>
                    </div>
                    <div class="speed">
                        <h3 id="speed">{{ $dp->mining_speed }} MH/s</h3>
                    </div>
                    <div class="desc">
                        {{ $dp->tenor }} Months Ethereum Mining * Ethash Mining Algorithm No Maintenance Fee
                    </div>
                    <div style="text-align:center">
                        <button class="btn btn-primary" style="width:50%;" onclick="openModalCustom('ETH')">Buy</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->

<!-- Modal -->
<div id="modalBuy" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Start Mining</h4>
            </div>
            <div class="modal-body">
                <div class="modal-content">
                    <div class="title">
                        <h2 id="modal-name">{{ $dp->product_name }}</h2>
                        <h5 id="modal-type">{{ $dp->product_type }}</h5>
                    </div>
                    <div class="desc">
                        <h4 class="left-side">Mining Speed:</h4>
                        <h4 class="right-side" id="modal-speed">{{ number_format($dp->mining_speed, 2, '.', ',') }} MHS
                        </h4>
                    </div>
                    <div class="desc">
                        <h4 class="left-side">Duration Mining:</h4>
                        <h4 class="right-side" id="modal-tenor">{{ $dp->tenor }} Months</h4>
                    </div>
                    <div class="desc">
                        <h4 class="left-side">Estimated earn / day:</h4>
                        <h4 class="right-side" id="modal-earn">${{ $dp->earn_per_day }}</h4>
                    </div>
                    <div class="desc">
                        <h4 class="left-side">Price:</h4>
                        <div class="right-side" style="position:relative;display:none;" id="modal-price-loading">
                            <div class="loader" style="position:absolute;right:0px;width:20px; height:20px;border: 2px solid #f3f3f3;border-top: 2px solid #E05227;">
                            </div>
                        </div>
                        <h4 class="right-side" id="modal-price">{{ $dp->price }} USDV</h4>
                    </div>
                    <p class="title-result">PAY WITH</p>
                    <div class="result">
                        <h5 id="balance">BALANCE : 0.25 ETH</h5>
                        <select class="form-control" onchange="changeWallet()" id="cbWallet">
                            @foreach ($dataUser->wallet as $wallet)
                            <option value="{{ $wallet->id }}">{{ $wallet->code }}</option>
                            @endforeach
                        </select>
                    </div>
                    <form action="{{ URL('/mining/do-mining') }}" method="POST">
                        @csrf
                        <input type="hidden" id="id_product_mining" name="id_product_mining" />
                        <input type="hidden" id="id_coin_currency" name="id_coin_currency" />
                        <button type="submit" class="btn btn-primary">Buy</button>
                    </form>
                </div>
            </div>
        </div>

    </div>
</div>

<div id="modalBuyCustom" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Start Mining</h4>
            </div>
            <div class="modal-body">
                <div class="modal-content">
                    <div class="title">
                        <h2 id="modal-name-custom">{{ $dp->product_name }}</h2>
                        <h5 id="modal-type-custom">{{ $dp->product_type }}</h5>
                    </div>
                    <div class="desc">
                        <h4 class="left-side">Mining Speed:</h4>
                        <h4 class="right-side" id="modal-speed-custom">{{ number_format($dp->mining_speed, 2, '.', ',') }}
                            MHS</h4>
                    </div>
                    <div class="desc">
                        <h4 class="left-side">Duration Mining:</h4>
                        <h4 class="right-side" id="modal-tenor-custom">{{ $dp->tenor }} Months</h4>
                    </div>
                    <div class="desc">
                        <h4 class="left-side">Estimated earn / day:</h4>
                        <h4 class="right-side" id="modal-earn-custom">${{ $dp->earn_per_day }}</h4>
                    </div>
                    <div class="desc">
                        <h4 class="left-side">Price:</h4>
                        <div class="right-side" style="position:relative;display:none;" id="modal-price-loading-custom">
                            <div class="loader" style="position:absolute;right:0px;width:20px; height:20px;border: 2px solid #f3f3f3;border-top: 2px solid #E05227;">
                            </div>
                        </div>
                        <h4 class="right-side" id="modal-price-custom">{{ $dp->price }} USDV</h4>
                    </div>
                    <p class="title-result">PAY WITH</p>
                    <div class="result">
                        <h5 id="balance-custom">BALANCE : 0.25 ETH</h5>
                        <select class="form-control" onchange="changeWalletCustom()" id="cbWalletCustom">
                            @foreach ($dataUser->wallet as $wallet)
                            <option value="{{ $wallet->id }}">{{ $wallet->code }}</option>
                            @endforeach
                        </select>
                    </div>
                    <form action="{{ URL('/mining/do-mining-custom') }}" method="POST">
                        @csrf
                        <input type="hidden" id="mining_speed_custom" name="mining_speed" />
                        <input type="hidden" id="type_coin_reward" name="type_coin_reward" />
                        <input type="hidden" id="tenor" name="tenor" />
                        <input type="hidden" id="id_coin_currency_custom" name="id_coin_currency" />
                        <button type="submit" class="btn btn-primary">Buy</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    var walletUser = <?php echo json_encode($dataUser->wallet); ?>;
    var settingGeneral = <?php echo json_encode($dataUser->setting_general); ?>;
</script>

<script src="{{ URL::asset('js/mining.js') }}"></script>
@include('User.footer')