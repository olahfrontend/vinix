@include('User.header',['activePage' => 'my-mining'])

<link rel="stylesheet" href="{{ URL::asset('css/my-mining.css') }}">
<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div id="content1">
            <h1>MY MINER</h1>
            <h4>List miner you have in vinix.</h4>
        </div>

        <div id="content2">
            <div class="row">
                @foreach ($dataFarms as $df)
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="item">
                        <div class="title">
                            <h3>{{$df->product_name}}</h3>
                            <h5>{{$df->product_type}}</h5>
                        </div>
                        <div class="desc">
                            <h4 class="left-side">Mining Speed:</h4>
                            <h4 class="right-side">{{number_format($df->mining_speed,2,".",",")}} MHS</h4>
                        </div>
                        <div class="desc">
                            <h4 class="left-side">Duration:</h4>
                            <h4 class="right-side">{{$df->tenor}} Months</h4>
                        </div>

                        <p class="title-result">PROGRESS</p>
                        <div class="progress">
                            <div class="progress-bar" role="progressbar" aria-valuenow="{{$df->progress_percentage}}" aria-valuemin="0" aria-valuemax="100" style="width:{{$df->progress_percentage}}%">
                                {{$df->progress_percentage}}%
                            </div>
                        </div>
                        <div class="desc">
                            <h4 class="left-side">Start:</h4>
                            <h4 class="right-side">
                                {{$df->created_at_format}}
                            </h4>
                        </div>
                        <div class="desc">
                            <h4 class="left-side">End:</h4>
                            <h4 class="right-side">
                                {{$df->end_at_format}}
                            </h4>
                        </div>
                        <div class="desc">
                            <h4 class="left-side">Last Harvest:</h4>
                            <h4 class="right-side">
                                {{$df->last_harvest_format}} ({{$df->selisih}} days ago)
                            </h4>
                        </div>
                        <p class="title-result">MINING EARNED</p>
                        <div class="result">
                            {{number_format($df->can_harvest,8,".",",")}} {{$df->reward_coin}}
                        </div>
                        <div class="container-btn">
                            <button class="btn btn-primary" onclick="harvestModal({{json_encode($df)}})">HARVEST</button>
                        </div>
                        <div class="container-btn">
                            <a href="my-mining/{{$df->id}}">
                                <button class="btn btn-primary btn-detail">DETAILS</button>
                            </a>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->

<!-- Modal -->
<div id="harvestModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Harvest</h4>
            </div>
            <div class="modal-body">
                <div class="title">
                    <h3 id="modal-name"></h3>
                    <h5 id="modal-type"></h5>
                </div>
                <div class="desc">
                    <h4 class="left-side">Speed:</h4>
                    <h4 class="right-side" id="modal-speed"></h4>
                </div>
                <div class="desc">
                    <h4 class="left-side">Tenor:</h4>
                    <h4 class="right-side" id="modal-tenor"></h4>
                </div>
                <div class="desc">
                    <h4 class="left-side">Start:</h4>
                    <h4 class="right-side" id="modal-start">
                    </h4>
                </div>
                <div class="desc">
                    <h4 class="left-side">End:</h4>
                    <h4 class="right-side" id="modal-end">
                    </h4>
                </div>
                <div class="desc">
                    <h4 class="left-side">Last harvest:</h4>
                    <h4 class="right-side" id="modal-last">
                    </h4>
                </div>
                <p class="title-result">MINING EARNED</p>
                <div class="result">
                    <h4 id="modal-earned"></h4>
                </div>
                <p class="title-result">HARVEST FEE</p>
                <div class="result">
                    <h4 id="modal-fee"></h4>
                </div>
                <p class="title-result">TOTAL HARVESTED COIN</p>
                <div class="result">
                    <h4 id="modal-total"></h4>
                </div>
                <p class="warning">you can't harvest because you don't have insufficient earning to pay fee</p>
                <p class="warning warning-withdraw">Minimum withdraw not reached</p>
                <form action="{{URL('/mining/do-harvest')}}" method="POST">
                    @csrf
                    <input type="hidden" id="id_trx_mining" name="id_trx_mining" />
                    <div class="container-btn">
                        <button class="btn btn-primary" id="btnHarvest">HARVEST</button>
                    </div>
                </form>

            </div>
        </div>

    </div>
</div>

<script src="{{ URL::asset('js/my-mining.js') }}"></script>
@include('User.footer')