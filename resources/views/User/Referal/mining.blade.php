@include('User.header',['activePage' => 'referal'])

<link rel="stylesheet" href="{{URL::asset('css/referal-staking.css')}}">
<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div id="content1">
            <h1>MINING REFERRAL</h1>
        </div>

        <div id="content2">
            <ul class="menu nav" id="myTab" role="tablist">
                @foreach($dataReferal->commision_mining as $index=>$dt)
                @if($index == 0)
                <li class="menu-item active">
                    <a class="nav-link" id="{{$dt->code}}-tab" data-toggle="tab" href="#{{$dt->code}}" role="tab" aria-controls="{{$dt->code}}" aria-selected="true">{{$dt->code}}</a>
                    @else
                <li class="menu-item">
                    <a class="nav-link" id="{{$dt->code}}-tab" data-toggle="tab" href="#{{$dt->code}}" role="tab" aria-controls="{{$dt->code}}" aria-selected="false">{{$dt->code}}</a>
                    @endif
                </li>
                @endforeach
            </ul>
            <div class="tab-content" id="myTabContent">
                @foreach($dataReferal->commision_mining as $index=>$dt)
                @if($index ==0)
                <div class="tab pane fade in active" id="{{$dt->code}}" role="tabpanel" aria-labelledby="{{$dt->code}}-tab">
                    @else
                    <div class="tab pane fade" id="{{$dt->code}}" role="tabpanel" aria-labelledby="{{$dt->code}}-tab">
                        @endif
                        <div class="title">
                            <img src="{{$dt->url_icon}}">
                            <h5>Total Speed</h5>
                            <h4>{{$dt->total_speed}} MHS</h4>
                            <h5>Earned Referral</h5>
                            <h4>{{$dt->total_referal}} {{$dt->code}}</h4>
                            <form action="{{URL('/referal/do-harvest-mining')}}" method="POST">
                                @csrf
                                <input type="hidden" name="type" value="{{$dt->code}}">
                                <button type="submit" class="btn btn-primary" {{$dt->total_referal <=0 ? 'disabled' : ''}}>Harvest</button>
                            </form>
                        </div>
                        @if(isset($dt->data_referal))
                        <div class="detail">
                            <table id="example" class="table table-striped table-bordered" style="width:100%">
                                <thead>
                                    <th>Transaction Date</th>
                                    <th>Product Name</th>
                                    <th>Referral from</th>
                                    <th>End Mining</th>
                                    <th>Last Harvest</th>
                                    <th>Mining Speed</th>
                                    <th>Commision Speed</th>
                                </thead>
                                <tbody>
                                    @foreach($dt->data_referal as $d)
                                    <tr>
                                        <td>{{$d->detail_mining->created_at}}</td>
                                        <td>{{$d->detail_mining->product_name}}</td>
                                        <td>{{$d->detail_mining->name}}</td>
                                        <td>{{$d->detail_mining->end_at}}</td>
                                        <td>{{$d->last_harvest}}</td>
                                        <td>{{$d->detail_mining->mining_speed}} MHS</td>
                                        <td>{{$d->commision_speed}} MHS</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        @else
                        <h4 style="text-align:center; font-weight:bold;margin-top:25px;">You don't have active mining referral</h4>
                        @endif
                    </div>
                    @endforeach
                </div>
            </div>
            <!-- END MAIN CONTENT -->
        </div>
        <!-- END MAIN -->

        @include('User.footer')

        <script src="{{URL('/js/wallet.js')}}"></script>