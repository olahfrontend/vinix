@include('User.header',['activePage' => 'referal'])

<link rel="stylesheet" href="{{URL::asset('css/referal-staking.css')}}">
<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div id="content1">
            <h1>STAKING REFERRAL</h1>
        </div>

        <div id="content2">
            <ul class="menu nav" id="myTab" role="tablist">
                @foreach($dataReferal->commision_staking as $index=>$dt)
                @if($index == 0)
                <li class="menu-item active">
                    <a class="nav-link" id="{{$dt->code}}-tab" data-toggle="tab" href="#{{$dt->code}}" role="tab" aria-controls="{{$dt->code}}" aria-selected="true">{{$dt->code}}</a>
                    @else
                <li class="menu-item">
                    <a class="nav-link" id="{{$dt->code}}-tab" data-toggle="tab" href="#{{$dt->code}}" role="tab" aria-controls="{{$dt->code}}" aria-selected="false">{{$dt->code}}</a>
                    @endif
                </li>
                @endforeach
            </ul>
            <div class="tab-content" id="myTabContent">
                @foreach($dataReferal->commision_staking as $index=>$dt)
                @if($index ==0)
                <div class="tab pane fade in active" id="{{$dt->code}}" role="tabpanel" aria-labelledby="{{$dt->code}}-tab">
                    @else
                    <div class="tab pane fade" id="{{$dt->code}}" role="tabpanel" aria-labelledby="{{$dt->code}}-tab">
                        @endif
                        <div class="title">
                            <img src="{{$dt->url_icon}}">
                            <h5>Earned Referral</h5>
                            <h4>{{$dt->total_referal}} {{$dt->code}}</h4>
                            <form action="{{URL('/referal/do-harvest-staking')}}" method="POST">
                                @csrf
                                <input type="hidden" name="type" value="{{$dt->code}}">
                                <button type="submit" class="btn btn-primary" {{$dt->total_referal <=0 ? 'disabled' : ''}}>Harvest</button>
                            </form>
                        </div>
                        @if(isset($dt->data_referal))
                        <div class="detail">
                            <table id="example" class="table table-striped table-bordered" style="width:100%">
                                <thead>
                                    <th>Transaction Date</th>
                                    <th>Product Name</th>
                                    <th>Referral from</th>
                                    <th>End Staking</th>
                                    <th>Last Harvest</th>
                                    <th>Amount Commision</th>
                                </thead>
                                <tbody>
                                    @foreach($dt->data_referal as $d)
                                    <tr>
                                        <td>{{$d->detail_staking->created_at}}</td>
                                        <td>{{$d->detail_staking->product_name}}</td>
                                        <td>{{$d->detail_staking->name}}</td>
                                        <td>{{$d->detail_staking->end_at}}</td>
                                        <td>{{$d->last_harvest}}</td>
                                        <td>{{$d->amount_referal}}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        @else
                        <h4 style="text-align:center; font-weight:bold;margin-top:25px;">You don't have active staking referral</h4>
                        @endif
                    </div>
                    @endforeach
                </div>
            </div>
            <!-- END MAIN CONTENT -->
        </div>
        <!-- END MAIN -->

        @include('User.footer')

        <script src="{{URL('/js/wallet.js')}}"></script>