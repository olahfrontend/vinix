@include('User.header',['activePage' => 'referal'])

<link rel="stylesheet" href="{{URL::asset('css/referal.css')}}">
<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div id="content1">
            <h1>MY Referral</h1>
            <h4>view and harvest your referral membership</h4>
        </div>

        <div id="content2">
            <h4>REFERRAL STAKING</h4>
            <div class="row">
                @foreach($dataReferal->commision_staking as $dt)
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xm-12">
                    <div class="item">
                        <img src="{{$dt->url_icon}}">
                        <div class="title">
                            {{$dt->coin_name}}
                        </div>
                        <h5>TOTAL EARNED REFERRAL</h5>
                        <div class="result">
                            {{number_format($dt->total_referal,8,'.',',')}} {{$dt->code}}
                        </div>
                        <div class="container-btn" style="margin-top:15px;">
                            <a href="/referal/staking">
                                <div class="btn btn-primary">Details</div>
                            </a>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>

            <h4>REFERRAL MINING</h4>
            <div class="row">
                @foreach($dataReferal->commision_mining as $dt)
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xm-12">
                    <div class="item">
                        <img src="{{$dt->url_icon}}">
                        <div class="title">
                            {{$dt->coin_name}}
                        </div>
                        <h5>TOTAL EARNED REFERRAL</h5>
                        <div class="result">
                            {{number_format($dt->total_referal,8,'.',',')}} {{$dt->code}}
                        </div>
                        <div class="container-btn" style="margin-top:15px;">
                            <a href="/referal/mining">
                                <div class="btn btn-primary">Details</div>
                            </a>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>

        <div id="content3">
            <h4>MY REFERRAL CODE</h4>
            <div class="code">
                <h3>{{$dataUser->user->referal_code}}</h3>
            </div>
            <table>
                <thead>
                    <th>NAME REFERRAL</th>
                    <th>TOTAL ACTIVE STAKING</th>
                    <th>TOTAL ACTIVE MINING</th>
                </thead>
                <tbody>
                    @foreach($dataReferal->referal_list as $index=>$dt)
                    <tr class="referral" data-toggle="collapse" data-target=".demo{{$index}}">
                        @if(count($dt->referal)>0)
                        <td><i class='fa fa-plus'></i>{{$dt->name}}</td>
                        @else
                        <td style="padding-left:28px;">{{$dt->name}}</td>
                        @endif
                        <td>{{$dt->total_active_staking}}</td>
                        <td>{{$dt->total_active_mining}}</td>
                        @foreach($dt->referal as $i=>$d)
                    <tr class="collapse demo{{$index}} child-referal">
                        <td>{{$d->name}}</td>
                        <td>{{$d->total_active_staking}}</td>
                        <td>{{$d->total_active_mining}}</td>
                    </tr>
                    @endforeach
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>




    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->

@include('User.footer')

<script src="{{URL('/js/wallet.js')}}"></script>