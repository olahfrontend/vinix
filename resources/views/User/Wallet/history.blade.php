@include('User.header',['activePage' => 'wallet'])

<link rel="stylesheet" href="{{URL::asset('css/wallet.css')}}">
<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div id="content1">
            <h1>MY HISTORY</h1>
            <h4>History of my transaction in Vinix</h4>
        </div>

        <div id="content3" class="row" style="padding:15px;">
            <table id="myTable" class="table table-bordered display">
                <thead>
                    <tr>
                        <th style="text-align:center;">Date Transaction</th>
                        <th style="text-align:center;">Transaction</th>
                        <th style="text-align:center;">Fee</th>
                        <th style="text-align:center;">Amount</th>
                        <th style="text-align:center;">Currency</th>
                        <th style="text-align:center;">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($dataWithdraw as $dt)
                    <tr>
                        <td>{{$dt->created_at}}</td>
                        <td>{{$dt->trx_type}}</td>
                        <td>{{$dt->total_fee}}</td>
                        <td>{{$dt->amount}}</td>
                        <td>{{$dt->code}}</td>
                        <td style="text-align:center;">
                            @if($dt->id_ref > 0)
                            <a href="{{URL('wallet/detail').'?id='.$dt->id_ref.'&ref='.$dt->trx_type}}">
                                <div class="btn btn-primary">Detail</div>
                            </a>
                            @else
                            <div class="btn btn-primary" disabled>Detail</div>
                            @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->

@include('User.footer')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.css">

<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.js"></script>
<script>
    $(document).ready(function() {
        $('#myTable').DataTable({

            "order": [
                [0, "desc"]
            ]
        });
    });
</script>
<script src="{{URL('/js/wallet.js')}}"></script>