<!doctype html>
<html lang="en">

<head>
    <title>Vinix</title>
    <link rel="icon" href="{{URL::asset('image/logo.png')}}" type="image/x-icon">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <!-- VENDOR CSS -->
    <link rel="stylesheet" href="{{URL::asset('assets/vendor/bootstrap/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{URL::asset('assets/vendor/font-awesome/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{URL::asset('assets/vendor/linearicons/style.css')}}">
    <link rel="stylesheet" href="{{URL::asset('assets/vendor/chartist/css/chartist-custom.css')}}">


    <link href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" rel="stylesheet">

    <!-- MAIN CSS -->
    <link rel="stylesheet" href="{{URL::asset('assets/css/main.css')}}">
    <link rel="stylesheet" href="{{URL::asset('css/register.css')}}">
    <!-- GOOGLE FONTS -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
    <!-- ICONS -->
    <link rel="apple-touch-icon" sizes="76x76" href="{{URL::asset('assets/img/apple-icon.png')}}">

</head>

<body>
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="brand">
            <a href="/"><img src="{{URL::asset('assets/img/logo-dark.png')}}" alt="Klorofil Logo" class="img-responsive logo"></a>
        </div>
        <div class="container-fluid">
            <div class="navbar-btn navbar-btn-right">
                <div class="btn btn-primary btn-login">SIGN UP</div>
            </div>
            <div class="navbar-btn navbar-btn-right">
                <div class="btn btn-primary btn-register" data-toggle="modal" data-target="#modalLogin"><span>SIGN IN</span></div>
            </div>
        </div>
    </nav>

    <div id="content1">
        <img src="{{URL::asset('image/logo.png')}}" width="200px" height="auto" />
        <h1>V I N I X</h1>
    </div>
    <h4 style="text-align:center; font-weight:bold;">VERIFICATION WITHDRAWAL</h4>

    @if(!isset($success))
    <div id="content2">
        <p style="text-align:center;">
            The verification has been generated, please check your email to activate your account.
        </p>
    </div>
    @else
    @if($success)
    <div id="wait" style="text-align:center;margin-top:25px;">
        <p style="font-size:16px;">Your Withdrawal request confirmed. Please wait for confirmation 1 x 24 hours</p>
    </div>
    @else
    <div id="wait" style="text-align:center;margin-top:25px;">
        <p style="font-size:16px;">Your verification code has expired please make withdrawal again.</p>
    </div>
    @endif
    @endif


    <div id="modalLogin" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <div class="modal-subheader">
                        <img src="{{URL::asset('image/logo.png')}}" width="100px" height="auto" />
                        <h2>V I N I X</h2>
                    </div>
                    <form method="POST" action="{{url('do-login')}}">
                        <div class="form-group">
                            @csrf
                            <label>Email</label><br>
                            <input type="text" placeholder="Email" name="email">
                        </div>
                        <div class="form-group">
                            <label>Password</label><br>
                            <input type="password" placeholder="Password" name="password">
                        </div>
                        <div style="text-align:right;">
                            <a href="/forgot-password">Forgot password</a>
                        </div>
                        <div class="btn-container">
                            <button class="btn btn-primary" type="submit">SIGN IN</button>
                        </div>
                    </form>
                    <div class="dont-have">
                        I don't have account, <a href="#">create a new one.</a>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <!-- Javascript -->
    <script src="{{URL::asset('assets/vendor/jquery/jquery.min.js')}}"></script>
    <script src="{{URL::asset('assets/vendor/bootstrap/js/bootstrap.min.js')}}"></script>
    <script src="{{URL::asset('assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
    <script src="{{URL::asset('assets/vendor/jquery.easy-pie-chart/jquery.easypiechart.min.js')}}"></script>
    <script src="{{URL::asset('assets/vendor/chartist/js/chartist.min.js')}}"></script>
    <script src="{{URL::asset('assets/scripts/klorofil-common.js')}}"></script>

</body>


</html>