@include('User.header',['activePage' => 'wallet'])

<link rel="stylesheet" href="{{URL::asset('css/wallet.css')}}">
<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div id="content1">
            <h1>MY BALANCE</h1>
            <h4>view and arrange your wallet balance in Vinix</h4>
            <a href="/history-deposit">
                <div class="btn btn-primary">History</div>
            </a>
        </div>

        <div id="content3" class="row">
            @foreach($dataWallet as $dt)
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="item">
                    <h3>{{$dt->coin_name}}</h3>
                    <img src="{{$dt->url_icon}}" width="100px" height="auto">
                    @if($dt->saldo < 1000) <h4>{{number_format($dt->saldo,8,".",",")}}</h4>
                        @else
                        <h4>{{number_format($dt->saldo,2,".",",")}}</h4>
                        @endif
                        <p>{{$dt->code}} in Wallet</p>
                        <?php $locked = false; ?>
                        @foreach($exclude as $e)
                        @if($e == $dt->code)
                        <div class="btn btn-primary unlock" disabled>Withdraw</div>
                        <?php $locked = true; ?>
                        @endif
                        @endforeach
                        @if(!$locked)
                        <div class="btn btn-primary unlock" onclick="showModalTransfer({{json_encode($dt)}})">Withdraw</div>
                        @endif
                        <!-- <button class="btn btn-primary unlock">Details</button> -->
                </div>
            </div>
            @endforeach
        </div>
        <!-- <div id="content2">
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item active">
                    <a class="nav-link" id="usd-tab" data-toggle="tab" href="#usd" role="tab" aria-controls="usd" aria-selected="true">USD</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="vinix-tab" data-toggle="tab" href="#vinix" role="tab" aria-controls="vinix" aria-selected="false">VINIX</a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane fade in active panel" id="usd" role="tabpanel" aria-labelledby="usd-tab">
                    <div class="panel-heading">
                        <h3 class="panel-title">History USD Balance</h3>
                        <h2>1.345,32 $</h2>
                    </div>
                    <div class="panel-body">
                        <div id="demo-line-chart" class="ct-chart"></div>
                    </div>
                </div>
                <div class="tab-pane fade panel" id="vinix" role="tabpanel" aria-labelledby="vinix-tab">
                    <div class="panel-heading">
                        <h3 class="panel-title">History VINIX Balance</h3>
                        <h2>35.423,00 VNX</h2>
                    </div>
                    <div class="panel-body">
                        <div id="demo2-line-chart" class="ct-chart"></div>
                    </div>
                </div>
            </div>
        </div> -->
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->
<!-- Modal -->
<div id="modalTransfer" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Transfer</h4>
            </div>
            <div class="modal-body">
                <div class="modal-content">
                    <form action="{{URL('/wallet/transfer')}}" method="POST">
                        @csrf
                        <div class="item" style="background-color:transparent; border:none;">
                            <h3 id="modal-name"></h3>
                            <img id="modal-image" src="{{$dt->url_icon}}" width="100px" height="auto">
                            <h5 style="color:#E05227;font-weight:bold;margin-top:25px;">Balance</h5>
                            <h4 id="modal-saldo"></h4>
                            <h5 style="color:#E05227;font-weight:bold;">Transfer to</h5>
                            <input type="text" placeholder="Address" id="input-address" name="address" />
                            <h5 style="color:#E05227;font-weight:bold;">Amount</h5>
                            <input type="number" placeholder="0.0" id="input-amount" name="amount" step="any" />
                            <h5 style="color:#E05227;font-weight:bold;">Fee Transfer</h5>
                            <h4 id="modal-fee">0.0</h4>
                            <button type="submit" class="btn btn-primary">Withdraw</button>
                            <input type="hidden" name="currency" id="input_currency" />
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>
</div>

@include('User.footer')
<script>
    var fee = <?php echo json_encode($dataUser->setting); ?>;
    $(function() {
        $("#vinix").css("display", "block");
        var options;

        var data = {
            labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
            series: [
                [200, 380, 350, 320, 410, 450, 570, 800, 1555, 1620, 950, 1345],
            ]
        };
        var data2 = {
            labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
            series: [
                [11250, 12340, 14250, 15620, 17310, 22450, 26170, 28400, 29455, 30320, 31250, 35423],
            ]
        };

        // line chart
        options = {
            height: "300px",
            showPoint: true,
            axisX: {
                showGrid: false
            },
            lineSmooth: false,
        };

        new Chartist.Line('#demo-line-chart', data, options);
        new Chartist.Line('#demo2-line-chart', data2, options);
    });
</script>
<script src="{{URL('/js/wallet.js')}}"></script>