@include('User.header',['activePage' => 'wallet'])

<link rel="stylesheet" href="{{ URL::asset('css/detailfarm.css') }}">
<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div id="content">
            @if ($data->type_ref == 'EXCHANGE')
                <div>
                    <h1>EXCHANGE DETAIL HISTORY</h1>

                    <div class="container-desc">
                        <div class="desc">
                            <h5>Transaction Date</h5>
                            <p>{{ $data->created_at }}</p>
                        </div>
                        <div class="desc">
                            <h5>From Coin</h5>
                            <p>{{ $data->from_coin }}</p>
                        </div>
                        <div class="desc">
                            <h5>To Coin</h5>
                            <p>{{ $data->to_coin }}</p>
                        </div>
                    </div>
                    <div class="container-desc">
                        <div class="desc">
                            <h5>Exchange Rate</h5>
                            <p>{{ $data->rate }}</p>
                        </div>
                        <div class="desc">
                            <h5>From Amount</h5>
                            <p>{{ $data->from_amount }}</p>
                        </div>
                        <div class="desc">
                            <h5>To Amount</h5>
                            <p>{{ $data->to_amount }}</p>
                        </div>
                    </div>
                </div>
            @elseif($data->type_ref == 'MINING')
                <div>
                    <h1>MINING DETAIL HISTORY</h1>

                    <div class="container-desc">
                        <div class="desc">
                            <h5>Transaction Date</h5>
                            <p>{{ $data->created_at }}</p>
                        </div>
                        <div class="desc">
                            <h5>End Date</h5>
                            <p>{{ $data->end_at }}</p>
                        </div>
                        <div class="desc">
                            <h5>Last Harvest</h5>
                            <p>{{ $data->last_harvest }}</p>
                        </div>
                    </div>
                    <div class="container-desc">
                        <div class="desc">
                            <h5>Duration</h5>
                            <p>{{ $data->tenor }}</p>
                        </div>
                        <div class="desc">
                            <h5>Mining Speed</h5>
                            <p>{{ $data->mining_speed }}</p>
                        </div>
                        <div class="desc">
                            <h5>Price Buy</h5>
                            <p>${{ $data->price }}</p>
                        </div>
                    </div>
                    <div class="container-desc">
                        <div class="desc">
                            <h5>Status</h5>
                            @if ($data->status == 0)
                                <p>On Going</p>
                            @elseif($data->status == 1)
                                <p>Done</p>
                            @else
                                <p>Cancelled</p>
                            @endif
                        </div>
                    </div>
                </div>
            @elseif($data->type_ref == 'STAKING')
                <div>
                    <h1>STAKING DETAIL HISTORY</h1>

                    <div class="container-desc">
                        <div class="desc">
                            <h5>Transaction Date</h5>
                            <p>{{ $data->created_at }}</p>
                        </div>
                        <div class="desc">
                            <h5>End Date</h5>
                            <p>{{ $data->end_at }}</p>
                        </div>
                        <div class="desc">
                            <h5>Last Harvest</h5>
                            <p>{{ $data->harvest_at }}</p>
                        </div>
                    </div>
                    <div class="container-desc">
                        <div class="desc">
                            <h5>Amount Stake</h5>
                            <p>{{ $data->amount_stake }}</p>
                        </div>
                        <div class="desc">
                            <h5>Currency</h5>
                            <p>{{ $data->detail[0]->code }}</p>
                        </div>
                        <div class="desc">
                            <h5>Status</h5>
                            @if ($data->status == 0)
                                <p>On Going</p>
                            @elseif($data->status == 1)
                                <p>Done</p>
                            @else
                                <p>Cancelled</p>
                            @endif
                        </div>
                    </div>
                </div>
            @elseif($data->type_ref == 'WITHDRAW' || $data->type_ref =='DEPOSIT')
                <div>
                    <h1>{{ $data->type_ref }} HISTORY</h1>

                    <div class="container-desc">
                        <div class="desc">
                            <h5>Transaction Date</h5>
                            <p>{{ $data->created_at }}</p>
                        </div>
                        <div class="desc">
                            <h5>Currency</h5>
                            <p>{{ $data->id_coin }}</p>
                        </div>
                        <div class="desc">
                            <h5>Address</h5>
                            <p>{{ $data->address }}</p>
                        </div>
                    </div>
                    <div class="container-desc">
                        <div class="desc">
                            <h5>Status</h5>
                            @if ($data->status == 0 && $data->verified == 0)
                                <p>Waiting Confirmation Email</p>
                            @elseif($data->status == 0 && $data->verified == 1)
                                <p>Pending</p>
                            @elseif($data->status == 1)
                                <p>Done</p>
                            @else
                                <p>Rejected</p>
                            @endif
                        </div>
                        <div class="desc">
                            <h5>Amount</h5>
                            <p>{{ $data->amount + $data->fee }}</p>
                        </div>
                        <div class="desc">
                            <h5>Fee</h5>
                            <p>{{ $data->fee }}</p>
                        </div>
                    </div>
                    @if($data->txn_hash != "")
                        <div class="container-desc">
                            <div class="desc" style="flex-basis:100%;">
                                <h5>Txn Hash</h5>
                                <p>{{ $data->txn_hash }}</p>
                            </div>
                        </div>
                    @endif
                </div>
            @endif

            <h5 style="font-weight:bold;">HISTORY TRANSACTION</h5>
            <div style="overflow-x:scroll">
                <table style="overflow-x:auto;">
                    <thead>
                        <th>Date Transaction</th>
                        <th>Description</th>
                        <th>Total Amount</th>
                        <th>Fee</th>
                        <th>Netto</th>
                        <th>Currency</th>
                    </thead>
                    <tbody>
                        @foreach ($data->detail as $dt)
                            <tr>
                                <td>{{ $dt->created_at }}</td>
                                <td>{{ $dt->trx_type }}</td>
                                @if ($dt->amount > 0)
                                    <td>{{ $dt->amount + $dt->total_fee }}</td>
                                @else
                                    <td>{{ $dt->amount }}</td>
                                @endif
                                <td>{{ $dt->total_fee }}</td>
                                <td>{{ $dt->amount }}</td>
                                <td>{{ $dt->code }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->
<script src="{{ URL::asset('js/my-farm.js') }}"></script>
@include('User.footer')
