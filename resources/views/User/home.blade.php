@include('User.header',['activePage' => 'home'])

<link rel="stylesheet" href="{{URL::asset('css/home.css')}}">
<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="content1">
            <div class="title">
                <img src="{{URL::asset('image/logo.png')}}">
                <h1>V I N I X</h1>
            </div>
            <h4>The #1 safest and yield smart farm chain</h4>
        </div>

        <div class="content2">
            <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                    <div class="item">
                        <h3>Total Asset's Staking</h3>
                        <img src="{{URL::asset('image/logo-vnx.png')}}" width="100px" height="auto">
                        <p>Total Staking can harvested</p>
                        <h4>${{number_format($dataDashboard->total_can_harvest_staking,2,".",",")}}</h4>
                        <p>Asset's Staking</p>
                        <h4>${{number_format($dataDashboard->total_staking,2,".",",")}}</h4>
                        <a href="/my-farms">
                            <button class="btn btn-primary unlock">Details</button>
                        </a>
                    </div>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                    <div class="item">
                        <h3>Total Asset's Mining</h3>
                        <img src="{{URL::asset('image/logo-usd.png')}}" width="100px" height="auto">
                        <p>Total Mining can harvested</p>
                        <h4>${{number_format($dataDashboard->total_can_harvest_mining,2,",",".")}}</h4>
                        <p>Asset's Mining</p>
                        <h4>${{number_format($dataDashboard->total_mining,2,",",".")}}</h4>
                        <a href="/my-mining">
                            <button class="btn btn-primary unlock">Details</button>
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <div class="content3">
            <div class="row">
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6">
                    <div class="item">
                        <h4>Earn up to</h4>
                        <h2>P.A {{$dataDashboard->wallet->profit_usdv}}%</h2>
                        <h4>in USDV Farms</h4>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6">
                    <div class="item">
                        <h4>Earn up to</h4>
                        <h2>P.A {{$dataDashboard->wallet->profit_vnx}}%</h2>
                        <h4>in VNX Farms</h4>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6">
                    <div class="item">
                        <h4>Earn up to</h4>
                        <h2>P.A {{$dataDashboard->wallet->profit_eth}}%</h2>
                        <h4>in ETH Farms</h4>
                    </div>
                </div>
            </div>
        </div>

        <div class="content4">
            <div class="row">
                <!-- <div class="col-xl-5 col-lg-5 col-md-5 col-sm-12 item">
                    <h4>Vinix Stats</h4>
                    <div class="detail">
                        <div class="left-side">
                            Total Vinix Supply
                        </div>
                        <div class="right-side">
                            166,888,736
                        </div>
                    </div>
                    <div class="detail">
                        <div class="left-side">
                            Total Vinix Burned
                        </div>
                        <div class="right-side">
                            0.000
                        </div>
                    </div>
                    <div class="detail">
                        <div class="left-side">
                            New Vinix/block
                        </div>
                        <div class="right-side">
                            25
                        </div>
                    </div>
                </div> -->

                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                    <div class="item">
                        <h4>Total Value Locked (TVL)</h4>
                        <h2 id="total_value_locked">$0</h2>
                        <p>Across all Staking and Mining Pools</p>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->
<script>
    function animateValue(obj, start, end, duration, prefix = "") {
        let startTimestamp = null;
        const step = (timestamp) => {
            if (!startTimestamp) startTimestamp = timestamp;
            const progress = Math.min((timestamp - startTimestamp) / duration, 1);
            if (prefix == "") {
                obj.innerHTML = prefix + numberWithCommas(parseFloat(progress * (end - start) + start).toFixed());
            } else {
                obj.innerHTML = prefix + numberWithCommas(parseFloat(progress * (end - start) + start).toFixed(2));
            }
            if (progress < 1) {
                window.requestAnimationFrame(step);
            }
        };
        window.requestAnimationFrame(step);
    }
    $(document).ready(function() {
        console.log(Date.now());
        var pembanding_supply = parseFloat("0.000000000172");
        var pembanding_value = parseFloat("0.000000452");
        var total_vinix_supply = parseFloat(Date.now() * pembanding_supply).toFixed();
        var total_value_locked = parseFloat(Date.now() * pembanding_value).toFixed(2);

        // $("#total_vinix_supply").html(numberWithCommas(total_vinix_supply));
        // $("#total_value_locked").html("$" + numberWithCommas(total_value_locked));


        const obj = document.getElementById("total_member");
        animateValue(obj, 0, total_vinix_supply, 2000);
        const obj2 = document.getElementById("total_value_locked");
        animateValue(obj2, 0, total_value_locked, 2000, "$");
    });

    function numberWithCommas(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }
</script>
@include('User.footer')