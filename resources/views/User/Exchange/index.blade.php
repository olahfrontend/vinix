@include('User.header',['activePage' => 'exchange'])

<link rel="stylesheet" href="{{URL::asset('css/exchange.css')}}">
<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div id="content1">
            <h1>EXCHANGE CURRENCY</h1>
            <h4>Trade tokens in an instant</h4>
        </div>
        <!-- <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item active">
                <a class="nav-link" id="usd-tab" data-toggle="tab" href="#usd" role="tab" aria-controls="usd" aria-selected="true">USD</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="vinix-tab" data-toggle="tab" href="#vinix" role="tab" aria-controls="vinix" aria-selected="false">VINIX</a>
            </li>
        </ul> -->
        <!-- <div class="tab-content" id="myTabContent">
            <div class="tab-pane active" id="usd" role="tabpanel" aria-labelledby="usd-tab"> -->
        <div class="item">
            <h3>Exchange Token</h3>
            <h5>Trade your token to another currency</h5>

            <form id="formExchange" action="{{url('/exchange/do_exchange')}}" method="POST">
                @csrf
                <div class="form-controller">
                    <div style="display:flex">
                        <h4 style="flex-basis:40%;">From</h4>
                        <h4 id="saldo_from" style="text-align:right;flex-basis:60%;">Balance : {{$dataUser->wallet[0]->saldo}}</h4>
                    </div>
                    <div class="input" id="form_usd_from">
                        <input type="hidden" id="id_usd_from" name="usd_from_currency" value="ETH" />
                        <div class="amount">
                            <input type="number" step="0.0001" placeholder="0.0" id="usd_from" name="usd_from" onkeyup="inputUSDChange('from')" />
                        </div>
                        <div class="currency" onclick="openModal('from')" id="usd_from_container">
                            <img src="{{URL::asset('image/logo-eth.png')}}" width="20px" height="auto" id="icon_usd_from" />
                            <div style="all: unset;" id="usd_from_currency">ETH</div>
                            <i class="fa fa-angle-down"></i>
                        </div>
                    </div>
                    <div class="max">
                        <div class="btn btn-primary" onclick="clickMax('from')">MAX</div>
                    </div>
                </div>
                <div class="swap-container" onclick="clickSwap()">
                    <i class="fa fa-arrow-down"></i>
                </div>
                <div class="form-controller">
                    <div style="display:flex">
                        <h4 style="flex-basis:40%;">To</h4>
                        <h4 id="saldo_to" style="text-align:right;flex-basis:60%;">Balance : {{$dataUser->wallet[1]->saldo}}</h4>
                    </div>
                    <div class="input" id="form_usd_to">
                        <input type="hidden" id="id_usd_to" name="usd_to_currency" value="VNX" />
                        <div class="amount">
                            <input type="number" step="0.0001" placeholder="0.0" id="usd_to" name="usd_to" onkeyup="inputUSDChange('to')" />
                        </div>
                        <div class="currency" onclick="openModal('to')" id="usd_to_container">
                            <img src="{{URL::asset('image/logo-vnx.png')}}" width="20px" height="auto" id="icon_usd_to" />
                            <div style="all: unset;" id="usd_to_currency">VNX</div>
                            <i class="fa fa-angle-down"></i>
                        </div>
                    </div>
                    <div class="max">
                        <div class="btn btn-primary" onclick="clickMax('to')">MAX</div>
                    </div>
                </div>
                <div class="container">
                    <div class="btn btn-primary exchange" onclick="confirmModal()">Exchange</div>
                </div>
            </form>
        </div>
        <!-- </div> -->
        <!-- <div class="tab-pane fade" id="vinix" role="tabpanel" aria-labelledby="vinix-tab">
                <div class="item">
                    <h3>Exchange VINIX</h3>
                    <h5>Trade your USD to Vinix coin or vice versa</h5>

                    <div class="form-controller">
                        <h4>From</h4>
                        <div class="input">
                            <div class="amount">
                                <input type="text" placeholder="0.0" />
                            </div>
                            <div class="currency fix">
                                <img src="{{URL::asset('image/logo-usd.png')}}" width="20px" height="auto" />
                                USD
                                <i class="fa fa-angle-down"></i>
                            </div>
                        </div>
                    </div>
                    <div class="swap-container">
                        <i class="fa fa-arrow-down"></i>
                    </div>
                    <div class="form-controller">
                        <h4>To</h4>
                        <div class="input">
                            <div class="amount">
                                <input type="text" placeholder="0.0" />
                            </div>
                            <div class="currency fix">
                                <img src="{{URL::asset('image/logo-vnx.png')}}" width="20px" height="auto" />
                                VNX
                                <i class="fa fa-angle-down"></i>
                            </div>
                        </div>
                    </div>
                    <div class="container">
                        <button class="btn btn-primary exchange">Exchange</button>
                    </div>
                </div>
            </div> -->
    </div>
</div>
<!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->
<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Select a token</h4>
            </div>
            <div class="modal-body">
                <h5>Token name</h5>
                @foreach($dataCoin as $dt)
                @if($dt->status == 1)
                <div class="list-token" onclick="SelectToken('{{$dt->id}}','{{$dt->url_icon}}','{{$dt->code}}')">
                    <img src="{{$dt->url_icon}}" width="25px" height="25px" />
                    <h5>{{$dt->coin_name}}</h5>
                </div>
                @endif
                @endforeach
            </div>
        </div>

    </div>
</div>
<div id="confirmModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Confirm Exchange</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-6">
                        <h5>FROM</h5>
                        <h4 id="confirmmodal-from">0.5 ETH</h4>
                    </div>
                    <div class="col-sm-6">
                        <h5>TO</h5>
                        <h4 id="confirmmodal-to">2.500 USDV</h4>
                    </div>
                </div>
                <h5>FEE EXCHANGE</h5>
                <h4 style="color:red" id="confirmmodal-fee">25 USDV</h4>
                <h5>TOTAL</h5>
                <h4 style="color:green;font-weight:bold;" id="confirmmodal-total">2.475 USDV</h4>

                <div class="container">
                    <div class="btn btn-primary" onclick="exchange()">Exchange</div>
                </div>
            </div>
        </div>

    </div>
</div>
<script>
    var fee_exchange = <?php echo $dataUser->setting->EXCHANGE_FEE->value; ?>;
    var walletUser = <?php echo json_encode($dataUser->wallet); ?>;
</script>
<script src="{{URL::asset('js/exchange.js')}}"></script>

@include('User.footer')