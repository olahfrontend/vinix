@include('Admin.header',['activePage' => 'master_user'])

<link rel="stylesheet" href="{{ URL::asset('css/wallet.css') }}">
<!-- MAIN -->
<style>
    .form-container {
        display: flex;
    }

    .form-container .form-group {
        flex-basis: 33%;
    }

    .form-group label {
        margin: 0px;
    }
</style>
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">

        <!-- BORDERED TABLE -->

        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title" style="margin-bottom:25px;">Detail Information</h3>
                <div class="form-container">
                    <div class="form-group">
                        <label>Name</label>
                        <p>{{ $data->user->name }}</p>
                    </div>
                    <div class="form-group">
                        <label>Email</label>
                        <p>{{ $data->user->email }}</p>
                    </div>
                    <div class="form-group">
                        <label>Joined at</label>
                        <p>{{ $data->user->created_at }}</p>
                    </div>
                </div>
                <div class="form-container">
                    <div class="form-group">
                        <label>Referal Code</label>
                        <p>{{ $data->user->referal_code }}</p>
                    </div>
                    <div class="form-group">
                        <label>Referal Parent</label>
                        @if ($data->user->referal_parent != '')
                        <p>{{ $data->user->referal_parent }}</p>
                        @else
                        <p>-</p>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>Status</label>
                        @if ($data->user->status == 1)
                        <p>Active</p>
                        @elseif($data->user->status == 0)
                        <p>Not Verified</p>
                        @endif
                    </div>
                </div>
            </div>
            <div class="panel-heading">
                <div class="btn btn-primary" onclick="openTopupCoin({{json_encode($data->user)}})">
                    Top Up Coin
                </div>
                <div class="btn btn-primary" onclick="openMiningCustom({{json_encode($data->user)}})">
                    Add Mining Custom
                </div>
            </div>
        </div>

        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">List Staking & Mining</h3>
            </div>

            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item active">
                    <a class="nav-link" id="pending-tab" data-toggle="tab" href="#pending" role="tab" aria-controls="pending" aria-selected="true">Staking</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="canceled-tab" data-toggle="tab" href="#canceled" role="tab" aria-controls="canceled" aria-selected="false">Mining</a>
                </li>
            </ul>

            <div class="tab-content" id="myTabContent">
                <div class="tab-pane active" id="pending" role="tabpanel" aria-labelledby="pending-tab">
                    <div class="panel-body">
                        <table id="myTable" class="table table-bordered display">
                            <thead>
                                <tr>
                                    <th>Date Staking</th>
                                    <th>Product name</th>
                                    <th>Amount Stake</th>
                                    <th>End At</th>
                                    <th>Last harvest</th>
                                    <th>Can Harvest</th>
                                    <th>Currency</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($data->staking as $dt)
                                <tr>
                                    <td>{{ $dt->created_at }}</td>
                                    <td>{{ $dt->product_name }}</td>
                                    <td>{{ $dt->amount_stake }}</td>
                                    <td>{{ $dt->end_at }}</td>
                                    <td>{{ $dt->harvest_at }}</td>
                                    <td>{{ $dt->can_harvest }}</td>
                                    <td>{{ $dt->coin_reward }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="tab-pane fade" id="canceled" role="tabpanel" aria-labelledby="canceled-tab">
                    <div class="panel-body">
                        <table id="myTable" class="table table-bordered display">
                            <thead>
                                <tr>
                                    <th>Date Mining</th>
                                    <th>Product name</th>
                                    <th>Mining Speed (Mhs)</th>
                                    <th>End At</th>
                                    <th>Last harvest</th>
                                    <th>Can Harvest</th>
                                    <th>Currency</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($data->mining as $dt)
                                <tr>
                                    <td>{{ $dt->created_at }}</td>
                                    @if (isset($dt->product_name))
                                    <td>{{ $dt->product_name }}</td>
                                    @else
                                    <td>Custom</td>
                                    @endif
                                    <td style="cursor:pointer;" onclick="openModal({{json_encode($dt)}})">{{ $dt->mining_speed }}</td>
                                    <td>{{ $dt->end_at }}</td>
                                    <td>{{ $dt->last_harvest }}</td>
                                    <td>{{ number_format($dt->can_harvest,8) }}</td>
                                    <td>{{ $dt->reward_coin }}</td>
                                    <td>
                                        <a href="/my-mining/log/{{$dt->id}}" class="btn btn-primary">Log</a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">History Harvest</h3>
            </div>

            <ul class="nav nav-tabs" id="myTab2" role="tablist">
                <li class="nav-item active">
                    <a class="nav-link" id="pending-tab2" data-toggle="tab" href="#pending2" role="tab" aria-controls="pending2" aria-selected="true">Staking</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="canceled-tab2" data-toggle="tab" href="#canceled2" role="tab" aria-controls="canceled2" aria-selected="false">Mining</a>
                </li>
            </ul>

            <div class="tab-content" id="myTabContent2">
                <div class="tab-pane active" id="pending2" role="tabpanel" aria-labelledby="pending-tab2">
                    <div class="panel-body">
                        <table id="myTable" class="table table-bordered display">
                            <thead>
                                <tr>
                                    <th>Date Harvest</th>
                                    <th>Product name</th>
                                    <th>Amount Stake</th>
                                    <th>Harvest Fee</th>
                                    <th>Harvest Amount</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($data->history_harvest_staking as $dt)
                                <tr>
                                    <td>{{ $dt->date_transaction }}</td>
                                    <td>{{ $dt->product_name }}</td>
                                    <td>{{ $dt->amount_stake }}</td>
                                    <td>{{ $dt->total_fee }}</td>
                                    <td>{{ $dt->amount }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="tab-pane fade" id="canceled2" role="tabpanel2" aria-labelledby="canceled-tab2">
                    <div class="panel-body">
                        <table id="myTable" class="table table-bordered display">
                            <thead>
                                <tr>
                                    <th>Date Harvest</th>
                                    <th>Product name</th>
                                    <th>Mining Speed</th>
                                    <th>Harvest Fee</th>
                                    <th>Harvest Amount</th>
                                    <th>Currency</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($data->history_harvest_mining as $dt)
                                <tr>
                                    <td>{{ $dt->date_transaction }}</td>
                                    @if(isset($dt->product_name))
                                    <td>{{ $dt->product_name }}</td>
                                    @else
                                    <td>Custom</td>
                                    @endif
                                    <td>{{ $dt->mining_speed}}</td>
                                    <td>{{ $dt->total_fee }}</td>
                                    <td>{{ $dt->amount }}</td>
                                    <td>{{ $dt->code }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- END BORDERED TABLE -->
    </div>
    <!-- END MAIN CONTENT -->
</div>

<div id="modalEditMhs" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Edit MHS</h4>
            </div>
            <div class="modal-body">
                <div class="modal-content">
                    <form action="{{ URL('/admin/do_edit_mhs') }}" method="POST">
                        @csrf
                        <input type="hidden" id="edit-id" name="id" />
                        <input type="number" step="any" id="edit-speed" name="speed" /> MHS<br><br>
                        <button type="submit" class="btn btn-primary">Edit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="modalAddCustomMining" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add Custom Mining</h4>
            </div>
            <div class="modal-body">
                <div class="modal-content">
                    <form action="{{ URL('/admin/do_add_custom_mining') }}" method="POST">
                        @csrf
                        <input type="hidden" id="id_user" name="id_user" />
                        <div class="form-group">
                            <label>Speed</label><br>
                            <input type="number" step="any" name="mining_speed" placeholder="MHS">
                        </div>
                        <div class="form-group">
                            <label>Tenor</label><br>
                            <input type="number" name="tenor">
                        </div>
                        <div class="form-group">
                            <label>Reward Coin</label><br>
                            <select style="width:80%" name="id_coin_currency">
                                @foreach($data_coin as $dt)
                                <option value="{{$dt->id}}">{{$dt->coin_name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Payment Currency</label><br>
                            <select style="width:80%" name="type_coin_reward">
                                @foreach($data_coin as $dt)
                                <option value="{{$dt->code}}">{{$dt->coin_name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <button type="submit" class="btn btn-primary">Add</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- MODAL TOP UP COIN -->
<div id="modalTopupCoin" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Top Up Coin</h4>
            </div>
            <div class="modal-body">
                <div class="modal-content">
                    <form action="{{ URL('/admin/do_topup_admin') }}" method="POST">
                        @csrf
                        <input type="hidden" id="id_user_topup" name="id_user" />
                        <div class="form-group">
                            <label>Coin</label><br>
                            <select style="width:80%" name="id_coin" id="id_coin_balance" onchange="updateBalance({{json_encode($data->user)}})">
                                @foreach($data_coin as $dt)
                                <option value="{{$dt->id}}">{{$dt->coin_name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Amount</label><br>
                            <input style="width:80%" type="number" step="any" name="nominal" />
                        </div>
                        <div class="form-group">
                            <label>Balance</label>
                            <h5 id="balance" style="font-weight:bold; margin:0px;">0</h5>
                        </div>
                        <button type="submit" class="btn btn-primary">Top Up</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END MAIN -->
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

@include('Admin.footer')
<script>
    //FUNCTION SHOW MODAL EDIT MHS
    function openModal(data) {
        console.log(data);
        $("#modalEditMhs").modal();

        $("#edit-id").val(data.id);
        $("#edit-speed").val(data.mining_speed);
    }
    //FUNCTION SHOW MODAL CUSTOM MINING
    function openMiningCustom(data) {
        // console.log(data.id);
        $("#modalAddCustomMining").modal();
        $("#id_user").val(data.id);

    }

    function openTopupCoin(data) {
        console.log(data);
        $("#modalTopupCoin").modal();
        $("#id_user_topup").val(data.id);
        updateBalance(data);
    }

    function updateBalance(data) {
        // console.log($("#id_coin_balance").val());
        // console.log($("#id_coin_balance").prop('selectedIndex'));
        var a = data.balance[$("#id_coin_balance").prop("selectedIndex")];
        $("#balance").html(a.saldo);
        // console.log(a);
    }
</script>