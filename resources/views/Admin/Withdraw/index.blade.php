@include('Admin.header',['activePage' => 'withdraw'])

<link rel="stylesheet" href="{{URL::asset('css/wallet.css')}}">
<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">

        <!-- BORDERED TABLE -->
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">List Request Withdrawal</h3>
            </div>

            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item active">
                    <a class="nav-link" id="pending-tab" data-toggle="tab" href="#pending" role="tab"
                        aria-controls="pending" aria-selected="true">PENDING</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="canceled-tab" data-toggle="tab" href="#canceled" role="tab"
                        aria-controls="canceled" aria-selected="false">CANCELED</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="done-tab" data-toggle="tab" href="#done" role="tab" aria-controls="done"
                        aria-selected="false">COMPLETED</a>
                </li>
            </ul>

            <div class="tab-content" id="myTabContent">
                <div class="tab-pane active" id="pending" role="tabpanel" aria-labelledby="pending-tab">
                    <div class="panel-body">
                        @if (count($dataWithdraw) > 0)
                            <table id="myTable" class="table table-bordered display">
                                <thead>
                                    <tr>
                                        <th>Nama Penarik</th>
                                        <th>Email</th>
                                        <th>Address</th>
                                        <th>Total Withdrawal</th>
                                        <th>Fee</th>
                                        <th>Netto Withdrawal</th>
                                        <th>Currency</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($dataWithdraw as $dt)
                                        @if ($dt->status == 0)
                                            <tr>
                                                <td>{{ $dt->name }}</td>
                                                <td>{{ $dt->email }}</td>
                                                <td>{{ $dt->address }}</td>
                                                <td>{{ $dt->amount + $dt->fee }}</td>
                                                <td>{{ $dt->fee }}</td>
                                                <td>{{ $dt->amount }}</td>
                                                <td>{{ $dt->code }}</td>
                                                <td style="padding:5px;">
                                                    <div class="btn btn-success"
                                                        onclick="modalConfirm(<?php echo $dt->id; ?>,this,1)"
                                                        style="width:100%;margin-bottom:5px;"><i
                                                            class="fa fa-check"></i>
                                                    </div>
                                                    <div class="btn btn-danger"
                                                        onclick="confirm(<?php echo $dt->id; ?>,this,-1)"
                                                        style="width:100%;"><i class="fa fa-close"></i></div>
                                                </td>
                                            </tr>
                                        @endif
                                    @endforeach
                                </tbody>
                            </table>
                    </div>
                </div>

                <div class="tab-pane fade" id="canceled" role="tabpanel" aria-labelledby="canceled-tab">
                    <div class="panel-body">
                        <table id="myTable" class="table table-bordered display">
                            <thead>
                                <tr>
                                    <th>Nama Penarik</th>
                                    <th>Email</th>
                                    <th>Address</th>
                                    <th>Total Withdrawal</th>
                                    <th>Fee</th>
                                    <th>Netto Withdrawal</th>
                                    <th>Currency</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($dataWithdraw as $dt)
                                    @if ($dt->status == -1)
                                        <tr>
                                            <td>{{ $dt->name }}</td>
                                            <td>{{ $dt->email }}</td>
                                            <td>{{ $dt->address }}</td>
                                            <td>{{ $dt->amount + $dt->fee }}</td>
                                            <td>{{ $dt->fee }}</td>
                                            <td>{{ $dt->amount }}</td>
                                            <td>{{ $dt->code }}</td>
                                        </tr>
                                    @endif
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="tab-pane fade" id="done" role="tabpanel" aria-labelledby="done-tab">
                    <div class="panel-body">
                        <table id="myTable" class="table table-bordered display">
                            <thead>
                                <tr>
                                    <th>Nama Penarik</th>
                                    <th>Email</th>
                                    <th>Address</th>
                                    <th>Total Withdrawal</th>
                                    <th>Fee</th>
                                    <th>Netto Withdrawal</th>
                                    <th>Currency</th>
                                    <th>Txn hash</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($dataWithdraw as $dt)
                                    @if ($dt->status == 1)
                                        <tr>
                                            <td>{{ $dt->name }}</td>
                                            <td>{{ $dt->email }}</td>
                                            <td>{{ $dt->address }}</td>
                                            <td>{{ $dt->amount + $dt->fee }}</td>
                                            <td>{{ $dt->fee }}</td>
                                            <td>{{ $dt->amount }}</td>
                                            <td>{{ $dt->code }}</td>
                                            <td>{{ $dt->txn_hash }}</td>
                                        </tr>
                                    @endif
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        @else
            <h4 style="text-align:center;">No Request Withdrawal</h4>
            @endif
        </div>
        <!-- END BORDERED TABLE -->
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
    $(document).ready(function() {
        $('#myTable').DataTable();
    });

    function confirm(id, comp, status, txn_hash = "") {
        var text = "Ingin menyelesaikan Transaksi withdrawal ini";
        var result = "Transaksi Berhasil diselesaikan";
        if (status == -1) {
            text = "Ingin menolak Transaksi withdrawal ini";
            result = "Transaksi berhasil ditolak";
        }
        swal({
                title: "Apakah anda yakin?",
                text: text,
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    //DELETE DATA
                    var formData = new FormData();
                    formData.append("_token", '<?php echo csrf_token(); ?>')
                    formData.append("id", id);
                    formData.append("status", status);
                    formData.append("txn_hash", txn_hash);
                    $.ajax({
                        type: 'POST',
                        processData: false,
                        contentType: false,
                        url: '{{ url('/api/confirm_withdraw') }}',
                        data: formData,
                        success: function(data) {
                            console.log(data);
                            $(comp).parents('tr').detach();
                            swal(result, {
                                icon: "success",
                            });
                        }
                    }).fail(function(data) {
                        console.log(data);
                        swal("Oops Something Wrong!", {
                            icon: "error",
                        });
                    });
                }
            });
    }

    function modalConfirm(id, comp, status) {
        swal({
                title: "Input Txn hash of this transaction to complete this withdrawal",
                buttons: true,
                dangerMode: true,
                content: "input",
            })
            .then((value) => {
                if (value) {
                    confirm(id, comp, status, value);
                }
            });
    }

</script>
@include('Admin.footer')
