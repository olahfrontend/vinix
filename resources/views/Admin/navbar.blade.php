<!-- NAVBAR -->
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="brand">
        <a href="/"><img src="{{URL::asset('assets/img/logo-dark.png')}}" alt="Klorofil Logo" class="img-responsive logo"></a>
    </div>
    <div class="container-fluid">
        <div class="navbar-btn">
            <button type="button" class="btn-toggle-fullwidth"><i class="lnr lnr-arrow-left-circle"></i></button>
        </div>
        <div class="navbar-btn navbar-btn-right">
            <form action="{{URL('logout')}}" method="POST" id="form-logout">
                @csrf
                <a class="btn btn-primary btn-login" onclick="Logout()"><span>SIGN OUT</span></a>
            </form>
        </div>
    </div>
</nav>
<!-- END NAVBAR -->
<!-- LEFT SIDEBAR -->
<div id="sidebar-nav" class="sidebar">
    <h5 style="text-align:center;color:white;font-weight:bold;">ADMIN PANEL</h5>
    <div class="sidebar-scroll">
        <nav>
            <ul class="nav">
                <li><a href="{{URL('admin/dashboard')}}" class="{{$activePage == 'dashboard' ? 'active' :''}}"><i class="lnr lnr-home"></i> <span>Dashboard</span></a></li>
                <li><a href="{{URL('admin/master_user')}}" class="{{$activePage == 'master_user' ? 'active' : ''}}"><i class="fas fa-users"></i> <span>User</span></a></li>
                <li><a href="{{URL('admin/withdraw')}}" class="{{$activePage == 'withdraw' ? 'active' :''}}"><i class="lnr lnr-cart"></i> <span>Withdraw</span></a></li>
                <li><a href="{{URL('admin/deposit')}}" class="{{$activePage == 'deposit' ? 'active' : ''}}"><i class="fas fa-exchange-alt"></i> <span>Deposit</span></a></li>
                <li><a href="{{URL('admin/master_staking')}}" class="{{$activePage == 'master_staking' ? 'active' : ''}}"><i class="fas fa-exchange-alt"></i> <span>Master Staking</span></a></li>
                <li><a href="{{URL('admin/master_mining')}}" class="{{$activePage == 'master_mining' ? 'active' : ''}}"><i class="fas fa-exchange-alt"></i> <span>Master Mining</span></a></li>
                <li><a href="{{URL('admin/master_setting')}}" class="{{$activePage == 'master_setting' ? 'active' : ''}}"><i class="fas fa-exchange-alt"></i> <span>Setting</span></a></li>
                
                <!-- <li>
                    <a href="#subPages" data-toggle="collapse" class="{{$activePage == 'farms' || $activePage == 'my-farms' ? 'active' : 'collapsed'}}" aria-expanded="{{$activePage == 'farms' || $activePage == 'my-farms' ? 'true' : 'false'}}"><i class="fas fa-truck"></i> <span>Farms</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
                    <div id="subPages" class="{{$activePage == 'farms' || $activePage == 'my-farms' ? 'collapse in' : 'collapse'}}">
                        <ul class="nav">
                            <li><a href="/farms" class="{{$activePage == 'farms' ? 'active' : ''}}">Stake</a></li>
                            <li><a href="/my-farms" class="{{$activePage == 'my-farms' ? 'active' :''}}">My Farms</a></li>
                        </ul>
                    </div>
                </li> -->
            </ul>
        </nav>
    </div>
</div>
<!-- END LEFT SIDEBAR -->