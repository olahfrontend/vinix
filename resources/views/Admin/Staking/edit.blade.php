@include('Admin.header',['activePage' => 'master_staking'])



<link rel="stylesheet" href="{{URL::asset('css/home.css')}}">
<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">

        <!-- BORDERED TABLE -->
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">Add Product Staking</h3>
            </div>
            <div class="panel-body">
                <form action="{{URL('/admin/master_staking/do_edit')}}" method="POST">
                    @csrf
                    <input type="hidden" name="id" value="{{$dataStaking->id}}">
                    <div class="form-group">
                        <label>Product Name</label>
                        <input class="form-control input-lg" placeholder="Product Name" type="text" name="product_name" value="{{$dataStaking->product_name}}">
                    </div>
                    <div class="form-group">
                        <label>Coin Stake</label>
                        <select class="form-control input-lg" name="id_coin_stake">
                            @foreach($dataCoin as $dt)
                            <option value="{{$dt->id}}" {{$dt->id == $dataStaking->id_coin_stake ? "selected" : ""}}>{{$dt->code}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Coin Reward</label>
                        <select class="form-control input-lg" name="id_coin_reward">
                            @foreach($dataCoin as $dt)
                            <option value="{{$dt->id}}" {{$dt->id == $dataStaking->id_coin_reward ? "selected" : ""}}>{{$dt->code}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="row">
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12">
                            <div class="form-group">
                                <label>Tenor</label>
                                <input class="form-control input-lg" placeholder="Tenor" type="number" name="tenor" value="{{$dataStaking->tenor}}">
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12">
                            <div class="form-group">
                                <label>Comitment Fee</label>
                                <input class="form-control input-lg" placeholder="Comitment Fee" type="number" name="comitment_fee" value="{{$dataStaking->comitment_fee}}">
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12">
                            <div class="form-group">
                                <label>Profit per month</label>
                                <input class="form-control input-lg" placeholder="Profit" type="number" name="profit" value="{{$dataStaking->profit}}">
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12">
                            <div class="form-group">
                                <label>Status</label>
                                <select name="status" class="form-control input-lg">
                                    @if($dataStaking->status == 0)
                                    <option value="1">Active</option>
                                    <option value="0" selected>Not Active</option>
                                    @else
                                    <option value="1" selected>Active</option>
                                    <option value="0">Not Active</option>
                                    @endif
                                </select>
                            </div>
                        </div>
                    </div>
                    <div style="text-align:center">
                        <button type="submit" class="btn btn-primary">Edit</button>
                    </div>
                </form>
            </div>
        </div>
        <!-- END BORDERED TABLE -->
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
    $(document).ready(function() {
        $('#myTable').DataTable();
    });
</script>
@include('Admin.footer')