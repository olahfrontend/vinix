@include('Admin.header',['activePage' => 'master_staking'])



<link rel="stylesheet" href="{{URL::asset('css/home.css')}}">
<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">

        <!-- BORDERED TABLE -->
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">Add Product Staking</h3>
            </div>
            <div class="panel-body">
                <form action="{{URL('/admin/master_staking/do_add')}}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label>Product Name</label>
                        <input class="form-control input-lg" placeholder="Product Name" type="text" name="product_name">
                    </div>
                    <div class="form-group">
                        <label>Coin Stake</label>
                        <select class="form-control input-lg" name="id_coin_stake">
                            @foreach($dataCoin as $dt)
                            <option value="{{$dt->id}}">{{$dt->code}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Coin Reward</label>
                        <select class="form-control input-lg" name="id_coin_reward">
                            @foreach($dataCoin as $dt)
                            <option value="{{$dt->id}}">{{$dt->code}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="row">
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12">
                            <div class="form-group">
                                <label>Tenor</label>
                                <input class="form-control input-lg" placeholder="Tenor" type="number" name="tenor">
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12">
                            <div class="form-group">
                                <label>Comitment Fee</label>
                                <input class="form-control input-lg" placeholder="Comitment Fee" type="number" name="comitment_fee">
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12">
                            <div class="form-group">
                                <label>Profit per month</label>
                                <input class="form-control input-lg" placeholder="Profit" type="number" name="profit">
                            </div>
                        </div>
                    </div>
                    <div style="text-align:center">
                        <button type="submit" class="btn btn-primary">Add</button>
                    </div>
                </form>
            </div>
        </div>
        <!-- END BORDERED TABLE -->
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
    $(document).ready(function() {
        $('#myTable').DataTable();
    });
</script>
@include('Admin.footer')