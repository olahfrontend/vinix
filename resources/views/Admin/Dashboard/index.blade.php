@include('Admin.header',['activePage' => 'dashboard'])
<style>
    .p-number {
        color: #E05227;
        font-weight: bold;
        font-size: 18px;
    }

    .icon.ic2 {
        margin-top: 5px;
    }
</style>
<link rel="stylesheet" href="{{URL::asset('css/home.css')}}">
<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            <!-- OVERVIEW -->
            <div class="panel panel-headline">
                <div class="panel-heading">
                    <h3 class="panel-title">GENERAL INFORMATION</h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="metric">
                                <span class="icon"><i class="fa fa-download"></i></span>
                                <p>
                                    <span class="number">{{$data->pending_withdraw}}</span>
                                    <span class="title">Pending Withdraw</span>
                                </p>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="metric">
                                <span class="icon"><i class="fa fa-shopping-bag"></i></span>
                                <p>
                                    <span class="number">${{number_format($data->total_harvest_in_dollar,2,".",",")}}</span>
                                    <span class="title">Total Harvest</span>
                                </p>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="metric">
                                <span class="icon"><i class="fa fa-dollar"></i></span>
                                <p>
                                    <span class="number">${{number_format($data->total_asset_in_dollar,2,".",",")}}</span>
                                    <span class="title">Total Asset</span>
                                </p>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="metric">
                                <span class="icon"><i class="fa fa-bar-chart"></i></span>
                                <p>
                                    <span class="number">${{number_format($data->total_sharing_profit_staking,2,".",",")}}</span>
                                    <span class="title">Total Can Harvest Staking</span>
                                </p>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="metric">
                                <span class="icon"><i class="fa fa-bar-chart"></i></span>
                                <p>
                                    <span class="number">${{number_format($data->total_sharing_profit_mining,2,".",",")}}</span>
                                    <span class="title">Total Can Harvest Mining</span>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="panel panel-headline">
                <div class="panel-heading">
                    <h3 class="panel-title">STAKING INFORMATION</h3>
                </div>

                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <?php $index = 0; ?>
                    @foreach($data->harvest->staking as $dt)
                    @if($index ==0)
                    <li class="nav-item active">
                        <a class="nav-link" id="{{$dt->code}}-tab" data-toggle="tab" href="#{{$dt->code}}" role="tab" aria-controls="{{$dt->code}}" aria-selected="true">{{$dt->code}}</a>
                    </li>
                    @else
                    <li class="nav-item">
                        <a class="nav-link" id="{{$dt->code}}-tab" data-toggle="tab" href="#{{$dt->code}}" role="tab" aria-controls="{{$dt->code}}" aria-selected="false">{{$dt->code}}</a>
                    </li>
                    @endif
                    <?php $index++; ?>
                    @endforeach
                </ul>

                <div class="tab-content" id="myTabContent">
                    <?php $index = 0; ?>
                    @foreach($data->harvest->staking as $dt)
                    <div class="tab-pane {{$index == 0 ? 'active' : 'fade'}}" id="{{$dt->code}}" role="tabpanel" aria-labelledby="{{$dt->code}}-tab">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="metric">
                                        <span class="icon ic2"><i class="fa fa-download"></i></span>
                                        <p>
                                            <span class="title">Total Harvest</span>
                                            <?php
                                            //CONVERT OBJECT JSON TO ARRAY
                                            $a = json_encode($data->harvest->staking);
                                            $b = json_decode($a, true);
                                            ?>
                                            @if($b[$dt->code]['amount'] > 10)
                                            <span class="number">{{number_format($b[$dt->code]['amount'],2,".",",")}} {{$dt->code}}</span>
                                            @else
                                            <span class="number">{{number_format($b[$dt->code]['amount'],8,".",",")}} {{$dt->code}}</span>
                                            @endif
                                            <span class="p-number">(${{number_format($b[$dt->code]['amount_in_dollar'],2,".",",")}})</span><br>
                                        </p>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="metric">
                                        <span class="icon"><i class="fa fa-shopping-bag"></i></span>
                                        <p>
                                            <span class="title">Total Can Harvest</span>
                                            <?php
                                            //CONVERT OBJECT JSON TO ARRAY
                                            $a = json_encode($data->staking);
                                            $b = json_decode($a, true);
                                            ?>
                                            @if($b[$dt->code]['amount'] > 10)
                                            <span class="number">{{number_format($b[$dt->code]['amount'],2,".",",")}} {{$dt->code}}</span>
                                            @else
                                            <span class="number">{{number_format($b[$dt->code]['amount'],8,".",",")}} {{$dt->code}}</span>
                                            @endif
                                            <span class="p-number">(${{number_format($b[$dt->code]['amount_in_dollar'],2,".",",")}})</span><br>
                                        </p>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="metric">
                                        <span class="icon"><i class="fa fa-shopping-bag"></i></span>
                                        <p>
                                            <span class="title">Total Staking</span>
                                            <?php
                                            //CONVERT OBJECT JSON TO ARRAY
                                            $a = json_encode($data->staking);
                                            $b = json_decode($a, true);
                                            ?>
                                            @if($b[$dt->code]['total_staking'] > 10)
                                            <span class="number">{{number_format($b[$dt->code]['total_staking'],2,".",",")}} {{$dt->code}}</span>
                                            @else
                                            <span class="number">{{number_format($b[$dt->code]['total_staking'],8,".",",")}} {{$dt->code}}</span>
                                            @endif
                                            <span class="p-number">(${{number_format($b[$dt->code]['total_staking_in_dollar'],2,".",",")}})</span><br>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <table class="table">
                                <thead>
                                    <tr>
                                        <td>Date Transaction</td>
                                        <td>User</td>
                                        <td>Type</td>
                                        <td>Product Name</td>
                                        <td>Currency</td>
                                        <td>Amount</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($datahistory->history_staking as $d)
                                    @if($d->code == $dt->code)
                                    <tr>
                                        <td>{{$d->created_at}}</td>
                                        <td>{{$d->name}}</td>
                                        <td>{{$d->trx_type}}</td>
                                        <td>{{$d->product_name}}</td>
                                        <td>{{$d->code}}</td>
                                        <td>{{$d->amount}}</td>
                                    </tr>
                                    @endif
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <?php $index++; ?>
                    @endforeach
                </div>
            </div>

            <div class="panel panel-headline">
                <div class="panel-heading">
                    <h3 class="panel-title">MINING INFORMATION</h3>
                </div>
                <?php $index = 0; ?>
                <ul class="nav nav-tabs" id="miningTab" role="tablist">
                    @foreach($data->harvest->mining as $dt)
                    <li class="nav-item {{$index == 0 ? 'active' : ''}}">
                        <a class="nav-link" id="mining-{{$dt->code}}-tab" data-toggle="tab" href="#mining-{{$dt->code}}" role="tab" aria-controls="mining-{{$dt->code}}" aria-selected="{{$index == 0 ? 'true' : 'false'}}">{{$dt->code}}</a>
                    </li>
                    <?php $index++; ?>
                    @endforeach
                </ul>

                <div class="tab-content" id="miningTabContent">
                    <?php $index = 0; ?>
                    @foreach($data->harvest->mining as $dt)
                    <div class="tab-pane {{$index == 0 ? 'active' : 'fade'}}" id="mining-{{$dt->code}}" role="tabpanel" aria-labelledby="mining-{{$dt->code}}-tab">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="metric">
                                        <span class="icon ic2"><i class="fa fa-download"></i></span>
                                        <p>
                                            <?php
                                            //CONVERT OBJECT JSON TO ARRAY
                                            $a = json_encode($data->harvest->mining);
                                            $b = json_decode($a, true);
                                            ?>
                                            <span class="title">Total Harvest</span>
                                            @if($b[$dt->code]['amount'] > 10)
                                            <span class="number">{{number_format($b[$dt->code]['amount'],2,".",",")}} {{$dt->code}}</span>
                                            @else
                                            <span class="number">{{number_format($b[$dt->code]['amount'],8,".",",")}} {{$dt->code}}</span>
                                            @endif
                                            <span class="p-number">(${{number_format($b[$dt->code]['amount_in_dollar'],2,".",",")}})</span><br>
                                        </p>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="metric">
                                        <span class="icon"><i class="fa fa-shopping-bag"></i></span>
                                        <p>
                                            <?php
                                            //CONVERT OBJECT JSON TO ARRAY
                                            $a = json_encode($data->mining);
                                            $b = json_decode($a, true);
                                            ?>
                                            <span class="title">Total Can Harvest</span>
                                            @if($b[$dt->code]['amount'] > 10)
                                            <span class="number">{{number_format($b[$dt->code]['amount'],2,".",",")}} {{$dt->code}}</span>
                                            @else
                                            <span class="number">{{number_format($b[$dt->code]['amount'],8,".",",")}} {{$dt->code}}</span>
                                            @endif
                                            <span class="p-number">(${{number_format($b[$dt->code]['amount_in_dollar'],2,".",",")}})</span><br>
                                        </p>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="metric">
                                        <span class="icon"><i class="fa fa-shopping-bag"></i></span>
                                        <p>
                                            <?php
                                            //CONVERT OBJECT JSON TO ARRAY
                                            $a = json_encode($data->mining);
                                            $b = json_decode($a, true);
                                            ?>
                                            <span class="title">Total Speed</span>
                                            <span class="number">{{number_format($b[$dt->code]['total_speed'],2,".",",")}} MHS</span>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <table class="table">
                                <thead>
                                    <tr>
                                        <td>Date Transaction</td>
                                        <td>User</td>
                                        <td>Type</td>
                                        <td>Product Name</td>
                                        <td>Currency</td>
                                        <td>Amount</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($datahistory->history_mining as $d)
                                    @if($d->code == $dt->code)
                                    <tr>
                                        <td>{{$d->created_at}}</td>
                                        <td>{{$d->name}}</td>
                                        <td>{{$d->trx_type}}</td>
                                        <td>{{$d->product_name}}</td>
                                        <td>{{$d->code}}</td>
                                        <td>{{$d->amount}}</td>
                                    </tr>
                                    @endif
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <?php $index++; ?>
                    @endforeach
                </div>
            </div>

        </div>

    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->
<script>
    $(function() {
        var data, options;

        // headline charts
        data = {
            labels: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'],
            series: [
                [23, 29, 24, 40, 25, 24, 35],
                [14, 25, 18, 34, 29, 38, 44],
            ]
        };

        options = {
            height: 300,
            showArea: true,
            showLine: false,
            showPoint: false,
            fullWidth: true,
            axisX: {
                showGrid: false
            },
            lineSmooth: false,
        };

        new Chartist.Line('#headline-chart', data, options);


        // visits trend charts
        data = {
            labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
            series: [{
                name: 'series-real',
                data: [200, 380, 350, 320, 410, 450, 570, 400, 555, 620, 750, 900],
            }, {
                name: 'series-projection',
                data: [240, 350, 360, 380, 400, 450, 480, 523, 555, 600, 700, 800],
            }]
        };

        options = {
            fullWidth: true,
            lineSmooth: false,
            height: "270px",
            low: 0,
            high: 'auto',
            series: {
                'series-projection': {
                    showArea: true,
                    showPoint: false,
                    showLine: false
                },
            },
            axisX: {
                showGrid: false,

            },
            axisY: {
                showGrid: false,
                onlyInteger: true,
                offset: 0,
            },
            chartPadding: {
                left: 20,
                right: 20
            }
        };

        new Chartist.Line('#visits-trends-chart', data, options);


        // visits chart
        data = {
            labels: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'],
            series: [
                [6384, 6342, 5437, 2764, 3958, 5068, 7654]
            ]
        };

        options = {
            height: 300,
            axisX: {
                showGrid: false
            },
        };

        new Chartist.Bar('#visits-chart', data, options);


        // real-time pie chart
        var sysLoad = $('#system-load').easyPieChart({
            size: 130,
            barColor: function(percent) {
                return "rgb(" + Math.round(200 * percent / 100) + ", " + Math.round(200 * (1.1 - percent / 100)) + ", 0)";
            },
            trackColor: 'rgba(245, 245, 245, 0.8)',
            scaleColor: false,
            lineWidth: 5,
            lineCap: "square",
            animate: 800
        });

        var updateInterval = 3000; // in milliseconds

        setInterval(function() {
            var randomVal;
            randomVal = getRandomInt(0, 100);

            sysLoad.data('easyPieChart').update(randomVal);
            sysLoad.find('.percent').text(randomVal);
        }, updateInterval);

        function getRandomInt(min, max) {
            return Math.floor(Math.random() * (max - min + 1)) + min;
        }

    });
</script>

@include('Admin.footer')