@include('Admin.header',['activePage' => 'withdraw'])

<link rel="stylesheet" href="{{URL::asset('css/wallet.css')}}">
<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">

        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">Edit Address</h3>
            </div>
            <div class="panel-body">
                <form action="{{URL('/admin/setting/update_fee')}}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label>Coin Name</label>
                        <select class="form-control input-lg" name="id" id="id_coin" onchange="coinChange()">
                            @foreach($dataCoin as $dt)
                            <option value="{{$dt->id}}">{{$dt->coin_name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Address</label>
                        <input class="form-control input-lg" placeholder="Address" type="text" name="address" id="address">
                    </div>
                    <div style="text-align:center">
                        <button type="submit" class="btn btn-primary">Edit</button>
                    </div>
                </form>
            </div>
        </div>
        <!-- BORDERED TABLE -->
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">List Request Deposit</h3>
            </div>

            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item active">
                    <a class="nav-link" id="pending-tab" data-toggle="tab" href="#pending" role="tab" aria-controls="pending" aria-selected="true">PENDING</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="canceled-tab" data-toggle="tab" href="#canceled" role="tab" aria-controls="canceled" aria-selected="false">CANCELED</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="done-tab" data-toggle="tab" href="#done" role="tab" aria-controls="done" aria-selected="false">COMPLETED</a>
                </li>
            </ul>

            <div class="tab-content" id="myTabContent">
                <div class="tab-pane active" id="pending" role="tabpanel" aria-labelledby="pending-tab">
                    <div class="panel-body">
                        <table id="myTable" class="table table-bordered display">
                            <thead>
                                <tr>
                                    <th>Nama Penarik</th>
                                    <th>Email</th>
                                    <th>Address</th>
                                    <th>Total Withdrawal</th>
                                    <th>Fee</th>
                                    <th>Netto Withdrawal</th>
                                    <th>Currency</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="tab-pane fade" id="canceled" role="tabpanel" aria-labelledby="canceled-tab">
                    <div class="panel-body">
                        <table id="myTable" class="table table-bordered display">
                            <thead>
                                <tr>
                                    <th>Nama Penarik</th>
                                    <th>Email</th>
                                    <th>Address</th>
                                    <th>Total Withdrawal</th>
                                    <th>Fee</th>
                                    <th>Netto Withdrawal</th>
                                    <th>Currency</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="tab-pane fade" id="done" role="tabpanel" aria-labelledby="done-tab">
                    <div class="panel-body">
                        <table id="myTable" class="table table-bordered display">
                            <thead>
                                <tr>
                                    <th>Nama Penarik</th>
                                    <th>Email</th>
                                    <th>Address</th>
                                    <th>Total Withdrawal</th>
                                    <th>Fee</th>
                                    <th>Netto Withdrawal</th>
                                    <th>Currency</th>
                                    <th>Txn hash</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- END BORDERED TABLE -->
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
    data = <?php echo json_encode($dataCoin); ?>;
    function coinChange() {
        $("#address").val(data[$("#id_coin").prop("selectedIndex")].address);
    }
    $(document).ready(function() {
        $('#myTable').DataTable();
        coinChange();
    });


    function confirm(id, comp, status, txn_hash = "") {
        var text = "Ingin menyelesaikan Transaksi withdrawal ini";
        var result = "Transaksi Berhasil diselesaikan";
        if (status == -1) {
            text = "Ingin menolak Transaksi withdrawal ini";
            result = "Transaksi berhasil ditolak";
        }
        swal({
                title: "Apakah anda yakin?",
                text: text,
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    //DELETE DATA
                    var formData = new FormData();
                    formData.append("_token", '<?php echo csrf_token(); ?>')
                    formData.append("id", id);
                    formData.append("status", status);
                    formData.append("txn_hash", txn_hash);
                    $.ajax({
                        type: 'POST',
                        processData: false,
                        contentType: false,
                        url: '{{ url(' / api / confirm_withdraw ') }}',
                        data: formData,
                        success: function(data) {
                            console.log(data);
                            $(comp).parents('tr').detach();
                            swal(result, {
                                icon: "success",
                            });
                        }
                    }).fail(function(data) {
                        console.log(data);
                        swal("Oops Something Wrong!", {
                            icon: "error",
                        });
                    });
                }
            });
    }

    function modalConfirm(id, comp, status) {
        swal({
                title: "Input Txn hash of this transaction to complete this withdrawal",
                buttons: true,
                dangerMode: true,
                content: "input",
            })
            .then((value) => {
                if (value) {
                    confirm(id, comp, status, value);
                }
            });
    }
</script>
@include('Admin.footer')