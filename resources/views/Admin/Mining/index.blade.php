@include('Admin.header',['activePage' => 'master_mining'])

<link rel="stylesheet" href="{{URL::asset('css/home.css')}}">
<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">

        <!-- BORDERED TABLE -->
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">Master Mining</h3>
            </div>
            <a href="/admin/master_mining/add">
                <div class="btn btn-primary" style="margin-left:25px;">
                    Add Product
                </div>
            </a>
            <div class="panel-body">
                @if(count($dataMining)>0)
                <table id="myTable" class="table table-bordered display">
                    <thead>
                        <tr>
                            <th>Product Name</th>
                            <th>Product Type</th>
                            <th>Coin Reward</th>
                            <th>Tenor</th>
                            <th>Price</th>
                            <th>Mining Speed</th>
                            <th style="max-width:50px;padding-left:0px;text-align:center;">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($dataMining as $dt)
                        <tr>
                            <td>{{$dt->product_name}}</td>
                            <td>{{$dt->product_type}}</td>
                            <td>{{$dt->code}}</td>
                            <td>{{$dt->tenor}}</td>
                            <td>{{$dt->price}}</td>
                            <td>{{$dt->mining_speed}}</td>
                            <td style="padding:5px; max-width:50px;">
                                <a href="{{URL('admin/master_mining/edit?id=').$dt->id}}">
                                    <div class="btn btn-success" style="width:100%;margin-bottom:5px;"><i class="fa fa-edit"></i></div>
                                </a>
                                <!-- <div class="btn btn-danger" onclick="confirm(<?php echo $dt->id; ?>,this,-1)" style="width:100%;"><i class="fa fa-trash"></i></div> -->
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            @else
            <h4 style="text-align:center;">No Product Mining</h4>
            @endif
        </div>
        <!-- END BORDERED TABLE -->
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
    $(document).ready(function() {
        $('#myTable').DataTable();
    });
</script>
@include('Admin.footer')