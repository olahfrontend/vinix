@include('Admin.header',['activePage' => 'master_mining'])



<link rel="stylesheet" href="{{URL::asset('css/home.css')}}">
<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">

        <!-- BORDERED TABLE -->
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">Add Product Mining</h3>
            </div>
            <div class="panel-body">
                <form action="{{URL('/admin/master_mining/do_add')}}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label>Product Name</label>
                        <input class="form-control input-lg" placeholder="Product Name" type="text" name="product_name">
                    </div>
                    <div class="form-group">
                        <label>Product Type</label>
                        <input class="form-control input-lg" placeholder="Product Type" type="text" name="product_type">
                    </div>
                    <div class="form-group">
                        <label>Coin Reward</label>
                        <select class="form-control input-lg" name="id_coin_reward">
                            @foreach($dataCoin as $dt)
                            <option value="{{$dt->id}}">{{$dt->code}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="row">
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12">
                            <div class="form-group">
                                <label>Tenor</label>
                                <input class="form-control input-lg" placeholder="Tenor" type="number" name="tenor">
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12">
                            <div class="form-group">
                                <label>Price ($)</label>
                                <input class="form-control input-lg" placeholder="Price" type="number" name="price">
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12">
                            <div class="form-group">
                                <label>Mining Speed (Mhs)</label>
                                <input class="form-control input-lg" placeholder="Mining Speed" type="number" name="mining_speed">
                            </div>
                        </div>
                    </div>
                    <div style="text-align:center">
                        <button type="submit" class="btn btn-primary">Add</button>
                    </div>
                </form>
            </div>
        </div>
        <!-- END BORDERED TABLE -->
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
    $(document).ready(function() {
        $('#myTable').DataTable();
    });
</script>
@include('Admin.footer')