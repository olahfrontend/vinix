@include('Admin.header',['activePage' => 'master_mining'])



<link rel="stylesheet" href="{{URL::asset('css/home.css')}}">
<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">

        <!-- BORDERED TABLE -->
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">Add Product Mining</h3>
            </div>
            <div class="panel-body">
                <form action="{{URL('/admin/master_mining/do_edit')}}" method="POST">
                    @csrf
                    <input type="hidden" name="id" value="{{$dataMining->id}}">
                    <div class="form-group">
                        <label>Product Name</label>
                        <input class="form-control input-lg" placeholder="Product Name" type="text" name="product_name" value="{{$dataMining->product_name}}">
                    </div>
                    <div class="form-group">
                        <label>Product Type</label>
                        <input class="form-control input-lg" placeholder="Product Type" type="text" name="product_type" value="{{$dataMining->product_type}}">
                    </div>
                    <div class="form-group">
                        <label>Coin Reward</label>
                        <select class="form-control input-lg" name="id_coin_reward">
                            @foreach($dataCoin as $dt)
                            <option value="{{$dt->id}}" {{$dt->id == $dataMining->id_coin_reward ? "selected" : ""}}>{{$dt->code}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="row">
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12">
                            <div class="form-group">
                                <label>Tenor</label>
                                <input class="form-control input-lg" placeholder="Tenor" type="number" name="tenor" value="{{$dataMining->tenor}}">
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12">
                            <div class="form-group">
                                <label>Price ($)</label>
                                <input class="form-control input-lg" placeholder="Price" type="number" name="price" value="{{$dataMining->price}}">
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12">
                            <div class="form-group">
                                <label>Mining Speed (Mhs)</label>
                                <input class="form-control input-lg" placeholder="Mining Speed" type="number" name="mining_speed" value="{{$dataMining->mining_speed}}">
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12">
                            <div class="form-group">
                                <label>Status</label>
                                <select name="status" class="form-control input-lg">
                                    @if($dataMining->status == 0)
                                    <option value="1">Active</option>
                                    <option value="0" selected>Not Active</option>
                                    @else
                                    <option value="1" selected>Active</option>
                                    <option value="0">Not Active</option>
                                    @endif
                                </select>
                            </div>
                        </div>
                    </div>
                    <div style="text-align:center">
                        <button type="submit" class="btn btn-primary">Edit</button>
                    </div>
                </form>
            </div>
        </div>
        <!-- END BORDERED TABLE -->
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
    $(document).ready(function() {
        $('#myTable').DataTable();
    });
</script>
@include('Admin.footer')