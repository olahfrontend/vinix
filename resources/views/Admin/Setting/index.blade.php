@include('Admin.header',['activePage' => 'master_setting'])



<link rel="stylesheet" href="{{URL::asset('css/home.css')}}">
<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">

        <!-- BORDERED TABLE -->
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">Setting General</h3>
            </div>
            <div class="panel-body">
                <form action="{{URL('/admin/setting/update_general')}}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label>Setting Name</label>
                        <select class="form-control input-lg" name="id" id="id_general" onchange="settingGeneralChange()">
                            @foreach($dataSetting->setting_general as $dt)
                            <option value="{{$dt->id}}">{{$dt->setting_name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Value</label>
                        <input class="form-control input-lg" placeholder="Value" step="any"  type="number" name="value" id="value_general">
                    </div>
                    <div style="text-align:center">
                        <button type="submit" class="btn btn-primary">Edit</button>
                    </div>
                </form>
            </div>
        </div>

        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">Setting Fee</h3>
            </div>
            <div class="panel-body">
                <form action="{{URL('/admin/setting/update_fee')}}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label>Setting Name</label>
                        <select class="form-control input-lg" name="id" id="id_fee" onchange="settingFeeChange()">
                            @foreach($dataSetting->setting_fee as $dt)
                            <option value="{{$dt->id}}">{{$dt->fee_name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Value</label>
                        <input class="form-control input-lg" placeholder="Value" step="any"  type="number" name="value" id="value_fee">
                    </div>
                    <div style="text-align:center">
                        <button type="submit" class="btn btn-primary">Edit</button>
                    </div>
                </form>
            </div>
        </div>
        <!-- END BORDERED TABLE -->
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
    var settingGeneral = <?php echo json_encode($dataSetting->setting_general); ?>;
    var settingFee = <?php echo json_encode($dataSetting->setting_fee); ?>;

    function settingGeneralChange() {
        // console.log(settingGeneral[$("#id_general")[0].selectedIndex].value);
        $("#value_general").val(settingGeneral[$("#id_general")[0].selectedIndex].value);
    }

    function settingFeeChange() {
        // console.log(settingGeneral[$("#id_general")[0].selectedIndex].value);
        $("#value_fee").val(settingFee[$("#id_fee")[0].selectedIndex].value);
    }
    $(document).ready(function() {
        settingFeeChange();
        settingGeneralChange();
    });
</script>
@include('Admin.footer')