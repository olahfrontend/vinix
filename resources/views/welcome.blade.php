<!doctype html>
<html lang="en">

<head>
    <title>Vinix</title>
    <link rel="icon" href="{{URL::asset('image/logo.png')}}" type="image/x-icon">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <!-- VENDOR CSS -->
    <link rel="stylesheet" href="{{URL::asset('assets/vendor/bootstrap/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{URL::asset('assets/vendor/font-awesome/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{URL::asset('assets/vendor/linearicons/style.css')}}">
    <link rel="stylesheet" href="{{URL::asset('assets/vendor/chartist/css/chartist-custom.css')}}">


    <link href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" rel="stylesheet">

    <!-- MAIN CSS -->
    <link rel="stylesheet" href="{{URL::asset('assets/css/main.css')}}">
    <link rel="stylesheet" href="{{URL::asset('css/welcome.css')}}">
    <!-- GOOGLE FONTS -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
    <!-- ICONS -->
    <link rel="apple-touch-icon" sizes="76x76" href="{{URL::asset('assets/img/apple-icon.png')}}">

</head>

<body>
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="brand">
            <a href="#"><img src="{{URL::asset('assets/img/logo-dark.png')}}" alt="Klorofil Logo" class="img-responsive logo"></a>
        </div>
        <div class="container-fluid">
            <div class="navbar-btn navbar-btn-right">
                <a href="/sign-up" class="btn btn-primary btn-login">SIGN UP</a>
            </div>
            <div class="navbar-btn navbar-btn-right">
                <div class="btn btn-primary btn-register" data-toggle="modal" data-target="#modalLogin"><span>SIGN IN</span></div>
            </div>
        </div>
    </nav>

    <div class="content1">
        <img src="{{URL::asset('image/background.jpg')}}">
        <div class="title">
            <h2>Start Mining Right Now!</h2>
            <h4>Your mining hardware is up and running!
                Just select your favorite virtual currency or mining algorithm and get started!</h4>
        </div>
    </div>


    <div class="content3">
        <div class="row">
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 left-side">
                <h2>Why Mining is Important?</h2>
                <h4>Virtual currency networks need computing power to operate safely. The best blockchain in the world is
                    supported by millions of computers around the world.</h4>
                <p>Unlike paper money, Bitcoin and other virtual currencies are mathematically
                    produced and executed digitally. People who volunteer their computing power
                    to secure the virtual currency network are called miners. Virtual currencies do not have a central
                    government or so-called 'middle men' who decide the future of the system.
                </p>
            </div>
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 right-side">
                <img src="{{URL::asset('image/img2.jpg')}}" id="img1">
                <img src="{{URL::asset('image/overlay2.png')}}" id="img2">
            </div>
        </div>
    </div>

    <div id="content2">
        <h2>Choose the mining package type</h2>
        <div class="row">
            <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6">
                <div class="item">
                    <h4 class="offer">Coming back <br>(Limited Offer)</h4>
                    <div class="title">
                        <h2>Small</h2>
                        <h5>Beginner</h5>
                    </div>
                    <div class="price">
                        <h2>$499,99</h2>
                    </div>
                    <div class="speed">
                        <h3>12.50 MH/s</h3>
                    </div>
                    <div class="desc">
                        24 Months Ethereum Mining * Ethash Mining Algorithm No Maintenance Fee
                    </div>
                    <a href="/sign-up"><button class="btn btn-primary">BUY</button></a>
                </div>
            </div>
            <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6">
                <div class="item">
                    <h4 class="offer">Coming back <br>(Limited Offer)</h4>
                    <div class="title">
                        <h2>Medium</h2>
                        <h5>Intermediate</h5>
                    </div>
                    <div class="price">
                        <h2>$999,00</h2>
                    </div>
                    <div class="speed">
                        <h3>25 MH/s</h3>
                    </div>
                    <div class="desc">
                        24 Months Ethereum Mining * Ethash Mining Algorithm No Maintenance Fee
                    </div>
                    <a href="/sign-up"><button class="btn btn-primary">BUY</button></a>
                </div>
            </div>
            <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6">
                <div class="item">
                    <h4 class="offer">Coming back <br>(Limited Offer)</h4>
                    <div class="title">
                        <h2>Large</h2>
                        <h5>Profesional</h5>
                    </div>
                    <div class="price">
                        <h2>$4.999,00</h2>
                    </div>
                    <div class="speed">
                        <h3>125 MH/s</h3>
                    </div>
                    <div class="desc">
                        24 Months Ethereum Mining * Ethash Mining Algorithm No Maintenance Fee
                    </div>
                    <a href="/sign-up"><button class="btn btn-primary">BUY</button></a>
                </div>
            </div>
        </div>
    </div>

    <div class="content1">
        <img src="{{URL::asset('image/background2.jpg')}}">
        <div class="title">
            <h2>Start Staking Right Now!</h2>
            <h4>Staking your coin, and get reward from your staked coin.</h4>
        </div>
    </div>

    <div class="content3">
        <div class="row">
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 left-side">
                <h2>What is Staking?</h2>
                <h4>Some blockchain protocols allow participants to earn additional cryptocurrency by contributing to the network.
                    These rewards can be earned in many different ways including staking, inflation, savings rates, etc.</h4>
                <p>Staking is the process of actively participating in transaction validation (similar to mining)
                    on a proof-of-stake (PoS) blockchain. On these blockchains, anyone with a minimum-required balance
                    of a specific cryptocurrency can validate transactions and earn Staking rewards </p>
            </div>
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 right-side">
                <img src="{{URL::asset('image/server.jpg')}}" id="img1">
                <img src="{{URL::asset('image/overlay.png')}}" id="img2">
            </div>
        </div>
    </div>

    <div id="content4">
        <h2>Stake Vinix or USDV</h2>
        <div class="row">
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">
                <div class="item">
                    <div class="desc">
                        <div class="left-side">
                            <img src="{{URL::asset('image/logo-usd.png')}}" width="50px" height="50px">
                        </div>
                        <h3 class="right-side title">
                            USDV STAKE<br>
                            <div class="sub-title">
                                <div class="core"><i class="fa fa-check-circle"></i> Core</div>
                                <div class="double">20X</div>
                            </div>
                        </h3>
                    </div>
                    <div class="desc">
                        <h4 class="left-side">APR:</h4>
                        <h4 class="right-side">24%</h4>
                    </div>
                    <div class="desc">
                        <h4 class="left-side">Earn:</h4>
                        <h4 class="right-side">USDV</h4>
                    </div>
                    <div class="container-btn">
                        <a href="/sign-up"><button class="btn btn-primary">UNLOCK WALLET</button></a>
                    </div>
                </div>
            </div>
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">
                <div class="item">
                    <div class="desc">
                        <div class="left-side">
                            <img src="{{URL::asset('image/logo-vnx.png')}}" width="50px" height="50px">
                        </div>
                        <h3 class="right-side title">
                            VINIX STAKE<br>
                            <div class="sub-title">
                                <div class="core"><i class="fa fa-check-circle"></i> Core</div>
                                <div class="double">20X</div>
                            </div>
                        </h3>
                    </div>
                    <div class="desc">
                        <h4 class="left-side">APR:</h4>
                        <h4 class="right-side">72%</h4>
                    </div>
                    <div class="desc">
                        <h4 class="left-side">Earn:</h4>
                        <h4 class="right-side">VNX</h4>
                    </div>
                    <div class="container-btn">
                        <a href="/sign-up"><button class="btn btn-primary">UNLOCK WALLET</button></a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- <div id="content5">
        <div class="row">
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                <div class="item">
                    <h4>Vinix Member</h4>
                    <h2 id="total_member">0</h2>
                    <p>User join to Vinix Member</p>
                </div>
            </div>

            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                <div class="item">
                    <h4>Total Value Locked (TVL)</h4>
                    <h2 id="total_value_locked">$0</h2>
                    <p>Across all Staking and Mining Pools</p>
                </div>
            </div>
        </div>
    </div> -->

    <div id="modalLogin" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <div class="modal-subheader">
                        <img src="{{URL::asset('image/logo.png')}}" width="100px" height="auto" />
                        <h2>V I N I X</h2>
                    </div>
                    <form action="{{url('do-login')}}" method="POST">
                        @csrf
                        <div class="form-group">
                            <label>Email</label><br>
                            <input type="text" placeholder="Email" name="email">
                        </div>
                        <div class="form-group">
                            <label>Password</label><br>
                            <input type="password" placeholder="Password" name="password">
                        </div>
                        <div style="text-align:right;">
                            <a href="/forgot-password">Forgot password</a>
                        </div>
                        <div class="btn-container">
                            <button class="btn btn-primary" type="submit">SIGN IN</button>
                        </div>
                    </form>
                    <div class="dont-have">
                        I don't have account, <a href="/sign-up">create a new one.</a>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <!-- Javascript -->
    <script src="{{URL::asset('assets/vendor/jquery/jquery.min.js')}}"></script>
    <script src="{{URL::asset('assets/vendor/bootstrap/js/bootstrap.min.js')}}"></script>
    <script src="{{URL::asset('assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
    <script src="{{URL::asset('assets/vendor/jquery.easy-pie-chart/jquery.easypiechart.min.js')}}"></script>
    <script src="{{URL::asset('assets/vendor/chartist/js/chartist.min.js')}}"></script>
    <script src="{{URL::asset('assets/scripts/klorofil-common.js')}}"></script>
    <script>
        function animateValue(obj, start, end, duration, prefix = "") {
            let startTimestamp = null;
            const step = (timestamp) => {
                if (!startTimestamp) startTimestamp = timestamp;
                const progress = Math.min((timestamp - startTimestamp) / duration, 1);
                if (prefix == "") {
                    obj.innerHTML = prefix + numberWithCommas(parseFloat(progress * (end - start) + start).toFixed());
                }
                else{
                obj.innerHTML = prefix + numberWithCommas(parseFloat(progress * (end - start) + start).toFixed(2));
                }
                if (progress < 1) {
                    window.requestAnimationFrame(step);
                }
            };
            window.requestAnimationFrame(step);
        }
        $(document).ready(function() {
            console.log(Date.now());
            var pembanding_supply = parseFloat("0.000000000172");
            var pembanding_value = parseFloat("0.000000452");
            var total_vinix_supply = parseFloat(Date.now() * pembanding_supply).toFixed();
            var total_value_locked = parseFloat(Date.now() * pembanding_value).toFixed(2);

            // $("#total_vinix_supply").html(numberWithCommas(total_vinix_supply));
            // $("#total_value_locked").html("$" + numberWithCommas(total_value_locked));


            const obj = document.getElementById("total_member");
            animateValue(obj, 0, total_vinix_supply, 2000);
            const obj2 = document.getElementById("total_value_locked");
            animateValue(obj2, 0, total_value_locked, 2000, "$");
        });

        function numberWithCommas(x) {
            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }
    </script>
</body>


</html>