<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrxDepositTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trx_deposit', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("id_user")->unsigned()->index();
            $table->integer("id_coin")->unsigned()->index();
            $table->string("address")->nullable();
            $table->string("trx_type");
            $table->integer("status");
            $table->string("qr_link")->nullable();
            $table->decimal("totaL_fee", 32, 8)->default(0.00000000);
            $table->decimal("fee", 32, 8)->default(0.00000000);
            $table->decimal("amount", 32, 8)->default(0.00000000)->nullable();
            $table->timestamps();

            $table->foreign("id_coin")->references("id")->on("master_coin")->onDelete("cascade");
            $table->foreign("id_user")->references("id")->on("users")->onDelete("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trx_deposit');
    }
}
