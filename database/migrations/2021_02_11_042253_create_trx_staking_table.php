<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrxStakingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trx_staking', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("id_user")->unsigned()->index();
            $table->integer("id_product_staking")->unsigned()->index();
            $table->decimal("amount_stake",32,8);
            $table->date("harvest_at");
            $table->date("end_at");
            $table->timestamps();
            
            $table->foreign("id_user")->references("id")->on("users")->onDelete("cascade");
            $table->foreign("id_product_staking")->references("id")->on("master_staking")->onDelete("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trx_staking');
    }
}
