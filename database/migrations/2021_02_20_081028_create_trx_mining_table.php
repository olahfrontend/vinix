<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrxMiningTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trx_mining', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("id_user")->unsigned()->index();
            $table->integer("id_product_mining")->unsigned()->index();
            $table->integer("status");
            $table->date("end_at");
            $table->date("last_harvest");
            $table->decimal("mining_speed");
            $table->timestamps();

            $table->foreign("id_user")->references("id")->on("users")->onDelete("cascade");
            $table->foreign("id_product_mining")->references("id")->on("master_mining")->onDelete("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trx_mining');
    }
}
