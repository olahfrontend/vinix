<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrxExchangeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trx_exchange', function (Blueprint $table) {

            $table->increments('id');
            $table->integer("id_user")->unsigned()->index();
            $table->integer("from_coin")->unsigned()->index();
            $table->integer("to_coin")->unsigned()->index();
            $table->integer("status");
            $table->decimal("fee", 32, 8)->default(0.00000000);
            $table->decimal("from_amount", 32, 8)->default(0.00000000)->nullable();
            $table->decimal("to_amount", 32, 8)->default(0.00000000)->nullable();
            $table->timestamps();

            $table->foreign("from_coin")->references("id")->on("master_coin")->onDelete("cascade");
            $table->foreign("to_coin")->references("id")->on("master_coin")->onDelete("cascade");
            $table->foreign("id_user")->references("id")->on("users")->onDelete("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trx_exchange');
    }
}
