<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldToTrxMiningTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trx_mining', function (Blueprint $table) {
            //
            $table->integer("id_product_mining")->nullable()->unsigned()->change();
            $table->decimal("price")->nullable()->default(null);
            $table->integer("id_coin_reward")->nullable()->default(null);
            $table->integer("tenor")->nullable()->default(null);
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trx_mining', function (Blueprint $table) {
            //
        });
    }
}
