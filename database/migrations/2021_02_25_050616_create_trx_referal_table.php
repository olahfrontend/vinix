<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrxReferalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trx_referal', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("id_user")->unsigned()->index();
            $table->integer("id_ref");
            $table->integer("status");
            $table->string("type");
            $table->datetime("last_harvest");
            $table->integer("level");
            $table->timestamps();
            
            $table->foreign("id_user")->references("id")->on("users")->onDelete("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trx_referal');
    }
}
