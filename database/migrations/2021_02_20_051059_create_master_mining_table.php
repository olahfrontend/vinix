<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMasterMiningTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('master_mining', function (Blueprint $table) {
            $table->increments('id');
            $table->string("product_name");
            $table->string("product_type");
            $table->integer("id_coin_reward")->unsigned()->index();
            $table->integer("tenor");
            $table->decimal("price");
            $table->decimal("mining_speed");
            $table->timestamps();

            $table->foreign("id_coin_reward")->references("id")->on("master_coin")->onDelete("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('master_mining');
    }
}
