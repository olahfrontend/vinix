<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePriceCoinTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('price_coin', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("id_coin")->unsigned()->index();
            $table->float("price");
            $table->timestamps();

            $table->foreign("id_coin")->references("id")->on("master_coin")->onDelete("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('price_coin');
    }
}
