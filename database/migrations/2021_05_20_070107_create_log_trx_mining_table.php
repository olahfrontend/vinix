<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogTrxMiningTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_trx_mining', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("id_trx_mining")->unsigned()->index();
            $table->decimal("earning", 32, 8);
            $table->timestamps();
            
            $table->foreign("id_trx_mining")->references("id")->on("trx_mining")->onDelete("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_trx_mining');
    }
}
