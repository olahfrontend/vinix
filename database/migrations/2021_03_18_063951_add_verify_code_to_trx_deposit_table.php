<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddVerifyCodeToTrxDepositTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trx_deposit', function (Blueprint $table) {
            //
            $table->string("verify_code")->default("");
            $table->integer("verified")->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trx_deposit', function (Blueprint $table) {
            //
            $table->dropColumn("verify_code");
            $table->dropColumn("verified");
        });
    }
}
