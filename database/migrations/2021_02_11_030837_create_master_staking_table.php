<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMasterStakingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('master_staking', function (Blueprint $table) {
            $table->increments('id');
            $table->string("product_name");
            $table->integer("id_coin_stake")->unsigned()->index();
            $table->integer("id_coin_reward")->unsigned()->index();
            $table->integer("tenor");
            $table->decimal("comitment_fee");
            $table->decimal("profit");
            $table->timestamps();

            $table->foreign("id_coin_reward")->references("id")->on("master_coin")->onDelete("cascade");
            $table->foreign("id_coin_stake")->references("id")->on("master_coin")->onDelete("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('master_staking');
    }
}
