<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSharingProfitTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sharing_profit', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("id_coin")->unsigned()->index();
            $table->integer("min_value");
            $table->integer("max_value");
            $table->timestamps();

            $table->foreign("id_coin")->references("id")->on("master_coin")->onDelete("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sharing_profit');
    }
}
